#ifndef BALISE_H
#define BALISE_H

#include <QObject>
#include <QList>
#include "itineraire.h"
/*
 *Classe Balise
 *de Henri Bourdeau
 *
 *Objet servant à modéliser les intersections/pièces
 */
class Balise : public QObject
{
    Q_OBJECT
public :
    Balise();                                 // Crée une nouvelle balise
    void ajouterAdresse(int const &a);        // Ajoute une balise au carnet d'adresses
    bool estConnue(int const &a);             // Vérifie si la balise a est dans le carnet
    bool dejaVu(Itineraire const &i) const;   // Demande à l'itinéraire s'il l'a déjà visitée
    bool plusCourt(Itineraire const &i) const;// Vérifie si un chemin calculé plus court existe
    QList<int> getAdresses() const;           // Donne son carnet d'adresses
protected :
    int m_nom;                                // Nom de la balise
    QList<int> m_adresses;                    // Carnet d'adresses

public slots :
    void itineraireRecu(Itineraire i);        // Recoit l'itinéraire i
signals :
    void itineraireCalcule(Itineraire i);     // Envoie l'itinéraire i terminé au simulateur
    void itineraireEnvoye(Itineraire i);      // Envoie l'itinéraire aux contacts

};

#endif // BALISE_H
