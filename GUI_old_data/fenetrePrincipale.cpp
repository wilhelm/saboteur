#include "fenetrePrincipale.h"

FenetrePrincipale::FenetrePrincipale()                      // Crée une nouvelle fenêtre
{
    m_menuCalcul = menuBar()->addMenu("Calcul");
    m_barreCalcul = addToolBar("Calcul");

    m_actionCalcul = new QAction("Nouvel Itineraire", this);

    m_actionCalcul->setShortcut(QKeySequence("Ctrl+C"));

    // Connecte le signal d'activation de l'action avec sa fonction d'initialisation de procédure
    connect(m_actionCalcul, SIGNAL(triggered()), this, SLOT(nouvelItineraire()));

    m_menuCalcul->addAction(m_actionCalcul);
    m_barreCalcul->addAction(m_actionCalcul);

    m_dock = new QDockWidget("Itineraires");
    m_dock->setAllowedAreas(Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea);
    m_chemins = new QListWidget(m_dock);

    // Ajoute la liste de chemins à celles affichées dans le dock
    m_dock->setWidget(m_chemins);
    addDockWidget(Qt::RightDockWidgetArea, m_dock);

    m_simulateur=new Simulateur; // Crée le nouveau simulateur

    // Connecte le signal d'itinéraire calculé du simulateur à sa fonction correspondante
    connect(m_simulateur, SIGNAL(cheminTermine(Itineraire)), this, SLOT(cheminCalcule(Itineraire)));

    // Gère l'affichage du chemin sélectionné
    connect(m_chemins, SIGNAL(currentRowChanged(int)), m_simulateur, SLOT(activerTraget(int)));

    setCentralWidget(m_simulateur);
}

void FenetrePrincipale::nouvelItineraire() // Lance la procédure de calcul
{
    int baliseD, baliseF; // Stockeront les balises de départ et d'arrivée

    delete m_chemins;     // Réinitialise la liste de chemins
    m_chemins = new QListWidget(m_dock);
    m_dock->setWidget(m_chemins);
    connect(m_chemins, SIGNAL(currentRowChanged(int)), m_simulateur, SLOT(activerTraget(int)));

    // Demande les balises de départ et de fin à l'utilisateur
    baliseD = QInputDialog::getInt(this, "Balise de départ", "Entrez la balise de départ");

    baliseF = QInputDialog::getInt(this, "Balise d'arrivée", "Entrez la balise d'arrivée");

    // Ordonne au simulateur de calculer les itinéraires
    m_simulateur->initialiserCalcul(baliseD, baliseF);
}

void FenetrePrincipale::cheminCalcule(const Itineraire i) // Traitement de l'itinéraire terminé
{
    Chemin chemin(i.getChemin());                         // Convertit l'itinéraire en chemin
    m_trajets.append(&chemin);                            // Ajoute le chemin à la liste
    m_chemins->addItems(QStringList() << chemin.getNom());// Ajoute l'objet nom du chemin à la liste affichée
}
