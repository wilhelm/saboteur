#include "chemin.h" // Stocke les chemins finis

Chemin::Chemin(QList<int> L) // Crée un nouvel objet Chemin
                             // Recevant une liste de noms
{
    int i;
    QString str;

    for(i = 0; i < L.size(); i++)
    {
        str.setNum(L[i]);
        m_nom += str + " ";         // Ajoute les noms de la liste à la sienne
    }
}

QString Chemin::getNom() const // Renvoie la liste des balises visitées
{
    return m_nom;
}
