#ifndef FENETREPRINCIPALE_H
#define FENETREPRINCIPALE_H

#include "simulateur.h"
#include <QMainWindow>
#include <QtWidgets>
#include <QListWidget>
#include <QString>
#include "chemin.h"
/*
 *Classe FenêtrePrincipale
 *de Henri Bourdeau
 *
 *Objet servant de fenêtre pour l'interface avec l'utilisateur
 **/
class FenetrePrincipale : public QMainWindow
{
    Q_OBJECT
public :
    FenetrePrincipale();                   // Crée une nouvelle fenêtre
public slots :
    void nouvelItineraire();               // Reçoit l'ordre d'entamer une procédure de calcul d'itinéraire
    void cheminCalcule(Itineraire const i);// Reçoit un chemin terminé
protected :
    Simulateur *m_simulateur;              // Le simulateur
    QMenu *m_menuCalcul;                   // Barre de menu pour le calcul
    QToolBar *m_barreCalcul;               // Barre d'outil pour le calcul
    QAction *m_actionCalcul;               // Action déclenchant la procédure de calcul
    QDockWidget *m_dock;                   // Widget servant à afficher les chemins calculés
    QListWidget *m_chemins;                // Liste des noms des chemins calculés
    QList<Chemin*> m_trajets;              // Liste contenant les objets des chemins calculés


};

#endif // FENETREPRINCIPALE_H
