#ifndef ITINERAIRE_H
#define ITINERAIRE_H

#include <QList>
/*
 *Classe Itinéraire
 *de Henri Bourdeau
 *
 *Objet servant à stocker les itinéraire au cours du calcul
 */
class Itineraire
{
public :
    Itineraire(int const &cible);      // Crée un nouvel itinéraire avec la cible
    int getCible() const;              // Donne le nom de la cible
    QList<int> getChemin() const;      // Donne la liste des balises visitées
    void baliseVisitee(int const &nom);// Ajoute la balise nom à la liste des balises visitées
private :
    int m_cible;                       // Nom de la cible
    QList<int> m_chemin;               // Liste des noms des balises visitées
};

#endif // ITINERAIRE_H
