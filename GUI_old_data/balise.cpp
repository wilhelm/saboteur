#include "balise.h"
#include "simulateur.h"

Balise::Balise() : m_nom(Simulateur::nombreBalises) {} // Crée un nouvel objet balise

void Balise::ajouterAdresse(const int &a) // Ajoute la balise a au carnet d'adresses
{
    m_adresses.append(a);
}

bool Balise::estConnue(const int &a) // Verifie si a est déjà dans le carnet d'adresses
{
    return m_adresses.contains(a);
}

bool Balise::dejaVu(const Itineraire &i) const // Verifie si l'itinéraire est déjà passé
{
    return i.getChemin().contains(m_nom);
}

bool Balise::plusCourt(const Itineraire &i) const // Vérifie si une solution plus courte
{                                                 // a déjà été calculée
    QList<int> L(i.getChemin());
    if(L.size() < Simulateur::tailleCheminMini -1)
        return true;
    return false;
}

QList<int> Balise::getAdresses() const // Envoie le carnet d'adresses
{
    return m_adresses;
}

void Balise::itineraireRecu(Itineraire i) // Traitement de l'itinéraire reçu
{
    if(!dejaVu(i) && plusCourt(i))
    {
        i.baliseVisitee(m_nom);           // Ajoute le nom de la balise à l'itinéraire
        int cible(i.getCible());
        if(cible==m_nom)                  // Vérifie si elle est la balise cible
            emit itineraireCalcule(i);    // Transmet l'itinéraire calculé à l'interface
        else
            emit itineraireEnvoye(i);     // Transmet l'itinéraire aux balises adjacentes
    }
}
