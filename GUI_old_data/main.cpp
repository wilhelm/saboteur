#include "fenetrePrincipale.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv); // initialisation

    FenetrePrincipale fenetrePrincipale; // Création de la fenêtre

    fenetrePrincipale.show(); // Affichage

    return app.exec(); // Fin

}
