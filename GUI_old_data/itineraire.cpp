#include "itineraire.h"

Itineraire::Itineraire(const int &cible) : m_cible(cible) {} // Crée le nouvel itinéraire

int Itineraire::getCible() const // Donne le nom de la cible
{
    return m_cible;
}

QList<int> Itineraire::getChemin() const // Donne la liste des balises visitées
{
    return m_chemin;
}

void Itineraire::baliseVisitee(const int &nom) //  Ajoute la balise nom à la liste
{
    m_chemin.append(nom);
}
