#ifndef SIMULATEUR_H
#define SIMULATEUR_H

#include <QApplication>
#include <QObject>
#include <QWidget>
#include <QMouseEvent>
#include <QPoint>
#include <QList>
#include <QLine>
#include <QPicture>
#include <QString>
#include <QColor>
#include <QRect>
#include <QPainter>
#include "balise.h"
#include "itineraire.h"

enum
{
    LIAISON = 1,
    BALISE = 2
};

class Simulateur : public QWidget
{
    Q_OBJECT

public :
    Simulateur(QWidget *parent = 0);                // Crée un nouveau simulateur
    void ajouterBalise();                           // Procédure d'ajout de balise
    void ajouterLiaison();                          // Liaison de deux balises
    void anuler();                                  // Annule la dernière action
    int getBalisePointee(QPoint const &point) const;// Donne la balise pointée par la souris
    void initialiserCalcul(int const &baliseD, int const &baliseF);// Lance le calcul
    bool positionBaliseEstValide() const;           /* Indique si la création d'une balise à cet endroit
                                                     n'en cache pas une autre*/
    static int nombreBalises;                       // Contient le nombre de balises
    static int tailleCheminMini;                    // Contient la taille du plus petit chemin calculé

public slots :
    void cheminRecu(Itineraire const i);            // Reçoit l'itinéraire calculé i
    void activerTraget(int const rang);             // Affiche le traget rang

protected :
    // Gestion de la souris
    void mouseMoveEvent(QMouseEvent *e);
    void mousePressEvent(QMouseEvent *e);
    void mouseReleaseEvent(QMouseEvent *e);
    void paintEvent(QPaintEvent *);

    // Eléments nécessaires à l'affichage
    QList<QLine> m_liaisons;                        // Traits des liaisons
    QList<QLine> m_trajet;                          // Traits du trajet affiché
    QList<QPoint> m_positions;                      // Positions des images des balises
    QList<QPicture> m_images;                       // Images des balises

    QList<Balise*> m_balises;                       // Liste des objets balises
    QList<Itineraire> m_chemins;                    // Liste des itinéraires calculés
    QList<int> m_actions;                           // Liste des actions effectuées
    QPoint m_posDClic;                              // Position de la souris au début du clic
    QPoint m_posFClic;                              // Idem à la fin
    int m_baliseD;                                  // Balise pointée au début du clic
    int m_baliseF;                                  // Idem à la fin
    int m_boutonClique;                             // Bouton de la souris cliqué

signals :
    void cheminTermine(const Itineraire i);         // Envoie l'itinéraire i calculé à la fenêtre
};

#endif // SIMULATEUR_H
