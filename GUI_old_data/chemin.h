#ifndef CHEMIN_H
#define CHEMIN_H

#include <QWidget>
#include <QList>
#include <QString>

/*
 *Classe Chemin
 *de Henri Bourdeau
 *
 *Objet servant à stocker les Itinéraires Calculés
 **/

class Chemin : public QWidget
{
public :
    Chemin(QList<int> L);  // Crée un nouveau chemin avec la liste L de noms
    QString getNom() const;// Donne son nom
protected :
    int m_numero;          // Numéro d'identification du chemin
    QString m_nom;         // Nom du chemin, contenant les balises à parcourir
};

#endif // CHEMIN_H
