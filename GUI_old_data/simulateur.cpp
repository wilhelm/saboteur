#include "simulateur.h"

// Proportions de l'affichage
#define H_BALISE  30
#define L_BALISE  30
#define ECHELLE 20

// Initialisation des variables globales
int Simulateur::nombreBalises = 0;
int Simulateur::tailleCheminMini = 0;

Simulateur::Simulateur(QWidget *parent) : QWidget(parent) // Création du simulateur
{
    // Initialisation de la balise 0
    QPoint position(L_BALISE/2, H_BALISE / 2);
    QRect contour(-L_BALISE/2, -H_BALISE/2, L_BALISE, H_BALISE);
    QColor peinture(0,200,0), stylo(0, 0, 0);
    QString nom("%1");
    QPainter painter;
    QPicture image;
    QPoint zero(0, 0);

    nom = nom.arg(nombreBalises);

    // Initialisation des positions des clics
    m_posDClic = zero;
    m_posFClic = zero;

    // Dessin de l'image de la balise 0
    painter.begin(&image);

    painter.setPen(Qt::NoPen);
    painter.setBrush(peinture);
    painter.drawRect(contour);

    painter.setPen(stylo);
    painter.setBrush(Qt::NoBrush);
    painter.drawText(contour, Qt::AlignCenter, nom);

    painter.end();

    // Stockage de la position et de l'image associée
    m_positions.append(position);
    m_images.append(image);

    // Création de l'objet balise
    Balise *balise;
    balise = new Balise;
    QObject::connect(balise, SIGNAL(itineraireCalcule(Itineraire)), this, SLOT(cheminRecu(Itineraire)));
    m_balises.append(balise);

    // Incrémentation du nombre de balises
    nombreBalises++;

    // Rafraichissement de l'image
    update();
}

void Simulateur::activerTraget(int const rang) // Affiche le trajet sélectionné
{
    m_trajet.clear();                     // Efface le trajet précédent
    QList<int> balises;
    balises = m_chemins[rang].getChemin();// Récupère le chemin souhaité
    int i;
    for(i = 0; i < balises.size()-1; i++)
        m_trajet.append(QLine(m_positions[balises[i]], m_positions[balises[i+1]]));/* Ajoute les
                                                                                    lignes du trajet à la liste
                                                                                    des trajets*/
    // Rafraichissement de l'image
    update();
}

void Simulateur::ajouterBalise()  // Ajout d'une nouvelle balise
{
    // Création des variables nécessaires
    QRect contour(-L_BALISE/2, -H_BALISE/2, L_BALISE, H_BALISE);
    QColor peinture(150,0,150), stylo(0, 0, 0);
    QString nom("%1");
    QPainter painter;
    QPicture image;

    nom = nom.arg(nombreBalises);

    // Dessin de l'image de la balise
    painter.begin(&image);

    painter.setPen(Qt::NoPen);
    painter.setBrush(peinture);
    painter.drawRect(contour);

    painter.setPen(stylo);
    painter.setBrush(Qt::NoBrush);
    painter.drawText(contour, Qt::AlignCenter, nom);

    painter.end();

    // Stockage de la position et de l'image associée
    m_positions.append(m_posFClic);
    m_images.append(image);

    // Création de l'objet balise
    Balise *balise;
    balise = new Balise;
    QObject::connect(balise, SIGNAL(itineraireCalcule(Itineraire)), this, SLOT(cheminRecu(Itineraire)));
    m_balises.append(balise);

    nombreBalises++;

    m_actions.append(BALISE); // Mémorise la création d'une balise dans la liste des actions

    // Rafraichissement de l'image
    update();
}

void Simulateur::ajouterLiaison() // Ajout d'une liaison entre balises
{
    if(!m_balises[m_baliseD]->estConnue(m_baliseF)) // Vérifie si la liaison existe déjà
    {
        QLine L(m_positions[m_baliseD], m_positions[m_baliseF]); // Création de la ligne
        m_liaisons.append(L);
        m_actions.append(LIAISON); // Mémorise la création d'une liaison dans la liste des actions

        // Connection des signaux des balises
        connect(m_balises[m_baliseD], SIGNAL(itineraireEnvoye(Itineraire)), m_balises[m_baliseF], SLOT(itineraireRecu(Itineraire)));
        connect(m_balises[m_baliseF], SIGNAL(itineraireEnvoye(Itineraire)), m_balises[m_baliseD], SLOT(itineraireRecu(Itineraire)));

        // Ajout des balises dans les carnets d'adresse
        m_balises[m_baliseD]->ajouterAdresse(m_baliseF);
        m_balises[m_baliseF]->ajouterAdresse(m_baliseD);
    }
}

void Simulateur::anuler() // Annule la dernière action
{
    if(!m_actions.isEmpty())
    {
        int A(m_actions.last());
        switch(A)
        {
        case LIAISON :
            {
            QLine L;
            L = m_liaisons.takeLast();
            break;
            }
        case BALISE :
            {
            m_positions.pop_back();
            m_images.pop_back();
            Balise *B(m_balises.takeLast());
            delete B;
            nombreBalises --;
            break;
            }
        }
        m_actions.pop_back();
    }
}

int Simulateur::getBalisePointee(QPoint const &point) const // Retourne la balise sur laquelle pointe la souris
{
    int i;
    for(i = 0; i < m_positions.size(); i++)
    {
        if((m_positions[i].x() - point.x() <= L_BALISE/2)&&(point.x() - m_positions[i].x() <= L_BALISE/2) &&(m_positions[i].y() - point.y() <= H_BALISE/2)&&(point.y() - m_positions[i].y() <= H_BALISE/2))
            return i;
    }
    return -1;
}

void Simulateur::initialiserCalcul(const int &baliseD, const int &baliseF) // Lance le calcul de l'itinéraire
{
    Itineraire chemin(baliseF); // Nouvel itinéraire avec pour cible baliseF

    tailleCheminMini = nombreBalises+1; // Plus long que tout chemin possible

    // Réinitialisation des chemins stockés
    m_chemins.clear();
    m_trajet.clear();

    m_balises[baliseD]->itineraireRecu(chemin); // Envoie l'itinéraire à la balise de départ
}

bool Simulateur::positionBaliseEstValide() const /* Vérifie que la création d'une balise à cet endroit n'en
                                                   cachera pas une autre*/
{
    QPoint point2(L_BALISE/2, H_BALISE / 2), point1(L_BALISE/2, -H_BALISE / 2);
    if(getBalisePointee(m_posFClic + point1)==-1 && getBalisePointee(m_posFClic - point1)==-1 && getBalisePointee(m_posFClic + point2)==-1 && getBalisePointee(m_posFClic - point2)==-1)
        return true;

    return false;
}

void Simulateur::mouseMoveEvent(QMouseEvent *e) // Gestion des mouvements de la souris (uniquement quand un clic est en cours)
{
    m_posFClic = e->pos();
    m_trajet.clear(); // Efface le chemin affiché en cas de clic

    // Rafraichissement de l'image
    update();    
}

void Simulateur::mousePressEvent(QMouseEvent *e) // Début de la gestion des clics
{
    m_posDClic = e->pos(); // Stoque la position de la souris

    int i(getBalisePointee(m_posDClic)); // Vérifie si une balise est pointée
    if(i != -1) // Si une balise est pointée
        m_posDClic = m_positions[i]; // Centre le clic sur la balise

    m_posFClic = m_posDClic; // Evite les affichages de traits vers l'ancienne position de la souris
    m_baliseD = getBalisePointee(m_posDClic); // Stocke le nom de la balise cliquée
    m_boutonClique = e->button(); // Stocke le nom du bouton cliqué
}

void Simulateur::mouseReleaseEvent(QMouseEvent *) // Fin de la gestion du clic
{
    switch(m_boutonClique) // Actions suivant le bouton cliqué
    {
    case Qt::LeftButton :
        if(m_posDClic==m_posFClic && positionBaliseEstValide()) /* Clic gauche en un point et aucune balise ne sera cachée
                                                                 par la création d'une balise à cet endroit*/
            ajouterBalise(); // Nouvelle balise à cet endroit
        else
        {
            m_baliseF = getBalisePointee(m_posFClic); // Stoque la position de la fin du trait
            if(m_baliseD != -1 && m_baliseF != -1 && m_baliseF != m_baliseD) /* Si le début du clic était bien
                                                                               sur une balise et la fin sur une
                                                                               autre*/
                ajouterLiaison(); // On trace le trait entre les deux balises
        }
        break;
    case Qt::RightButton: // En cas de clic droit
        anuler(); // On annule la dernière action
        break;
    }
    m_boutonClique = Qt::NoButton; // Oubli du bouton cliqué
    m_posDClic = m_posFClic; // Evite les affichages de traits vers l'ancienne position de la souris

    // Rafraichissement de l'image
    update();
}

void Simulateur::paintEvent(QPaintEvent *) // Gestion de l'affichage
{
    QPainter painter;
    QColor c_stylo(150,0,150);
    QColor c_trajet(0, 150, 0);
    int i;

    // Début du dessin sur le simulateur
    painter.begin(this);

    QPen stylo(c_stylo, 1);
    QPen trajet(c_trajet, 2);

    painter.setPen(stylo);

    painter.drawLine(m_posDClic, m_posFClic); /* Ligne reliant les points du début et de la fin du clic :
                                                Si une balise était pointée au début, le début du trait y est
                                                    centré
                                                Sinon, la position de fin est ramenée sur celle du début :
                                                    pas de ligne*/

    for(i = 0; i < m_liaisons.size(); i++)
        painter.drawLine(m_liaisons[i]); // Dessin des lignes mémorisées : les couloirs

    painter.setPen(trajet);

    for(i = 0; i < m_trajet.size(); i++)
        painter.drawLine(m_trajet[i]); // Dessin du trajet sélectionné

    painter.setPen(stylo);

    for(i=0; i<m_positions.size(); i++)
        painter.drawPicture(m_positions[i], m_images[i]); // Dessin des balises, les intersections

    painter.end();
}

void Simulateur::cheminRecu(const Itineraire i) // Réception d'un chemin calculé
{
    m_chemins.append(i); // Ajout de i à la liste des itinéraires calculés

    tailleCheminMini = i.getChemin().size(); // Rectification du chemin le plus court

    emit cheminTermine(i); // Envoie l'itinéraie à la fenêtre principale
}
