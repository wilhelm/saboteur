QT += widgets

SOURCES += \
    simulateur.cpp \
    main.cpp \
    itineraire.cpp \
    balise.cpp \
    fenetrePrincipale.cpp \
    chemin.cpp

HEADERS += \
    simulateur.h \
    itineraire.h \
    balise.h \
    fenetrePrincipale.h \
    chemin.h
