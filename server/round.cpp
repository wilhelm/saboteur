#include "round.h"
#include <cstring>
#include "../net/types.h"

#include <fstream>
#include "../client/card.h"
#include "../client/pathCard.h"
#include "../client/arrivalCard.h"
#include "../client/placeActionCard.h"
#include "../client/playerActionCard.h"
#include "../client/board.h"
#include "../net/server.h"
#include <list>
#include <QString>

using namespace net;
using namespace Client;

namespace server{

Round::Round(int nbPlayers, std::vector<Player*> players, Server* s)
{
    debug=true;
    Debug("Begin Round Round");
    this->s = s;
    this->nbPlayers = nbPlayers;
    this->players = players;
    assignRoles();
    fillAllCards();
    this->heap = createHeap(67);
    assignHands();
    this->board = new Board();
    this->hasEnded = 0;
    this->minersWon = 0;
    int temp = rand() % nbPlayers;
    TurnNumber t = temp;
    this->turn = temp;

    /// create objects for the broadcast
    Debug("Preparing to init round");
    std::vector<PlayerInfo> playersToSend;
    for(int i = 0; i < nbPlayers; i++){
        Debug("Setting info for player " + QString::number(i));
        Debug("His id is : " + QString::number(players.at(i)->getID()));
        playersToSend.push_back(PlayerInfo(players.at(i)->getID(), players.at(i)->getName(), (NumCards)(players.at(i)->getHand().size()),
                                           (GoldAmount)(players.at(i)->getGold()), players.at(i)->getBlocked(0), players.at(i)->getBlocked(1), players.at(i)->getBlocked(2)));
    }
    std::vector<PlayerInfo> const pTS = playersToSend;

    std::vector<PlayedCard> const boardToSend;

    NumCards nbDeckToSend = (NumCards)heap.size();


    for (int i = 0; i < nbPlayers; i++){
        Debug("Setting hand to send for player " + QString::number(i));
        std::set<CardID> hand = players.at(i)->getHand();
        std::vector<CardID> handToSendTemp;
        for(std::set<CardID>::iterator it = hand.begin(); it != hand.end(); it ++){
            if(*it == 0) {Debug("FOUND CARD 0 IN Round()");}
            handToSendTemp.push_back(*it);
        }

        std::vector<CardID> const handToSend = handToSendTemp;
        Debug("Sending init message to all players");
        /// broadcast
        s->sendResendAllMessage(players.at(i)->getID(), pTS, boardToSend, nbDeckToSend, handToSend, players.at(i)->getIsSaboteur(), t);
    }
    Debug("End Round Round");
}

Round::~Round(){
    Debug("Begin Round ~Round");
    delete s;
    delete allCards;
    delete allPathCards;
    delete allPlaceActionCards;
    delete allPlayerActionCards;
    delete board;
    for(std::vector<Player*>::iterator it = players.begin(); it != players.end(); it++){
        delete *it;
    }
    Debug("End Round ~Round");
}

int Round::getNbPlayers()
{
    return this->nbPlayers;
}

std::vector<Player*> Round::getPlayers()
{
    return this->players;
}

int Round::getNbHeap()
{
    return heap.size();
}

Card Round::getCard(CardID c){
    return allCards[c-5];
}

PathCard Round::getPathCard(CardID c){
    return allPathCards[c-5];
}

PlaceActionCard Round::getPlaceActionCard(CardID c){
    return allPlaceActionCards[c-63];
}

PlayerActionCard Round::getPlayerActionCard(CardID c){
    return allPlayerActionCards[c-45];
}

Board* Round::getBoard()
{
    return this->board;
}

TurnNumber Round::getTurn()
{
    return this->turn;
}

bool Round::getHasEnded()
{
    return hasEnded;
}

bool Round::getMinersWon(){
    return minersWon;
}


std::list<int> Round::fisherYatesShuffle(int k, int n){
    Debug("Begin Round fisherYatesShuffle");
	srand (time(NULL));
	int *a = new int[k];
	a[0] = 0;
    for (int i = 1; i < k; i++){
		int r = rand()%(i+1);
        a[i] = i;
		a[i] = a[r];
		a[r] = i; 
	}
    for (int i = k; i < n; i++){
		int r = rand()%(i+1);
		if(r<k) a[r]=i;
	}
	std::list<int> Q =  *new std::list<int>() ; 
    for (int i = 0; i < k; i++){
		Q.push_back(a[i]);
        //Debug(QString::number(a[i]));
	}
	delete[] a;
    Debug("End Round fisherYatesShuffle");
	return Q;
}

void Round::fillAllCards(){
    Debug("Begin Round fillAllCards");
	/// reading the file containing all the cards
    allCards = new Client::Card[71];
    allArrivalCards = new Client::ArrivalCard[3];
    allPathCards = new Client::PathCard[40];
    allPlayerActionCards = new Client::PlayerActionCard[18];
    allPlaceActionCards = new Client::PlaceActionCard[9];
    //Debug("server::Round::fillAllCards() : before for");
    for (int i = 1; i < 72; i++){
        //Debug(QString::number(i));
        if (i == 1){
            allCards[i-1] = *new Client::PathCard(i);
            Debug(QString::number(allCards[i-1].id));
        }
        else if (i <= 4) {
            Client::ArrivalCard card = *new Client::ArrivalCard(i);
            allArrivalCards[i-2]=card;
            allCards[i-1] = card;

		}
        else if(i <= 44) {
            Client::PathCard card = *new Client::PathCard(i);
            allPathCards[i-5] = card;
            allCards[i-1] = card;
		}
        else if (i <= 62) {
            Client::PlayerActionCard card = *new Client::PlayerActionCard(i);
            allCards[i-1] = card;
            allPlayerActionCards[i-45] = card;
		}
		else{
            Client::PlaceActionCard card = *new Client::PlaceActionCard(i);
            allCards[i-1] = card;
            allPlaceActionCards[i-63] = card;
		}
	}
    Debug("End Round fillAllCards");
}

std::list<Client::Card> Round::createHeap(const int& k)
{
    Debug("Begin Round createHeap");
	std::list<Client::Card> res = *new std::list<Client::Card>();
    std::list<int> Q = fisherYatesShuffle(k,67);

    for (std::list<int>::iterator it = Q.begin(); it != Q.end(); it++ ){
        Debug("Card choosen : " + QString::number(*it + 5));
        res.push_back(allCards[*it + 5 - 1]); /// (not including arrival and departure cards)
    }
    Debug("End Round createHeap");
	return res;

}


void Round::reactionCardOnBoard(PlayerID p, CardID c, Coords coords, Orientation ori){
    qDebug() << "Begin Round reactionCardOnBoard";
    Player *it = players.at(p);
    if (it->getHand().find(c)==it->getHand().end()) {
        qDebug() << "Card is not proprety of the player";
        s->sendRefusalMessage(p);
        gatherAndSendInfo(p);
    }
    else {
        if(c<45){/// PathCard
            PathCard card = getPathCard(c);
            if (!board->checkMoveAuthorization(coords, &card, ori)|| board->checkDeleteAuthorization(coords)){/// can't reach the coords or there is already a card
                Debug("The location is not accessible");
                s->sendRefusalMessage(p);
                gatherAndSendInfo(p);
            }
            else{
                Debug("Move authorized");
                board->update(coords, &card, ori);
                s->sendPlayCardOnBoardMessage(p, c, coords, ori);
                getWinner(it, c, coords, ori);
                finalizeTurn(it, p, c);

            }
        }
        else{/// PlaceActionCard
            PlaceActionCard card = getPlaceActionCard(c);
            if(card.collapse){/// collapse card
                if(!board->checkDeleteAuthorization(coords)){/// no card to collapse
                    qDebug() << "Tried to collapse an inexisting tunnel";
                    s->sendRefusalMessage(p);
                    gatherAndSendInfo(p);
                }
                else{
                    qDebug() << "Collapse action authorized";
                    board->deleteCard(coords);
                    s->sendPlayCardOnBoardMessage(p, c, coords, ori);
                    finalizeTurn(it, p, c);
                }
            }
            else{/// vision card
                if (!(coords == Coords(8,2) || coords == Coords(8,0) || coords == Coords(8,-2))){
                    qDebug() << "tried to see an unauthorized card";
                    s->sendRefusalMessage(p);
                    gatherAndSendInfo(p);
                }
                else{
                    qDebug() << "Vision authorized";
                    CardID id = board->getBoard()[coords]->id;
                    s->sendRevealCardMessage(p, coords, (id == 2));
                    s->sendPlayCardOnBoardMessage(p, c, coords, ori);
                    id ++;
                    finalizeTurn(it, p, c);
                }
            }

        }
    }

    Debug("End Round reactionCardOnBoard");
}


void Round::reactionCardOnPlayer(PlayerID p, CardID c, PlayerID victim){
    Debug("Begin Round reactionCardOnPlayer");
    Player *player = players.at(p);

    if (player->getHand().find(c)==player->getHand().end()) {
        s->sendRefusalMessage(p);
        gatherAndSendInfo(p);
    }

    PlayerActionCard card = getPlayerActionCard(c);

    Player *it = players.at(victim);
    if (card.destroyed){
        for (int i = 0; i < 3; i++){
            if(card.affected[i] && it->getBlocked(i)){
                s->sendRefusalMessage(p);
                gatherAndSendInfo(p);
            }
            if(card.affected[i] && !(it->getBlocked(i))){
                it->setBlocked(i);
                s->sendPlayCardOnPlayerMessage(p, c, victim);
                finalizeTurn(player, p, c);
            }
        }
    }
    else{
        for (int i = 0; i < 3; i++){
            if(card.affected[i]){
                it->setFree(i);
                s->sendPlayCardOnPlayerMessage(p, c, victim);
                finalizeTurn(player, p, c);
            }
        }
    }
    Debug("End Round reactionCardOnPlayer");
}

void Round::reactionDiscard(PlayerID p, CardID c) {
    Debug("Begin Round reactionDiscard");
    Player *player = players.at(p);

    if (player->getHand().find(c) == player->getHand().end()){ //if the player is not in the game or doesn't have the card
        s->sendRefusalMessage(p);
        gatherAndSendInfo(p);
    }
    else{
        s->sendDiscardCardMessage(p);
        finalizeTurn(player, p, c);
    }
    Debug("End Round reactionDiscard");
}

void Round::gatherAndSendInfo(PlayerID p){
    Debug("Begin Round gatherAndSendInfo");
    /// players
    std::vector<PlayerInfo> playersToSend;
    for (int i = 0; i < nbPlayers; i++){
        playersToSend.push_back(PlayerInfo(players.at(i)->getID(), players.at(i)->getName(), (NumCards)(players.at(i)->getHand().size()),
                                           (GoldAmount)(players.at(i)->getGold()), players.at(i)->getBlocked(0), players.at(i)->getBlocked(1), players.at(i)->getBlocked(2)));
    }
    std::vector<PlayerInfo> const pTS = playersToSend;

    /// board
    std::vector<PlayedCard> boardToSend;
    for (map<net::Coords, PathCard*>::iterator it = this->board->getBoard().begin(); it != this->board->getBoard().end(); it++){
        if(it->second->id == 0){Debug("FOUND CARD 0 IN GATHER AND SEND INFO 1");}
        boardToSend.push_back(PlayedCard(it->second->id, it->first, (this->board->getSens().find(it->first))->second));
    }
    std::vector<PlayedCard> const bTS = boardToSend;

    NumCards cardsToSend = this->getNbHeap();

    std::vector<net::CardID> handToSendTemp;
    for(set<net::CardID>::iterator it = players.at(p)->getHand().begin(); it != players.at(p)->getHand().end(); it++){
        handToSendTemp.push_back(*it);
    }
    std::vector<net::CardID> const handToSend = handToSendTemp;

    s->sendResendAllMessage(p, pTS, bTS, cardsToSend, handToSend, players.at(p)->getIsSaboteur(), this->turn);
    Debug("End Round gatherAndSendInfo");
}

void Round::assignRoles(){
    Debug("Begin Round assignRoles");
    int nbSaboteurs=0;
    int nbMinors=0;
    //debug
    if(nbPlayers == 1) {
        nbSaboteurs = 0;
        nbMinors = 1;
    }
    if(nbPlayers==3){nbSaboteurs=1; nbMinors=3;}
    if(nbPlayers==4){nbSaboteurs=1; nbMinors=4;}
    if(nbPlayers==5){nbSaboteurs=2; nbMinors=4;}
    if(nbPlayers==6){nbSaboteurs=2; nbMinors=5;}
    if(nbPlayers==7){nbSaboteurs=3; nbMinors=5;}
    if(nbPlayers==8){nbSaboteurs=3; nbMinors=6;}
    if(nbPlayers==9){nbSaboteurs=3; nbMinors=7;}
    if(nbPlayers==10){nbSaboteurs=4; nbMinors=7;}
    for (int i = 0; i < nbPlayers; i++){
        int n = 1 + rand() % (nbSaboteurs+nbMinors);
        if (n <= nbSaboteurs){
            Debug("This player is Saboteur");
            players.at(i)->setSaboteur();
            nbSaboteurs--;
        }
        else {
            Debug("This player is a miner");
            players.at(i)->setMinor();
            nbMinors--;
        }
    }
    Debug("End Round assignRoles");
}

void Round::assignHands(){
    Debug("Begin Round assignHands");
    int nbCardsInHand=0;
    if (nbPlayers >= 3 && nbPlayers <= 5)
        nbCardsInHand=6;
    if (nbPlayers >= 6 && nbPlayers <= 7)
        nbCardsInHand=5;
    if (nbPlayers >= 8 && nbPlayers <= 10)
        nbCardsInHand=4;
    for (int i = 0; i < nbPlayers ;i++){
        for (int j = 0; j < nbCardsInHand; j++){
            players.at(i)->addHand(heap.front().id);
            heap.pop_front();
        }
    }
    Debug("End Round assignHands");
}

bool Round::checkHasEnded() {
    Debug("Begin Round checkHasEnded");
    bool AllHandsEmpty = true;
    for(std::vector<Player*>::iterator it = players.begin() ; it<players.end() ; it++){
        if ((*it)->getHand().size()!= 0){
            AllHandsEmpty = false;
        }
    }
    if((getNbHeap() == 0 && AllHandsEmpty) || getMinersWon()){
        Debug("End Round checkHasEnded");
        return true;
    }
    else {
        Debug("End Round checkHasEnded");
        return false;
    }
}


void Round::drawCard(PlayerID p){
    Debug("Begin Round drawCard");
    if(heap.size() > 0){
        Card newCard = heap.front();
        if(newCard.id == 0){qDebug("FOUND CARD WITH ID 0 IN DRAWCARD");}
        heap.pop_front();
        s->sendDrawMessage(p, newCard.id);
    }
    Debug("End Round drawCard");
}

void Round::finalizeTurn(Player *player, PlayerID p, CardID c){
    Debug("Begin Round finalizeTurn");
    if (checkHasEnded()) {
        assignGold();
    } else {
        player->getHand().erase(c);
        drawCard(p);
        newTurn();
    }
    Debug("End Round finalizeTurn");
}

void Round::newTurn(){
    Debug("Begin Round newTurn");
    turn ++;
    s->sendTurnMessage(turn % nbPlayers);
    Debug("End Round newTurn");
}

int Round::getRelativePosition(Coords coords, Coords objectiveCoords){
    Debug("Begin Round getRelativePosition");
    if (coords.x - objectiveCoords.x == -1 && coords.y == objectiveCoords.y){
        Debug("End Round getRelativePosition");
        return 0; ///west
    }
    if (coords.y - objectiveCoords.y == 1 && coords.x == objectiveCoords.x){
        Debug("End Round getRelativePosition");
        return 1; ///north
    }
    if (coords.x - objectiveCoords.x == 1 && coords.y == objectiveCoords.y){
        Debug("End Round getRelativePosition");
        return 2; ///east
    }
    if (coords.y - objectiveCoords.y == -1 && coords.x == objectiveCoords.x){
        Debug("End Round getRelativePosition");
        return 3; ///south
    }
    else{
        Debug("End Round getRelativePosition");
        return 4;
    }
}

bool Round::reachFrom(CardID c, Coords coords, Orientation ori, Coords objectiveCoords){
    Debug("Begin Round reachFrom");
    int pos = getRelativePosition(coords, objectiveCoords);
    if (pos < 4 && (PathCard(c).open[(pos+2+2*ori)%4]) && (PathCard(c).open[4])){
        Debug("End Round reachFrom");
        return true;
    }
    else{
        Debug("End Round reachFrom");
        return false;
    }
}

void Round::updateArrival(int pos, PathCard* objectiveCard, Coords objectiveCoords){
    Debug("Begin Round updateArrival");
    if (!(board->getAvailable(objectiveCoords))){
        bool ori = false;
        if(!(objectiveCard->open[pos])){
            ori = true;
        }
        board->update(objectiveCoords, objectiveCard, ori);
        /// send signal of availability to client
        s->sendPlayCardOnBoardMessage(net::EVERY_PLAYER, objectiveCard->id, objectiveCoords, ori);
    }
    Debug("End Round updateArrival");
}

void Round::getWinner(Player* p, CardID c, Coords coords, Orientation ori){
    Debug("Begin Round getWinner");
    PathCard* top = board->getBoard()[Coords(8,2)];
    PathCard* middle = board->getBoard()[Coords(8,0)];
    PathCard* bottom = board->getBoard()[Coords(8,-2)];

    if(reachFrom(c, coords, ori, Coords(8,2))){
        if(top->id==2){
            minersWon = true;
            firstWinner = p;
            s->sendPlayCardOnBoardMessage(net::EVERY_PLAYER, top->id, Coords(8,2), 0);
            Debug("End Round getWinner");
            return;
        }
        else{
            updateArrival(getRelativePosition(coords, Coords(8,2)), top, Coords(8,2));
            Debug("End Round getWinner");
            return;
        }
    }

    if (reachFrom(c, coords, ori, Coords(8,0))){
        if (middle->id == 2){
            minersWon = true;
            firstWinner = p;
            s->sendPlayCardOnBoardMessage(net::EVERY_PLAYER, middle->id, Coords(8,0), 0);
            Debug("End Round getWinner");
            return;
        }
        else{
            updateArrival(getRelativePosition(coords, Coords(8,0)), middle, Coords(8,0));
            Debug("End Round getWinner");
            return;
        }

    }

    if(reachFrom(c, coords, ori, Coords(8,-2))){
        if (bottom->id == 2){
            minersWon = true;
            firstWinner = p;
            s->sendPlayCardOnBoardMessage(net::EVERY_PLAYER, bottom->id, Coords(8,-2), 0);
            Debug("End Round getWinner");
            return;
        }
        else{
            updateArrival(getRelativePosition(coords, Coords(8,-2)), bottom, Coords(8,-2));
            Debug("End Round getWinner");
            return;
        }
    }
}

void Round::assignGold(){
    Debug("Begin Round assignGold");
    std::vector<Player*> winners;

    if (minersWon){
        ///compute the winners
        winners.push_back(firstWinner);
        std::vector<Player*> players2 = sortPlayers();
        for (int i = 1; i < nbPlayers; i++){
            if (!(players2.at(i)->getIsSaboteur())){
                winners.push_back(players2.at(i));
            }
        }
        ///draw randomly the goldCards' values
        int goldCards[one + two + three];
        for (int i = 0; i < one; i++)
            goldCards[i] = 1;
        for (int i = one; i < two; i++)
            goldCards[i] = 2;
        for (int i = two; i < three; i++)
            goldCards[i] = 3;
        std::vector<int> drawnCards;
        while (drawnCards.size() != nbPlayers){
            int temp = rand() % (one+two+three);
            if (std::find(drawnCards.begin(), drawnCards.end(), temp) != drawnCards.end()){
                drawnCards.push_back(temp);
            }
        }

        std::vector<int> values;
        for(int i=0; i<nbPlayers; i++){
            values.push_back(goldCards[drawnCards.at(i)]);
        }
        std::sort(values.begin(), values.end());


        ///assign the values to the miners
        for(int i=0; i < values.size(); i++){
            winners.at(i % (winners.size()))->addGold(values.back());
            values.pop_back();
        }
    }

    else{ /// if the saboteurs have won

        ///compute the winners
        for(int i=0; i<nbPlayers; i++){
            if (players.at(i)->getIsSaboteur()){
                winners.push_back(players.at(i));
            }
        }


        int gold = 0;
        if((signed int)winners.size() == 1) gold = 4;
        if((signed int)winners.size() == 2) gold = 3;
        if((signed int)winners.size() == 3) gold = 3;
        if((signed int)winners.size() == 4) gold = 2;

        for(int i= 0; i < winners.size(); i++){
            winners.at(i)->addGold(gold);
        }

    }

    Debug("Begin Round assignGold");
    return;
}

std::vector<Player*> Round::sortPlayers(){
    Debug("Begin Round sortPlayers");
    std::vector<Player*> res = std::vector<Player*>();
    res.push_back(firstWinner);

    for(int i = firstWinner->getTurn()+1 ; i < firstWinner->getTurn() + nbPlayers; i++){
        ///look for the player whose turnNumber is i%nbPlayers in players and push it in res
        for(int j=0; j<nbPlayers; j++){
            if (players.at(j)->getTurn() == i%nbPlayers){
                res.push_back(players.at(j));
                break;
            }
        }
    }
    Debug("End Round sortPlayers");
    return res;
}


void Round::Debug(QString message){
    if(debug){
        qDebug() << message;
    }
}


}
