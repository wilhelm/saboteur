#include "player.h"

using namespace Client;
using namespace std;

namespace server {

Player::Player(net::PlayerID id, const string& pseudo) {
    debug = true;
    Debug("Begin Player Player");
    this->id = id;
    this->turnID = id;
    this->name = pseudo;
    set<net::CardID>* handSet = new set<net::CardID>();
    this->hand = *handSet;
    this->isSaboteur = false;
    this->blocked = new bool[3];
    for (int i = 0; i < 3; i++){
        blocked[i] = false;
    }
    this->gold = 0;
    Debug("End Player Player");
}


Player::~Player() {
    Debug("Begin Player ~Player");
    Debug("End Player ~Player");
}

void Player::setRole(bool isSab){
    isSaboteur = isSab;
}

net::PlayerID Player:: getID() const{
    return id;
}

int Player::getTurn(){
    return turnID;
}

string Player::getName(){
    return name;
}

set<net::CardID> Player::getHand(){
    return hand;
}

void Player::addHand(net::CardID c){
    Debug("Begin Player addHand "+ QString::number(c));
    hand.insert(c);
    Debug("End Player addHand");
}

void Player::removeHand(net::CardID c){
    Debug("Begin Player removeHand");
    hand.erase(c);
    Debug("End Player removeHand");
}

bool Player::getIsSaboteur(){
    return isSaboteur;
}

void Player::setSaboteur(){
    isSaboteur = 1;
}

void Player::setMinor(){
    isSaboteur = 0;
}

bool Player::getBlocked(int i){
    return blocked[i];
}

void Player::setBlocked(int i){
    blocked[i] = 1;
}

void Player::setFree(int i){
    blocked[i] = 0;
}

bool Player::getBlocked(){
    return (blocked[0]||blocked[1]||blocked[2]);
}

int Player::getGold(){
    return gold;
}

void Player::addGold(int i){
    gold += i;
}

bool Player::operator==(const Player& rplayer){
    return (this->id == rplayer.getID());
}

void Player::Debug(QString message){
    if(debug){
        qDebug() << message;
    }
}

}
