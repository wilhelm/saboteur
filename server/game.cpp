#include "game.h"
#include <stdlib.h>

using namespace net;

namespace server{

Game::Game(int nbRounds, int nbPlayers, Server* s){

    this->s = s;
    this->nbRounds = nbRounds;
    this->nbPlayers = nbPlayers;
    std::vector<Player*> v;
    this->players = v;
    achievedRounds = 0;
    currentPlayers = 0;

    /// connect the slots and the signals
    connect(s, SIGNAL(newHelloMessage(PlayerID, std::string)), SLOT(reactionHelloMessage(PlayerID,std::string)));
    connect(s, SIGNAL(newPlayCardOnBoardMessage(PlayerID, CardID, Coords, Orientation)),
            SLOT(reactionCardOnBoard(PlayerID, CardID, Coords, Orientation)));
    connect(s, SIGNAL(newPlayCardOnPlayerMessage(PlayerID, CardID, PlayerID)),
            SLOT(reactionCardOnPlayer(PlayerID, CardID, PlayerID)));
    connect(s, SIGNAL(newDiscardCardMessage(PlayerID, CardID)), SLOT(reactionDiscard(PlayerID, CardID)));
    connect(s, SIGNAL(newPleaseResendAllMessage(PlayerID)), SLOT(reactionResendAllMessage(PlayerID)));

}

Game::~Game(){

    delete s;
    delete currentRound;

}

void Game::reactionHelloMessage(net::PlayerID p, std::string const & pseudo){
    qDebug() << "begin server::Game::reactionHelloMessage";
    if(currentPlayers == nbPlayers){
        /// The game is full
        //TODO: send message "the game is full"
    }
    else{
        qDebug() << "Pushing back players";
        /// We add the player to the current players list and update the current number of players
        players.push_back(new Player(p,pseudo));
        qDebug() << "Pushed back players";
        currentPlayers++;
        if(currentPlayers == nbPlayers){
            /// Assign a random playing order - commented because we suppose here that turnID = playerID
            /**
            for(int i=0 ; i<nbPlayers ; i++){
                int j = rand() % nbPlayers;
                Player aux = players[j];
                players[j]=players[i];
                players[i]=aux;
            }
            **/

            /// The first round can start
            currentRound = new Round(nbPlayers, players, s);
            s->sendGameMessage(nbRounds);
        }

    }
    qDebug() << "end server::Game::reactionHelloMessage";
}

void Game::reactionCardOnBoard(PlayerID p, CardID c, Coords coords, Orientation ori){
    qDebug()<<"[Server:Game] forwarding reactionOnBoard to Round";
   if(currentRound != NULL){ /// if a round is being played - else does nothing
       currentRound->reactionCardOnBoard(p, c, coords, ori);
       checkRound();
   }
   return;
}

void Game::reactionCardOnPlayer(PlayerID p, CardID c, PlayerID victim){

    if(currentRound != NULL){ /// if a round is being played - else does nothing
        currentRound->reactionCardOnPlayer(p, c, victim);
        checkRound();
    }
    return;

}

void Game::reactionDiscard(PlayerID p, CardID c){

    if(currentRound != NULL){ /// if a round is being played - else does nothing
        currentRound->reactionDiscard(p, c);
        checkRound();
    }
    return;

}

void Game::reactionResendAllMessage(PlayerID p){

    if(currentRound != NULL){ /// if a round is being played - else does nothing
        currentRound->gatherAndSendInfo(p);
    }
    return;

}

void Game::checkRound(){
    if(currentRound->getHasEnded()){
        players = currentRound->getPlayers(); /// update gold
        delete this->currentRound;
        achievedRounds ++;
        if(achievedRounds < nbRounds){
            /// we go for another round
            currentRound = new Round(nbPlayers, players, this->s);
            s->sendGameMessage(nbRounds - achievedRounds);
        }
        else{
            winner = getWinner();
            s->sendWinnerMessage(winner->getID());
            delete this;
        }
    }
    return;

}

Player* Game::getWinner(){   /// computes argmax(player.gold) - we first assume that in case of equality, the lowest id wins

    int gmax = 0;
    int res = 0;

    for(int i=0; i<nbPlayers; i++){
        if(players[i]->getGold() > gmax){
            gmax = players[i]->getGold();
            res = i;
        }
    }

    return players[res];
}
}
