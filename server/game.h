#ifndef GAME_H
#define GAME_H

#include <QObject>
#include "player.h"
#include "round.h"
#include "../net/types.h"

using net::PlayerID;
using net::CardID;
using net::Coord;
using net::Coords;
using net::Orientation;

namespace server{

class Game : public QObject {
    Q_OBJECT
public:
    Server* s;
    int nbRounds;                   /// number of rounds wanted
    int nbPlayers;                  /// number of players wanted
    std::vector<Player*> players;    /// players for this game
    Round* currentRound;            /// current round being played (or to be played soon)
    int currentPlayers;             /// number of players currently present
    int achievedRounds ;            /// number of rounds that have already been played in this game
    Player* winner;

    Game(int nbRounds, int nbPlayers, Server* s);            /// constructor
    ~Game();                                                                                        /// destructor

public slots:
    void reactionHelloMessage(PlayerID p, std::string const & pseudo);                              /// reaction upon receiving a hello message
    void reactionCardOnBoard(PlayerID p, CardID c, Coords coords, Orientation ori);                 /// reaction upon receiving an action where a card is being played on the board(path/crumbling/vision)
    void reactionCardOnPlayer(PlayerID p, CardID c, PlayerID victim);                               /// reaction upon receiving an action where a card is being played on a player(blocking/freeing)
    void reactionDiscard(PlayerID p, CardID c);                                                     /// reaction upon receiving an action where a card is discarded
    void reactionResendAllMessage(PlayerID p);                                                      /// reaction upon receiving a "please resend all information" message

    void checkRound();                                                                              /// computes if the currentRound has just ended and updates the game if necessary
    Player* getWinner();                                                                             /// computes the winner of the game (using argmax gold)
};

}

#endif // GAME_H

