#ifndef PLAYER_H
#define PLAYER_H

#include <string>
#include <set>
#include "../client/card.h"
#include "../net/types.h"
#include <QDebug>
#include <QString>

using namespace std;
using namespace Client;

namespace server{

class Player {

private:

    bool debug;
    net::PlayerID id;                                           /// id of the player - for network
    int turnID;                                                 /// order of player in play turn
    string name;                                                /// name of the player                                      /// hand of the player during a round
    bool isSaboteur;                                            /// 1 if the player is a saboteur
    bool* blocked;                                              /// array of 3 booleans: 0=pickaxe/1=light/2=trolley
    int gold;                                                   /// amount of gold accumulated through the rounds

public:

    set<net::CardID> hand;

    Player(net::PlayerID p, const string& pseudo);                     /// constructor
    ~Player();                                                  /// desctructor

    net::PlayerID getID() const;

    int getTurn();

    void setRole(bool isSab);

    string getName();

    set<net::CardID> getHand();
    void addHand(net::CardID c);
    void removeHand(net::CardID c);

    bool getIsSaboteur();
    void setSaboteur();                                         /// sets isSaboteur to 1
    void setMinor();                                            /// sets isSaboteur to 0

    bool getBlocked(int i);
    void setBlocked(int i);                                     /// sets blocked[i] to 1
    void setFree(int i);                                        /// sets blocked[i] to 0

    bool getBlocked();

    int getGold();
    void addGold(int i);

    bool operator==(const Player& rplayer);                    /// equality operator overload, based on equality of PlayerIDs

    void Debug(QString message);

};

}

#endif // PLAYER_H
