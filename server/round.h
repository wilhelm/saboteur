#ifndef ROUND_H
#define ROUND_H

#include "player.h"

#include <fstream>
#include "../client/card.h"
#include "../client/pathCard.h"
#include "../client/arrivalCard.h"
#include "../client/placeActionCard.h"
#include "../client/playerActionCard.h"
#include "../client/board.h"
#include "../net/server.h"
#include <list>
#include <stdlib.h> /// for rand
#include <time.h> /// to initialize the random seed
#include <QDebug>

using namespace net;

namespace server{

class Round{

public:
    Round();
    Round(int nbPlayers, std::vector<Player*> players, Server* s);       /// constructor

    ~Round();                                                           /// desctructor

    int getNbPlayers();

    std::vector<Player*> getPlayers();

    int getNbHeap();

    Card getCard(CardID c);
    PathCard getPathCard(CardID c);
    PlaceActionCard getPlaceActionCard(CardID c);
    PlayerActionCard getPlayerActionCard(CardID c);

    Board* getBoard();

    TurnNumber getTurn();

    bool getHasEnded();
    bool getMinersWon();

    ///following methods are called within slots of the Game class
    void reactionCardOnBoard(PlayerID p, CardID c, Coords coords, Orientation ori);                 /// reaction upon receiving an action where a card is being played on the board(path/crumbling/vision)
    void reactionCardOnPlayer(PlayerID p, CardID c, PlayerID victim);                               /// reaction upon receiving an action where a card is being played on a player(blocking/freeing)
    void reactionDiscard(PlayerID p, CardID c);                                                     /// reaction upon receiving an action where a card is discarded

    void gatherAndSendInfo(PlayerID p);                                                             /// reaction upon receiving a "please resend all information" message

    void fillAllCards();                                                                            /// fill allcards from Client/DonneesCatres
    std::list<int> fisherYatesShuffle(int k, int n);                                                /// create a list of distinct k integers from {0..n-1}, used in createHeap
    std::list<Client::Card> createHeap(const int& k);                                                      /// initialises the heap

    void assignRoles();                                                                             /// assigns roles to players (saboteur/non-saboteur)
    void assignHands();                                                                             /// assigns hands to players                                                                      /// find the card which CardID is c

    bool checkHasEnded();                                                                           /// computes if the round has ended - gold found or no more cards to play
    void drawCard(PlayerID p);
    void finalizeTurn(Player *player, PlayerID p, CardID c);
    void newTurn();                                                                                 /// launches a new turn
    int getRelativePosition(Coords coords, Coords objectiveCoords);                                 /// returns the relative position of coords compared to objectiveCoords: 0=west, 1=north, 2=east, 3=south, 4=not neighbour
    bool reachFrom(CardID c, Coords coords, Orientation ori, Coords objectiveCoords);               /// tests if we can reach the objective card from the card placed on the board
    void updateArrival(int pos, PathCard* objectiveCard, Coords objectiveCoords);                   /// if the objective card is a stone, updates its availability and orientation to match the previously played card
    void getWinner(Player* p, CardID c, Coords coords, Orientation ori);                             /// computes if the miners have won when a PathCard is played by player p and updates minersWon
    void assignGold();                                                                              /// assigns gold to the winners
    std::vector<Player*> sortPlayers();                                                             /// sort players by their order, starting from firstWinner
    void Debug(QString message);

private:

    bool debug;
    Server* s;
    int nbPlayers;
    std::vector<Player*> players;
    std::list<Card> heap;
    Client::Card* allCards;
    Client::PathCard* allPathCards;
    Client::PlaceActionCard* allPlaceActionCards;
    Client::PlayerActionCard* allPlayerActionCards;
    Client::ArrivalCard* allArrivalCards;
    Board* board;
    TurnNumber turn;                /// counter for the current turn
    bool hasEnded;                  /// 1 if the round has ended
    bool minersWon;                 /// 1 if the miners have won the game ; else the saboteurs have
    Player* firstWinner;             /// player who has found the gold (if it is the case)
    const int one = 16;
    const int two = 8;
    const int three = 4;

};



}


#endif // ROUND_H

