QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = server
TEMPLATE = app
QMAKE_CXXFLAGS += -std=c++11


SOURCES  += main.cpp \
    player.cpp \
    game.cpp \
    round.cpp \
    ../client/arrivalCard.cpp \
    ../client/board.cpp \
    ../client/card.cpp \
    ../client/pathCard.cpp \
    ../client/placeActionCard.cpp \
    ../client/playerActionCard.cpp \
    ../net/server.cpp

HEADERS  += \
    player.h \
    game.h \
    round.h \
    ../net/server.h \
    ../client/arrivalCard.h \
    ../client/board.h \
    ../client/card.h \
    ../client/pathCard.h \
    ../client/placeActionCard.h \
    ../client/playerActionCard.h \
    ../net/types.h

FORMS    +=
