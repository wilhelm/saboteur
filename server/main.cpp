#include <QApplication>
#include "../net/types.h"
#include "../net/server.h"
#include "game.h"

using namespace net;
using namespace server;

/// This executable must be called with the following format:
/// .. portNumber nbRounds nbPlayers

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    bool debug = true;

    quint16 portNumber = 3333;
    int nbRounds = 0;
    int nbPlayers = 0;

    portNumber = atoi(argv[1]);
    nbRounds = atoi(argv[2]);
    nbPlayers = atoi(argv[3]);

    qDebug() << "server launched for portNumber = " << portNumber << ", nbRounds" << nbRounds << ", nbPlayers = " << nbPlayers;

    Server* s = new Server(debug);

    new Game(nbRounds, nbPlayers, s);

    s->listen(portNumber);

    return a.exec();
}
