#ifndef OTHERPLAYER_H
#define OTHERPLAYER_H

#include <QString>

namespace Client {
class OtherPlayer {
public:
    OtherPlayer(int id, int nbCards, QString name, bool* blocked);
    ~OtherPlayer();

    int id;                                                     /// playerId of the player
    int nbCards;
    QString name;                                           /// name of the player
    bool* blocked;                                              /// array of 3 booleans: trolley/pickaxe/light
    //bool isBlocked() { return blocked[0] || blocked[1] || blocked[2]; }    /// 1 if the player is blocked
};
}
#endif // OTHERPLAYER_H
