#include "../client/pathCard.h"
#include "../client/placeActionCard.h"
#include "../client/playerActionCard.h"
#include "../net/client.h"
#include "../client/myClient.h"
#include "../client/card.h"
#include "../client/board.h"
#include "../client/otherplayer.h"
#include "../net/types.h"
#include <QDebug>
#include <QThread>
#include <QTime>
#include <QCoreApplication>
#include <QEventLoop>

namespace Client {
MyClient::MyClient(QObject *parent)
    : QObject(parent)
{
    qDebug() << "MyClient MyClient start";
    clientConnect = new net::Client(true);
    myGUI = new UI::SaboteurGUI();

    myGUI->show();
    this->gameStarted = false;

    ///Connections our signal, GUI's slots.
    QObject::connect(this, SIGNAL(sendCardDrawn(int)), myGUI, SLOT(cardDrawn(int)));
    QObject::connect(this, SIGNAL(sendConfirmCardPlayed(bool)), myGUI, SLOT(confirmCardPlayed(bool)));
    QObject::connect(this, SIGNAL(sendSetLastCardPlayed(int, Client::Position*, bool)), myGUI, SLOT(setLastCardPlayed(int, Client::Position*, bool)));
    QObject::connect(this, SIGNAL(sendUpdateReachablePositions(Client::Position*)), myGUI, SLOT(updateReachablePositions(Client::Position*)));
    QObject::connect(this, SIGNAL(sendUpdateBoard(int, Client::Position*, bool)), myGUI, SLOT(updateBoard(int,Client::Position*,bool)));
    QObject::connect(this, SIGNAL(sendPlayerBlockedStatus(bool*, int)), myGUI, SLOT(playerBlockedStatus(bool*, int)));
    QObject::connect(this, SIGNAL(sendPlayerHavingTurn(int, int)), myGUI, SLOT(playerHavingTurn(int, int)));
    QObject::connect(this, SIGNAL(sendDisplayGame(QString, int, int, int, map<int,QString>, int)), myGUI, SLOT(displayGame(QString, int, int, int, map<int,QString>, int)));
    QObject::connect(this, SIGNAL(sendAddPlayerToGame(QString)), myGUI, SLOT(addPlayerToGame(QString)));
    QObject::connect(this, SIGNAL(sendConnectionErrorSlot()), myGUI, SLOT(connectionErrorSlot()));
    QObject::connect(this, SIGNAL(sendCreateGame(QString, int, int, int)), myGUI, SLOT(createGame(QString, int, int, int)));
    QObject::connect(this, SIGNAL(sendUpdateRole(bool)), myGUI, SLOT(updateRole(bool)));
    QObject::connect(this, SIGNAL(sendUpdateNbCards(int,int)), myGUI, SLOT(updatePlayerNbCards(int,int)));

    ///Connections our slots, GUI's signals.
    QObject::connect(myGUI, SIGNAL(tryPlayCard(int, bool, int, int, Client::Position*)), this, SLOT(receivedTryPlayCard(int, bool, int, int, Client::Position*)));
    QObject::connect(myGUI, SIGNAL(dumpCard(int)), this, SLOT(receivedDumpCard(int)));

    QObject::connect(myGUI, SIGNAL(createGameSignal(QString, int, int, int)), this, SLOT(receivedCreateGameSignal(QString, int, int, int)));
    QObject::connect(myGUI, SIGNAL(joinGameSignal(QString, QString, int)), this, SLOT(receivedJoinGameSignal(QString, QString, int)));
    QObject::connect(myGUI, SIGNAL(gameDisplayed()), this, SLOT(receivedGameDisplayed()));

    ///Connections with net signals
    QObject::connect(clientConnect, SIGNAL(newGameMessage(net::RoundNumber)), this, SLOT(receivedNewGameMessage(net::RoundNumber)));
    QObject::connect(clientConnect, SIGNAL(newWinnerMessage(net::PlayerID)), this, SLOT(receivedNewWinnerMessage(net::PlayerID)));
    QObject::connect(clientConnect, SIGNAL(newDiscardCardMessage(net::PlayerID)), this, SLOT(receivedNewDiscardCardMessage(net::PlayerID)));
    QObject::connect(clientConnect, SIGNAL(newTurnMessage(net::TurnNumber)), this, SLOT(receivedNewTurnMessage(net::TurnNumber)));
    QObject::connect(clientConnect, SIGNAL(newDrawMessage(net::PlayerID,net::CardID)), this, SLOT(receivedNewDrawMessage(net::PlayerID,net::CardID)));
    QObject::connect(clientConnect, SIGNAL(newPlayCardOnBoardMessage(net::PlayerID,net::CardID,net::Coords,net::Orientation)), this, SLOT(receivedNewPlayCardOnBoardMessage(net::PlayerID,net::CardID,net::Coords,net::Orientation)));
    QObject::connect(clientConnect, SIGNAL(newPlayCardOnPlayerMessage(net::PlayerID,net::CardID,net::PlayerID)), this, SLOT(receivedNewPlayCardOnPlayerMessage(net::PlayerID,net::CardID,net::PlayerID)));
    QObject::connect(clientConnect, SIGNAL(newRevealCardMessage(net::Coords,bool)), this, SLOT(receivedNewRevealCardMessage(net::Coords,bool)));
    QObject::connect(clientConnect, SIGNAL(newRefusalMessage()), this, SLOT(receivedNewRefusalMessage()));
    QObject::connect(clientConnect, SIGNAL(newResendAllMessage(net::PlayerID,std::vector<net::PlayerInfo>,std::vector<net::PlayedCard>,net::NumCards,std::vector<net::CardID>,net::RoleID,net::TurnNumber)), this, SLOT(receivedNewResendAllMessage(net::PlayerID,std::vector<net::PlayerInfo>,std::vector<net::PlayedCard>,net::NumCards,std::vector<net::CardID>,net::RoleID,net::TurnNumber)));



    qDebug() << "MyClient MyClient end";
}

MyClient::~MyClient() { }


MyClient::MyClient(string name, bool identity, QObject *parent) : QObject(parent) {
    this->name = name;
    this->identity = identity;
    this->nbTurn = 0;
    this->gameStarted = false;
}

map<net::PlayerID,OtherPlayer*> MyClient::getOtherPlayers() {
    return otherPlayers;
}

void MyClient::playPathCard(PathCard* c, net::Coords pos, bool sens) {
    if(board->checkMoveAuthorization(pos,c,sens)) {
        net::CardID cardId = c->id;
        net::Coord pos1 = pos.x;
        net::Coord pos2 = pos.y;
        net::Coords coords = net::Coords(pos1,pos2);
        net::Orientation orientation = sens;
        clientConnect->sendPlayCardOnBoardMessage(cardId, coords, orientation);
    } else {
        emit MyClient::sendConfirmCardPlayed(false);
    }
}

void MyClient::playActionPlace(PlaceActionCard* c, net::Coords pos) {
    if(board->checkDeleteAuthorization(pos)) {
        net::CardID cardId = c->id;
        net::Coord pos1 = pos.x;
        net::Coord pos2 = pos.y;
        net::Coords coords = net::Coords(pos1,pos2);
        net::Orientation orientation = true;
        clientConnect->sendPlayCardOnBoardMessage(cardId, coords, orientation);
    } else {
        emit MyClient::sendConfirmCardPlayed(false);
    }
}

void MyClient::playActionPlayer(PlayerActionCard* c, int numOtherPlayer) {
    net::CardID cardId = c->id;
    clientConnect->sendPlayCardOnPlayerMessage(cardId, numOtherPlayer);
}

/// SLOTS
void MyClient::receivedTryPlayCard(int cardId, bool orientation, int playerId, int blockage, Position *position)
{
    //TODO remove blockage from parameters
    blockage = blockage;
    qDebug() << "[Client Try] Receive Try Play Card Id : " << QString::number(cardId);
    int type = Card::getCardType(cardId);
    net::Coords myCoords = net::Coords(position->first, position->second);

    if (type == 2) // Carte chemin
    {
        qDebug() << "Card type 2";
        PathCard* myCard = new PathCard(cardId);
        bool authorized = board->checkMoveAuthorization(myCoords, myCard, orientation);
        if (authorized)
        {
            qDebug() << "Autorized. Send to server";
            clientConnect->sendPlayCardOnBoardMessage(cardId, myCoords, orientation);
        }
        else
        {
            qDebug() << "Permission denied. Return to GUI";
            emit sendConfirmCardPlayed(false);
            qDebug() << "End return error";
        }
    }

    if (type == 3) // Carte ActionJoueur
    {
        qDebug() << "Card type 3";
        PlayerActionCard* myCard = new PlayerActionCard(cardId);
        OtherPlayer* player = otherPlayers[playerId];
        bool authorized = true;
        if (myCard->destroyed)
        {
            for (int i = 0; i<3; i++)
            {
                if (myCard->affected[i] && player->blocked[i])
                    authorized = false;
            }
        }
        if (authorized)
        {
            qDebug() << "Autorized. Send to server";
            clientConnect->sendPlayCardOnPlayerMessage(cardId, playerId);
        }
        else
        {
            qDebug() << "Permission denied. Return to GUI";
            emit sendConfirmCardPlayed(false);
            qDebug() << "End return error";
        }
    }

    if (type == 4) // Carte ActionLieu
    {
        qDebug() << "Card type 4";
        PlaceActionCard* myCard = new PlaceActionCard(cardId);

        //éboulement
        if (myCard->collapse)
        {
            bool authorized = board->checkDeleteAuthorization(myCoords);
            if (authorized)
            {
                qDebug() << "Autorized. Send to server";
                clientConnect->sendPlayCardOnBoardMessage(cardId, myCoords, true);
            }
            else
            {
                qDebug() << "Permission denied. Return to GUI";
                emit sendConfirmCardPlayed(false);
                qDebug() << "End return error";
            }
        }

        //vision
        else
        {
            bool authorized = (myCoords == net::Coords(8, 2) || myCoords == net::Coords(8, 0) || myCoords == net::Coords(8, -2));
            if (authorized)
            {
                qDebug() << "Autorized. Send to server";
                clientConnect->sendPlayCardOnBoardMessage(cardId, myCoords, true);
            }
            else
            {
                qDebug() << "Permission denied. Return to GUI";
                emit sendConfirmCardPlayed(false);
                qDebug() << "End return error";
            }
        }
    }

    return;

}
    
void MyClient::dumpCard(Card *c) {
    net::CardID cardId = c->id;
    clientConnect->sendDiscardCardMessage(cardId);
}

void MyClient::receivedNewGameMessage(net::RoundNumber r){
    qDebug()<<"[MyClient] receive new game message";
    map<int,QString> otherPlayersInfo;
    for(map<net::PlayerID, OtherPlayer*>::iterator it = otherPlayers.begin();
        it != otherPlayers.end(); it++){
        otherPlayersInfo[(int)(it->first)] = it->second->name;
    }
    QString pseudo = QString::fromStdString(myPlayer->getName());
    int myId = myPlayer->getID();
    emit MyClient::sendDisplayGame(pseudo, myId, nbPlayers, r, otherPlayersInfo, board->nbCartesPioche);
}

void MyClient::receivedGameDisplayed(){
    qDebug() << "[Client] Coucou start for display";
    for(set<net::CardID>::iterator it = myPlayer->hand.begin(); it != myPlayer->hand.end(); it++){
        qDebug() << "[Client] Send Card Drawn : " << QString::number((int)(*it));
        emit sendCardDrawn((int)(*it));
    }
    qDebug() << "[Client] Send update role";
    emit sendUpdateRole(myPlayer->getIsSaboteur());
    qDebug() << "[Client] Send update turn";
    for(map<net::PlayerID, OtherPlayer*>::iterator it = otherPlayers.begin(); it != otherPlayers.end(); it++){
        qDebug() << "[Client] Send update turn for player ID : " << QString::number(it->second->id);
        emit sendUpdateNbCards(it->second->id,it->second->nbCards);
    }
    receivedNewTurnMessage(nbTurn);
}

void MyClient::receivedNewTurnMessage(net::TurnNumber T) {
    qDebug() << "[Client] Receive send turn message, Turn : " << QString::number((int)T);
    nbTurn = T;
    int playerId = T%nbPlayers;
    int nbCards;
    if(playerId == myPlayer->getID()) {
        nbCards = myPlayer->hand.size();
    } else {
        nbCards = otherPlayers[playerId]->nbCards;
    }
    qDebug() << "[Client] Going to send turn message";
    emit MyClient::sendPlayerHavingTurn(playerId, nbCards);
}

void MyClient::receivedNewDrawMessage(net::PlayerID p, net::CardID c) {
    if(p == myPlayer->getID()) {
        myPlayer->addHand(c);
        emit MyClient::sendCardDrawn(c);
    } else {
        otherPlayers[p]->nbCards += 1;
    }
}

void MyClient::receivedNewPlayCardOnBoardMessage(net::PlayerID p, net::CardID c, net::Coords coords,
                               net::Orientation ori) {

    int type = Card::getCardType(c);

    Position* pos = new Position();
    pos->first = coords.x;
    pos->second = coords.y;
    if(type == 2) {             // Carte chemin
        PathCard* pc = new PathCard(c);
        board->updateAvailabilityAfterPath(coords, pc, ori);
    } else if(type == 4) {      // Carte action
        board->deleteCard(coords);
        board->updateAvailabilityAfterCollapse();
    }

    if(p == myPlayer->getID()) {
        myPlayer->removeHand(c);
    } else {
        otherPlayers[p]->nbCards -= 1;
    }

    emit MyClient::sendUpdateBoard(c, pos, ori);
    emit MyClient::sendSetLastCardPlayed(c, pos, ori);
    emit MyClient::sendConfirmCardPlayed(true);
}

void MyClient::receivedNewPlayCardOnPlayerMessage(net::PlayerID p, net::CardID c, net::PlayerID victim) {
    PlayerActionCard* pac = new PlayerActionCard(c);
    OtherPlayer* vic = otherPlayers[victim];
    if(pac->destroyed) {
        for(int i=0; i<3; i++) {
            vic->blocked[i] = !(pac->affected[i]);
        }
    }
    else {
        for(int i=0; i<3; i++) {
            vic->blocked[i] = (pac->affected[i]);
        }
    }

    if(p == myPlayer->getID()) {
        myPlayer->removeHand(c);
    } else {
        otherPlayers[p]->nbCards -= 1;
    }
    emit MyClient::sendPlayerBlockedStatus(vic->blocked, victim);
    emit MyClient::sendConfirmCardPlayed(true);
}

void MyClient::receivedNewDiscardCardMessage(net::PlayerID p){
    if(p == myPlayer->getID()) {
    } else {
        otherPlayers[p]->nbCards -= 1;
    }
    //emit Myclient::sendSetLastCardPlayed(-1, NULL, NULL);
}

void MyClient::receivedNewRevealCardMessage(net::Coords c, bool isGold){
    //(ArrivalCard*)board->board[c]->known = true;
    //TODO
    isGold = isGold;
    int myCardId = board->board[c]->id;
    Position* pos = new Position();
    pos->first = c.x;
    pos->second = c.y;
    emit MyClient::sendUpdateBoard(myCardId,pos,board->sens[c]);
}

void MyClient::receivedNewRefusalMessage() {
    emit MyClient::sendConfirmCardPlayed(false);
}

void MyClient::receivedNewResendAllMessage(net::PlayerID id,
                         std::vector<net::PlayerInfo> const & players,
                         std::vector<net::PlayedCard> const & boardNew,
                         net::NumCards still_in_deck,
                         std::vector<net::CardID> const & cards_in_hand,
                         net::RoleID role,
                         net::TurnNumber turn)
{
    qDebug() << "[Client] Receive New Resend All Message";
    /// Player informations update
    qDebug() << "[Client] My ID : " << QString::number(id);
    this->nbPlayers = players.size();
    for(std::vector<net::PlayerInfo>::const_iterator it = players.begin(); it != players.end(); ++it)
    {
        if (id==it->id) {
            myPlayer = new Player(id,it->pseudo);
            qDebug() << "[Client] My psudo is : " << (it->pseudo).c_str();
        }
        bool* blockedTemp = new bool[3];
        blockedTemp[0] = it->isPickaxeBroken;
        blockedTemp[1] = it->isCarriageBroken;
        blockedTemp[2] = it->isLampBroken;
        otherPlayers[(int)(it->id)] = new OtherPlayer((int)(it->id), (int)it->num_cards, QString::fromStdString(it->pseudo), blockedTemp);
    }

    /// Board update
    board = new Board();
    qDebug() << "[Client] End of init board";
    for(std::vector<net::PlayedCard>::const_iterator it = boardNew.begin(); it != boardNew.end(); ++it)
    {
        PathCard* c = new PathCard((int)(it->id));
        board->update(it->pos, c, (bool)(it->ori));
        qDebug() << "[Client] Updating board : " << QString::number(it->id);
    }
    qDebug() << "[Client] End of update board";
    /// Pioche
    board->nbCartesPioche = (int) still_in_deck;
    qDebug() << "[Client] Update still in deck";
    /// Hand update
    //std::set<net::CardID> s(cards_in_hand.begin(), cards_in_hand.end());
    //myPlayer->hand = s;
    qDebug() << QString::number(cards_in_hand.size());

    std::set<net::CardID> *s = new std::set<net::CardID>();

    for(std::vector<net::CardID>::const_iterator it = cards_in_hand.begin(); it != cards_in_hand.end(); it++){
        qDebug() << "Card In Hand Id : "<<QString::number(*it);
        s->insert(*it);
        //emit sendCardDrawn((int)*it);
    }
    qDebug()<<"End Update Cards in Hand";
    myPlayer->hand = *s;

    /// Role update
    myPlayer->setRole((bool)role);
    qDebug() << "[Client] Update role";
    /// Turn number update
    this->nbTurn = (int)turn;
    qDebug() << "[Client] Update turn";
    /// TODO: continue (still_in_deck, cards_in_hand, role, turn)
}

void MyClient::receivedNewWinnerMessage(net::PlayerID player) {
    /// TODO
    player = player;
}

void MyClient::receivedDumpCard(int cardId) {
    clientConnect->sendDiscardCardMessage(cardId);
    //myPlayer->removeHand(cardId);
}

void MyClient::receivedCreateGameSignal(QString userName, int nbPlayers, int nbRounds, int port) {
    ///TODO
    // Create the server
    qDebug() << "Creating server";
    string program = "../build-server/server " + to_string(port) + " " + to_string(nbRounds) + " " + to_string(nbPlayers)+" &";

    int i = system(program.c_str());
    qDebug() << "return code : " << i;
    delay();
    // récupérer l'ipAdress d'une manière ou d'une autre

    // Connect to the server
    qDebug() << "Connecting to server";
    clientConnect->connect(QString("127.0.1.1"), port, userName.toStdString());
    qDebug() << "Connection end";
    emit sendCreateGame(userName, nbPlayers, nbRounds, port);


}

void MyClient::receivedJoinGameSignal(QString userName, QString ipAddress, int port)
{
    qDebug() << "begin Client::MyClient::receivedJoinGameSignal";
    const std::string pseudo = userName.toStdString();
    clientConnect->connect(ipAddress, port, pseudo);
    emit sendCreateGame(userName, nbPlayers, 3, port);
    //emit sendAddPlayerToGame(userName);
    qDebug() << "end Client::MyClient::receivedJoinGameSignal";
}

void MyClient::delay()
{
    QTime dieTime= QTime::currentTime().addSecs(1);
    while (QTime::currentTime() < dieTime)
        QCoreApplication::processEvents(QEventLoop::AllEvents, 100);
}

}
