#ifndef ARRIVALCARD_H
#define ARRIVALCARD_H
#include "../client/pathCard.h"
#include <QFile>
#include <QStringList>
#include <QString>
#include <QMessageBox>
#include <QTextStream>

#include "../net/types.h"

namespace Client { 

class ArrivalCard : public PathCard
{
public:
	bool gold; /// wether the arrival card contains gold
    bool known;///wether we can see the card
    ArrivalCard();
    ~ArrivalCard();
    ArrivalCard(net::CardID id, std::vector<bool> open, bool gold);
    ArrivalCard(net::CardID id);
};

}

#endif /// ARRIVALCARD_H
