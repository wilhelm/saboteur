#include "../client/placeActionCard.h"

namespace Client {
PlaceActionCard::~PlaceActionCard()
{

}
PlaceActionCard::PlaceActionCard()
{

}

PlaceActionCard::PlaceActionCard(net::CardID id, bool collapse)
{
	this->id = id;
	this->collapse = collapse;
}

PlaceActionCard::PlaceActionCard(net::CardID id)
{
	QFile file("../client/DonneesCarte.txt");
    if(!file.open(QIODevice::ReadOnly)) {
   		 QMessageBox::information(0, "error", file.errorString());
	}
	QTextStream in(&file);
	QString line;
    for(int i=0;i<id;i++){
		line = in.readLine();
	}

	QStringList fields = line.split(" ");
    std::vector<bool> open = std::vector<bool>();
	QString T = QString::fromStdString("Eboulement");
	bool collapse = !fields.at(6).compare(T);
    
	this->id = id;
	this->collapse = collapse;
	file.close();
}

}
