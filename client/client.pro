QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
CONFIG(release, debug|release):DEFINES += QT_NO_DEBUG_OUTPUT

DEFINES += "QT_MAJOR_VERSION=$$QT_MAJOR_VERSION"

TARGET = client
TEMPLATE = app
CONFIG += console c++11
QMAKE_CXXFLAGS += -std=c++11

SOURCES += main.cpp \
    myClient.cpp \
    board.cpp \
    card.cpp \
    pathCard.cpp \
    placeActionCard.cpp \
    playerActionCard.cpp \
    arrivalCard.cpp \
    otherplayer.cpp \
    ../GUI/saboteurGUI/saboteurGUI.cpp \
    ../GUI/beginForm/beginform.cpp \
    ../GUI/user/user.cpp \
    ../GUI/platal/platal.cpp\
    ../GUI/user/leftuserfield.cpp \
    ../GUI/user/iconesblocage.cpp \
    ../GUI/otherPlayers/otherPlayers.cpp \
    ../GUI/user/userField.cpp \
    ../GUI/cardGUI/cardGUI.cpp \
    ../GUI/GUI/gui.cpp \
    ../GUI/user/iconturn.cpp \
    ../GUI/createGame.cpp \
    ../GUI/user/blockicon.cpp \
    ../net/client.cpp \
    ../server/player.cpp

HEADERS += myClient.h \
    board.h \
    card.h \
    pathCard.h \
    placeActionCard.h \
    playerActionCard.h \
    arrivalCard.h \
    otherplayer.h \
    ../GUI/saboteurGUI/saboteurGUI.h \
    ../GUI/beginForm/beginform.h \
    ../GUI/user/user.h \
    ../GUI/platal/platal.h \
    ../GUI/user/leftuserfield.h \
    ../GUI/user/iconesblocage.h \
    ../GUI/otherPlayers/otherPlayers.h \
    ../GUI/user/userField.h \
    ../GUI/cardGUI/cardGUI.h \
    ../GUI/GUI/gui.h \
    ../GUI/user/iconturn.h \
    ../GUI/createGame.h \
    ../GUI/user/blockicon.h \
    ../net/client.h \
    ../net/types.h \
    ../server/player.h

