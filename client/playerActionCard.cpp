#include "../client/playerActionCard.h"

namespace Client {

PlayerActionCard::PlayerActionCard()
{

}

PlayerActionCard::PlayerActionCard(net::CardID id, bool destroyed, bool affected[])
{
	this->id = id;
	this->destroyed = destroyed;
	this->affected = affected;
}

PlayerActionCard::PlayerActionCard(net::CardID id)
{
	QFile file("../client/DonneesCarte.txt");
    if(!file.open(QIODevice::ReadOnly)) {
   		 QMessageBox::information(0, "error", file.errorString());
	}
	QTextStream in(&file);
	QString line;
    for(int i=0;i<id;i++){
		line = in.readLine();
	}

	QStringList fields = line.split(" ");
    std::vector<bool> open = std::vector<bool>();
	QString T = QString::fromStdString("Reparation");
	QString Tr = QString::fromStdString("True");
	bool destroyed = fields.at(6).compare(T);
    bool* affected = new bool[3];
    for(int i=0;i<3;i++){
        affected[i] = !fields.at(2*(i+4)).compare(Tr);
    }


	this->id = id;
	this->destroyed = destroyed;
	this->affected = affected;
	file.close();
}

PlayerActionCard::~PlayerActionCard(){
}
}
