#ifndef MYCLIENT_H
#define MYCLIENT_H

#include "../client/placeActionCard.h"
#include "../client/playerActionCard.h"
#include "../client/pathCard.h"
#include "../client/card.h"
#include "../server/player.h"
#include "../client/board.h"
#include "../client/otherplayer.h"
#include "../net/client.h"
#include "../GUI/saboteurGUI/saboteurGUI.h"
#include <string>
#include <set>
#include <map>
#include <QObject>

using namespace std;
using namespace server;

namespace Client {

class MyClient : public QObject {
    Q_OBJECT
public:
    MyClient(QObject *parent = 0);
    ~MyClient();
    MyClient(string name, bool identity, QObject *parent = 0);

    map<net::PlayerID,OtherPlayer*> getOtherPlayers();

    ///
    /// \brief playPathCard: put a card at a postion and
    ///        check if this operation is legal.
    /// \param c: card to put on the plateau.
    ///
    void playPathCard(PathCard* c, net::Coords pos, bool sens);

    ///
    /// \brief playActionPlayer: affect an action to a player
    /// \param c: card to play
    /// \param numOtherPlayer: the player affected
    ///
    void playActionPlayer(PlayerActionCard* c, int numOtherPlayer);

    ///
    /// \brief playActionPlace: play an Eboulement card
    /// \param c: card to play
    ///
    void playActionPlace(PlaceActionCard* c, net::Coords pos);

    void dumpCard(Card* c);

    Player* myPlayer;           ///the player corresponding to the client
    Board* board;
    net::Client* clientConnect;
    int nbTurn;
    int nbPlayers;
    GUI* myGUI;

public slots:
    /// Slots from GUI.
    void receivedTryPlayCard(int cardId, bool orientation, int playerId, int blockage, Client::Position* position = nullptr);
    void receivedDumpCard(int cardId);
    void receivedCreateGameSignal(QString userName, int nbPlayers, int nbRounds, int port);
    void receivedJoinGameSignal(QString userName, QString ipAddress, int port);

    /// Slots from server.
    void receivedNewGameMessage(net::RoundNumber R);
    void receivedNewTurnMessage(net::TurnNumber T);
    void receivedNewDrawMessage(net::PlayerID p, net::CardID c);
    void receivedNewPlayCardOnBoardMessage(net::PlayerID p, net::CardID c, net::Coords coords,
                                   net::Orientation ori);
    void receivedNewPlayCardOnPlayerMessage(net::PlayerID p, net::CardID c, net::PlayerID victim);
    void receivedNewDiscardCardMessage(net::PlayerID p);
    void receivedNewRevealCardMessage(net::Coords c, bool isGold);
    void receivedNewRefusalMessage();
    void receivedNewResendAllMessage(net::PlayerID id,
                             std::vector<net::PlayerInfo> const & players,
                             std::vector<net::PlayedCard> const & board,
                             net::NumCards still_in_deck,
                             std::vector<net::CardID> const & cards_in_hand,
                             net::RoleID role,
                             net::TurnNumber turn);
    void receivedNewWinnerMessage(net::PlayerID player);
    void delay();
    void receivedGameDisplayed();

signals:
    /// Signals to GUI
    void sendPlayerHavingTurn(int playerId, int nbCards);
    void sendPlayerBlockedStatus(bool* blocked, int playerId);
    void sendUpdateBoard(int cardId, Client::Position* p, bool orientation);
    void sendUpdateReachablePositions(Client::Position *positions);
    void sendSetLastCardPlayed(int cardId, Client::Position *position = nullptr, bool orientation = true);
    void sendConfirmCardPlayed(bool confirmation);
    void sendCardDrawn(int cardId);
    void sendDisplayGame(QString pseudo, int myId, int nbPlayers, int nbRounds, map<int,QString> otherPlayersInfo, int nbCardDeck = 100);
    void sendAddPlayerToGame(QString pseudo);
    void sendConnectionErrorSlot();
    void sendCreateGame(QString userName, int nbPlayers, int nbRounds, int port);
    void sendUpdateRole(bool isSaboteur);
    void sendUpdateNbCards(int playerId, int nbCards);
    /// Signals to Server are methods of clientConnect

private:

    ///
    /// \brief otherPlayers: a hashmap containing
    /// the other players' id(num) as key and
    /// other information as value.

    map<net::PlayerID, OtherPlayer*> otherPlayers;
    string name;
    bool identity;
    bool gameStarted;
};
}
#endif // MYCLIENT_H
