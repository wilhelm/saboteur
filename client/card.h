#ifndef CARD_H
#define CARD_H

#include "../net/types.h"
#include <QFile>
#include <QStringList>
#include <QString>
#include <QMessageBox>
#include <QTextStream>

namespace Client {

class Card
{
public:
    net::CardID id;
    int type;
    Card();
    ~Card();
    Card(net::CardID id);
    void Debug(QString s);
    bool debug = true;

    static int getCardType(int id); /// correspondance type de renvoi: type de carte
                                    /// 0: Depart, 1: Arrivée, 2: chemin, 3: ActionJoueur,
                                    /// 4: actionLieu.

    static QString getPathToImage(int id); ///returns a QString conaining the path to image 


};

bool operator<(Card const &c1, Card const &c2);

}
#endif /// CARD_H
