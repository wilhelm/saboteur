#ifndef PLAYERACTIONCARD_H
#define PLAYERACTIONCARD_H
#include "../client/card.h"
#include <QFile>
#include <QStringList>
#include <QString>
#include <QMessageBox>
#include <QTextStream>

namespace Client{

class PlayerActionCard : public Card
{
public:
	bool destroyed; ///casser, reparer
    bool* affected; ///  pioche, lampe, chariot
    PlayerActionCard();
    ~PlayerActionCard();
    PlayerActionCard(net::CardID id, bool destroyed, bool affected[]);
    PlayerActionCard(net::CardID id);

};

}

#endif /// PLAYERACTIONCARD_H
