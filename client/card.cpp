#include "../client/card.h"
#include <QDebug>

namespace Client {
Card::Card()
{
    Debug("begin Client::Card::Card()");

    id = 0;
    type = 0;
    Debug("end Client::Card::Card()");
}
Card::Card(net::CardID id)
{
    Debug("begin Client::Card::Card(net::CardID id)");
    this->id = id;
    this->type = 0;
    Debug("end Client::Card::Card(net::CardID id)");
}
Card::~Card(){

}

int Card::getCardType(int id){
    if(id==1) return 0;
    else if(id<=4) return 1;
    else if(id<=44) return 2;
    else if(id<=62) return 3;
    else return 4;

}

QString Card::getPathToImage(int id){
    QFile file("../client/DonneesCarte.txt");
	if(!file.open(QIODevice::ReadOnly)) {
   		 QMessageBox::information(0, "error", file.errorString());
	}
	QTextStream in(&file);
	QString line;
	for(int i=0;i<id;i++){
		line = in.readLine();
	}

    qDebug() << "==========> CARD " << id << " : '" << line << "'";
    QStringList fields = line.split(" ");
    return fields.at(4);

}

void Card::Debug(QString s) {
    if(debug) {
        qDebug() << s;
    }
}

bool operator<(Card const &c1, Card const &c2)
{
    return (c1.id < c2.id);
}


}
