#ifndef PATHCARD_H
#define PATHCARD_H
#include "../client/card.h"
#include <vector>
#include <QFile>
#include <QStringList>
#include <QString>
#include <QMessageBox>
#include <QTextStream>

namespace Client{

class PathCard : public Card
{
public:
    std::vector<bool> open; /// west, north, east, south, centre
    PathCard();
    PathCard(net::CardID id, std::vector<bool> open);
    PathCard(net::CardID id);
    ~PathCard();
};

}

#endif /// PATHCARD_H
