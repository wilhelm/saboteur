#include "../client/arrivalCard.h"
#include "../net/types.h"
#include <QFile>
#include <QMessageBox>
#include <QTextStream>
#include <QDebug>

namespace Client {
ArrivalCard::ArrivalCard()
{

}

ArrivalCard::~ArrivalCard() {

}

ArrivalCard::ArrivalCard(net::CardID id, std::vector<bool> open, bool gold)
{
	this->id = id;
    this->open = open;
	this->gold = gold;
}

ArrivalCard::ArrivalCard(net::CardID id)
{
    QFile file("../client/DonneesCarte.txt");
    if(!file.open(QIODevice::ReadOnly)) {
         QMessageBox::information(0, "error", file.errorString());
    }
	QTextStream in(&file);
	QString line;
    for(int i=0;i<id;i++){
		line = in.readLine();
	}

	QStringList fields = line.split(" ");
    std::vector<bool> open = std::vector<bool>();
    QString T = QString::fromStdString("True");
	for(int i=3;i<8;i++){
        open.push_back(!fields.at(2*i).compare(T));
	}
    bool gold = !fields.at(16).compare(T);


	this->id = id;
	this->open = open;
	this->gold = gold;
	this->known = false;
	file.close();
}

}
