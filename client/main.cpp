#include <QApplication>
#include <QDebug>
#include "board.h"
#include "pathCard.h"
#include "../GUI/saboteurGUI/saboteurGUI.h"
#include "../net/types.h"
#include "myClient.h"

using namespace std;
using namespace Client;

void testPlateau1();

int main(int argc, char *argv[])
{
    qDebug() << "Main begin.";
    QApplication a(argc, argv);
    //testPlateau1();
    //UI::SaboteurGUI w;
    //w.show();

    new MyClient();
    qDebug() << "Main end.";

    return a.exec();
}

void testPlateau1()
{
    Board* b = new Board();

    net::Coords position1 = net::Coords(0,1);
    net::Coords position2 = net::Coords(0,2);
    vector<bool> v{true, true, true, true, true};

    PathCard* myCard = new PathCard(1, v);
    PathCard* myCard2 = new PathCard(2,v);


    cout << " Autorisation de placer c1 ?" << b->checkMoveAuthorization(position1, myCard, true) << endl;
    cout << " Placing card 1 position (0,1)..." << endl;
    b->update(position1, myCard, true);

    cout << " Autorisation de placer c2 ?" << b->checkMoveAuthorization(position2, myCard2, true) << endl;

    cout << " Placing card 2 position (0,2)..." << endl;
    b->update(position2, myCard2, true);

    cout << " Can we delete card 1 ? " << endl;
    cout << b->checkDeleteAuthorization(position1);

    cout << " Deleting card 1..." << endl;
    b->deleteCard(position1);

    cout << " Is card 2 still available ? " << b->getAvailable(position2) << endl;

}
