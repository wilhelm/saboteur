#include "../client/board.h"
#include "../client/placeActionCard.h"
#include "../client/pathCard.h"
#include "arrivalCard.h"
#include <QDebug>

namespace Client {
Board::Board()
{
    Debug("begin Client::Board::Board");
    net::Coords initial = net::Coords(0,0);
    net::Coords a1 = net::Coords(8,0);
    net::Coords a2 = net::Coords(8,3);
    net::Coords a3 = net::Coords(8,-3);
    //vector<bool> v{true, true, true, true, true};

    board = std::map<net::Coords, PathCard*>();
    board[initial] = new PathCard(1); // Carte de départ.
    sens = std::map<net::Coords, bool>();

    sens[initial] = true;
    board[a1] = new ArrivalCard(2); // Donc open partout.
    sens[a1] = true;
    board[a2] = new ArrivalCard(3);
    sens[a2] = true;
    board[a3] = new ArrivalCard(4);
    sens[a3] = true;
    available = map<net::Coords, bool>();
    available[initial] = true;
    nbCartesPioche = 40 + 27; // Actions + chemins, avant première
    Debug("end Client::Board::Board");
}
Board::~Board() {

}

void Board::updateAvailabilityAfterPath(net::Coords p, PathCard* c, bool sensJouer)
{
    /**
        The pathCard c is supposed to be correctly placed.
        For each neighbor not available and connected to the card, make her available,
        and if her center is opened, recursively update from her position.
      */

    // if the center is not opened, we update nothing.
    if (c->open[4] == false)
    {
        return;
    }

    // west
    if (c->open[0])
    {
        net::Coords positionGauche = net::Coords(p.x, p.y);
        // If in the other side, west becomes east, north south.
        if (sensJouer) positionGauche.x -= 1;
        else positionGauche.x += 1;

        // If there is a neighbor
        if (board.find(positionGauche) != board.end())
        {
            // If it is not yet available
            if (available.find(positionGauche) == available.end())
            {
                available[positionGauche] = true;
                // If open center
                if (board[positionGauche]->open[4])
                {
                    updateAvailabilityAfterPath(positionGauche, board[positionGauche], sens[positionGauche]);
                }
            }
        }
    }

    // North
    if (c->open[1])
    {
        net::Coords positionHaut = net::Coords(p.x, p.y);
        if (sensJouer)
            positionHaut.y += 1;
        else
            positionHaut.y -= 1;

        if (board.find(positionHaut) != board.end())
        {
            if (available.find(positionHaut) == available.end())
            {
                available[positionHaut] = true;
                // If open center
                if (board[positionHaut]->open[4])
                {
                    updateAvailabilityAfterPath(positionHaut, board[positionHaut], sens[positionHaut]);
                }
            }
        }
    }

    // East
    if (c->open[2])
    {
        net::Coords positionDroite = net::Coords(p.x, p.y);
        if (sensJouer)
            positionDroite.x += 1;
        else
            positionDroite.x -= 1;

        if (board.find(positionDroite) != board.end())
        {
            if (available.find(positionDroite) == available.end())
            {
                available[positionDroite] = true;
                // If open center
                if (board[positionDroite]->open[4])
                {
                    updateAvailabilityAfterPath(positionDroite, board[positionDroite], sens[positionDroite]);
                }
            }
        }
    }

    // south
    if (c->open[3])
    {
        net::Coords positionBas = net::Coords(p.x, p.y);
        if (sensJouer)
            positionBas.y -= 1;
        else
            positionBas.y += 1;

        if (board.find(positionBas) != board.end())
        {
            if (available.find(positionBas) == available.end())
            {
                available[positionBas] = true;
                // If open center
                if (board[positionBas]->open[4])
                {
                    updateAvailabilityAfterPath(positionBas, board[positionBas], sens[positionBas]);
                }
            }
        }
    }
}

void Board::updateAvailabilityAfterCollapse()
{
    /**
      We just played a collapse on net::Coords p.
      Every played card is set to non-available, then we run updateAvailabilityAfterPath
      on the start card to recursively update the whole map.
     */

    // Set cards to non-vailable

    for(map<net::Coords, bool>::iterator it = available.begin() ; it != available.end(); ++it)
    {
        it->second = false;
    }

    net::Coords initial = net::Coords(0,0);
    available[initial] = true;

    updateAvailabilityAfterPath(initial, board[initial], sens[initial]);
}

void Board::deleteCard(net::Coords p)
{
    board.erase(p);
    available.erase(p);
    sens.erase(p);
    updateAvailabilityAfterCollapse();
}

bool Board::checkMoveAuthorization(net::Coords p, PathCard* c, bool sensJouer)
{
    return true;
    qDebug() << "Sens Jouer : " << QString::number(sensJouer);
    qDebug() << "X : " << QString::number(p.x) << " Y : " << QString::number(p.y);
    
    /**
     Checks if the PathCard c can go to net::Coords p in the sens sens : for each neighbour position, checks if :
     - there is an available card at this neighbour position
     - this card is connected to the new one (placed in the the sens sens)
     - there is no problem with other neighbours (path leading noway)
     */

    int turnCard;
    if (sensJouer)
    {
        turnCard = 0;
    }
    else
    {
        turnCard = 2;
    }
    
    qDebug() << "Card c : " << QString::number(c->open[0]) << " "<< QString::number(c->open[1]) << " " << QString::number(c->open[2]) << " "<< QString::number(c->open[3]) << " ";
    //west
    
    net::Coords positionGauche = net::Coords(p.x-1, p.y);

    if(positionGauche.x != 0 || positionGauche.y != 0){//Ce n4est pas le depart
        std::map<net::Coords, Client::PathCard*>::iterator it = board.find(positionGauche);
        if(it != board.end()){
            if((*it).second->open[(2 + sens[positionGauche])%4] != c->open[(0+turnCard)%4]){
                qDebug() << "Reject 0";
                return false;
            }
        }
    }
    else{
        if(!(c->open[(0+turnCard)%4])){
            qDebug() << "Reject 1";
            return false;
        }
    }
    
    //north

    net::Coords positionHaut = net::Coords(p.x, p.y+1);

    if(positionHaut.x != 0 || positionHaut.y != 0){//Ce n4est pas le depart
        std::map<net::Coords, Client::PathCard*>::iterator it = board.find(positionHaut);
        if(it != board.end()){
            if((*it).second->open[(3 + sens[positionGauche])%4] != c->open[(1+turnCard)%4]){
                qDebug() << "Reject 2";
                return false;
            }
        }
    }
    else{
        if(!c->open[(1+turnCard)%4]){
            qDebug() << "Reject 3";
            return false;
        }
    }

    //east

    net::Coords positionDroite = net::Coords(p.x+1, p.y);

    if(positionDroite.x != 0 || positionDroite.y != 0){//Ce n4est pas le depart
        std::map<net::Coords, Client::PathCard*>::iterator it = board.find(positionDroite);
        if(it != board.end()){
            if((*it).second->open[(4 + sens[positionDroite])%4] != c->open[(2+turnCard)%4]){
                qDebug() << "Reject 4";
                return false;
            }
        }
    }
    else{
        if(!c->open[(2+turnCard)%4]){
            qDebug() << "Reject 5";
            return false;
        }
    }

    //south

    net::Coords positionBas = net::Coords(p.x, p.y-1);

    if(positionBas.x != 0 || positionBas.y != 0){//Ce n4est pas le depart
        std::map<net::Coords, Client::PathCard*>::iterator it = board.find(positionBas);
        if(it != board.end()){
            if((*it).second->open[(5 + sens[positionBas])%4] != c->open[(3+turnCard)%4]){
                qDebug() << "Reject 6";
                return false;
            }
        }
    }
    else{
        if(!c->open[(3+turnCard)%4]){
            qDebug() << "Reject 7";
            return false;
        }
    }
    
    return true;
    
}

bool Board::checkDeleteAuthorization(net::Coords p)
{
    // checks whether there is a card at net::Coords p
    // makes sure the "start" and "arrival" cards are not deleted
    return ((p.x != 0 || p.y != 0) && (p.x != 8 || p.y != 0) && (p.x != 8 || p.y != 2) && (p.x != 8 || p.y != -2) && (board.find(p) != board.end()));
    
}

void Board::update(net::Coords p, PathCard* c, bool sensJouer)
{
    //adds the card to the map, declares it available, declares its sens and calls updateAvailabilityAfterPath
    
    board[p] = c;
    sens[p] = sensJouer;
    available[p] = true;
    updateAvailabilityAfterPath(p, c, sensJouer);
}

// ************** GETTERS *************

bool Board::getAvailable(net::Coords p)
{
    return available[p];
}

void Board::Debug(QString s) {
    if(debug) {
        qDebug() << s;
    }
}
}
