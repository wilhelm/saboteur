#include "../client/pathCard.h"
#include <QDebug>

namespace Client {

PathCard::PathCard()
{
    Debug("begin Client::PathCard::PathCard()");
    Debug("end Client::PathCard::PathCard()");
}

PathCard::PathCard(net::CardID id, std::vector<bool> open)
{
    Debug("begin Client::PathCard::PathCard(net::CardID id, std::vector<bool> open)");
    this->id = id;
    this->open = open;
    Debug("end Client::PathCard::PathCard(net::CardID id, std::vector<bool> open)");
}

PathCard::PathCard(net::CardID id)
{
    qDebug() << "begin Client::PathCard::PathCard(net::CardID id) for id " << id;
    QFile file("../client/DonneesCarte.txt");
    if(!file.open(QIODevice::ReadOnly)) {
        qDebug() << file.errorString();
        QMessageBox::information(0, "error", file.errorString());
    }
    QTextStream in(&file);
    QString line;
    for(int i=0;i<id;i++){
        line = in.readLine();
    }

    QStringList fields = line.split(" ");
    std::vector<bool> open = std::vector<bool>();
    QString T = QString::fromStdString("True");
    qDebug() << "Reading data from line. Line size : " << QString::number(fields.size());
    for(int i=3;i<8;i++){
        qDebug() << "field : "+QString::number(i) + " : " + QString::number(!fields.at(2*i).compare(T));
        open.push_back(!fields.at(2*i).compare(T));
    }

    this->id = id;
    this->open = open;

    file.close();
    Debug("end Client::PathCard::PathCard(net::CardID id)");
}

PathCard::~PathCard() {
}
}
