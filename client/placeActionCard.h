#ifndef PLACEACTIONCARD_H
#define PLACEACTIONCARD_H
#include "../client/card.h"
#include <QFile>
#include <QStringList>
#include <QString>
#include <QMessageBox>
#include <QTextStream>

namespace Client{

class PlaceActionCard : public Card
{
public:
	bool collapse;
    ~PlaceActionCard();
    PlaceActionCard();
    PlaceActionCard(net::CardID id, bool collapse);
    PlaceActionCard(net::CardID id);

};

}

#endif /// PLACEACTIONCARD_H
