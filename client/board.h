#ifndef BOARD_H
#define BOARD_H

#include "../client/card.h"
#include "../client/placeActionCard.h"
#include <map>
#include <utility>
#include <iostream>
#include "../client/pathCard.h"
#include "../net/types.h"

using namespace std;
namespace Client {

typedef pair<int,int> Position;

class Board
{
public:
    Board();
    ~Board();

    void Debug(QString s);
    bool debug = true;

    void updateAvailabilityAfterPath(net::Coords p, PathCard* c, bool sensJouer);

    void updateAvailabilityAfterCollapse();

    bool checkMoveAuthorization(net::Coords p, PathCard* c, bool sensJouer);

    /// See if a card is in this net::Coords.
    bool checkDeleteAuthorization(net::Coords p);

    /// Plays a path card.
    void update(net::Coords p, PathCard* c, bool sensJouer);

    /// Deletes a card after collapse.
    void deleteCard(net::Coords p);

    bool getAvailable(net::Coords p);

    /// Accesseurs

    map<net::Coords, PathCard*> getBoard()
    {
        return board;
    }

    map<net::Coords, bool> getSens()
    {
        return sens;
    }

    int nbCartesPioche;

    map<net::Coords, PathCard*> board;
    // Un peu plus lourd, mais vu la taille du plateau, peu d'impact.
    map<net::Coords, bool> sens;

    /// Is the card at this net::Coords available ? (accessible depuis l'origine)
    map<net::Coords, bool> available;

};
}
#endif // BOARD_H
