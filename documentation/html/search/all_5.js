var searchData=
[
  ['game',['Game',['../classserver_1_1Game.html',1,'server']]],
  ['game',['Game',['../classserver_1_1Game.html#a8d344f507b49c4164d8065cf3f2d822f',1,'server::Game']]],
  ['getblocked',['getBlocked',['../classserver_1_1Player.html#aee66fd073fa8b1d993ee7b5ac239a897',1,'server::Player::getBlocked(int i)'],['../classserver_1_1Player.html#a5f7fa067a305d6c4be2ef9c2fb7fff06',1,'server::Player::getBlocked()']]],
  ['getid',['getID',['../classserver_1_1Player.html#a31ab7b3fb2bd8ec161dca6e5875aed45',1,'server::Player']]],
  ['getnbplayers',['getNbPlayers',['../classserver_1_1Round.html#a9195cf863eb21d28f3c6db08fa80f199',1,'server::Round']]],
  ['getwinner',['getWinner',['../classserver_1_1Game.html#ae2967ca41214eef907a4f06496ea7af3',1,'server::Game::getWinner()'],['../classserver_1_1Round.html#a5b76c20a1462ddcc2ccf5a0919c96beb',1,'server::Round::getWinner()']]],
  ['goback',['goBack',['../classUI_1_1SaboteurGUI.html#afca490c93706798bce095bb2ab75bf66',1,'UI::SaboteurGUI']]],
  ['gui',['GUI',['../classGUI.html',1,'GUI'],['../classGUI.html#acb0ba8c6fc121d814d30560e2c29f2fe',1,'GUI::GUI()']]]
];
