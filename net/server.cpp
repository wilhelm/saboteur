#include "../net/server.h"
#include "../net/types.h"
#include <QtNetwork/QTcpServer>
#include <QtNetwork/QTcpSocket>
#include <QtNetwork/QHostAddress>
#include <QString>

namespace net
{
Server::Server(bool dbg){
    debug = dbg;
}

void Server::Debug(QString message){
    if(debug){
        qDebug() << message;
    }
}

void Server::listen(quint16 port)
{
    tcpServer = new QTcpServer(this);
    Debug(QString("J'ai créé mon serveur"));
    tcpServer->listen(QHostAddress::Any, port);
    Debug(QString() + "J'écoute avec le port " + QString::number(port));
    QObject::connect(tcpServer, SIGNAL(newConnection()), this, SLOT(newPlayer()));
    Debug(QString() + "J'ai connecté le signal newConnection avec le slot newPlayer");
    signalMapper = new QSignalMapper(this);
    Debug(QString() + "J'ai fait le mapping du signal");
}

void Server::sendTurnMessage(TurnNumber t){
    Debug(QString() + "Je prépare le message TURN_MESSAGE avec le tour " + t);
    ActionID id = TURN_MESSAGE;
    QByteArray m = QByteArray::fromRawData(reinterpret_cast<char const *>(&id), sizeof(id));
    m.append(t);
    Debug("J'envoie le message TURN_MESSAGE avec le tour " + t);
    for(size_t p = 0; p < tcpSockets.size(); p++) {
        tcpSockets[p]->write(m.constData(), sizeof(id)+sizeof(t));
        Debug(QString() + "J'ai envoyé le message TURN_MESSAGE " + t + " au joueur " + p);
    }
    Debug(QString() + "J'ai envoyé le message TURN_MESSAGE avec le tour " + t);
}

void Server::sendRefusalMessage(PlayerID to){
    Debug(QString() + "Je prépare un REFUSAL_MESSAGE pour le joueur " + to);
    ActionID id = REFUSAL_MESSAGE;
    QByteArray m = QByteArray::fromRawData(reinterpret_cast<char const *>(&id), sizeof(id));
    tcpSockets[to]->write(m.constData(), sizeof(id));
    Debug(QString() + "J'ai envoyé le REFUSAL_MESSAGE au joueur " + to);
}

void Server::sendDrawMessage(PlayerID p, CardID c){
    Debug(QString() + "Je prépare un DRAW_MESSAGE du joueur " + p + "et la carte " + c);
    ActionID id = DRAW_MESSAGE;
    QByteArray m = QByteArray::fromRawData(reinterpret_cast<char const *>(&id), sizeof(id));
    QByteArray m_other = QByteArray::fromRawData(reinterpret_cast<char const *>(&id), sizeof(id));
    m.append(p).append(c);
    m_other.append(p).append(UNKNOWN_CARD);
    Debug(QString() + "J'envoie le DRAW_MESSAGE du joueur " + p + "et la carte " + c);
    for(size_t q = 0; q < tcpSockets.size(); q++) {
        if(q == p){
            tcpSockets[p]->write(m.constData(), sizeof(id)+sizeof(p)+sizeof(c));
        }
        else{
            tcpSockets[q]->write(m_other.constData(), sizeof(id)+sizeof(p)+sizeof(UNKNOWN_CARD));
        }
    }
    Debug(QString() + "J'ai envoyé le DRAW_MESSAGE du joueur " + p + " et la carte " + c);
}

void Server::sendPlayCardOnBoardMessage(PlayerID p, CardID c, Coords coords, Orientation ori){
    Debug(QString() + "Je prépare le PLAY_CARD_ON_BOARD_MESSAGE joueur " + p + " carte " + c + " coords (" + coords.x + "," + coords.y + ")" + " orientation " + ori);
    ActionID id = PLAY_CARD_ON_BOARD_MESSAGE;
    QByteArray m = QByteArray::fromRawData(reinterpret_cast<char const *>(&id), sizeof(id));
    char c_ori = (ori) ? 1 : 0;
    m.append(p).append(c).append(coords.x).append(coords.y).append(c_ori);
    Debug(QString() + "J'envoie le PLAY_CARD_ON_BOARD_MESSAGE joueur " + p + " carte " + c + " coords (" + coords.x + "," + coords.y + ")" + " orientation " + ori);
    for(size_t q = 0; q < tcpSockets.size(); q++){
        tcpSockets[q]->write(m.constData(), sizeof(id)+sizeof(p)+sizeof(c)+sizeof(coords.x)+sizeof(coords.y)+sizeof(c_ori));
    }
    Debug(QString() + "J'ai envoyé' le PLAY_CARD_ON_BOARD_MESSAGE joueur " + p + " carte " + c + " coords (" + coords.x + "," + coords.y + ")" + " orientation " + ori);
}

void Server::sendPlayCardOnPlayerMessage(PlayerID p, CardID c, PlayerID victim){
    Debug(QString() + "Je prépare le PLAY_CARD_ON_PLAYER_MESSAGE joueur " + p + " carte " + c + " victime " + victim);
    ActionID id = PLAY_CARD_ON_PLAYER_MESSAGE;
    QByteArray m = QByteArray::fromRawData(reinterpret_cast<char const *>(&id), sizeof(id));
    m.append(p).append(c).append(victim);
    Debug(QString() + "J'envoie' le PLAY_CARD_ON_PLAYER_MESSAGE joueur " + p + " carte " + c + " victime " + victim);
    for(size_t q = 0; q < tcpSockets.size(); q++){
        tcpSockets[q]->write(m.constData(), sizeof(id)+sizeof(p)+sizeof(c)+sizeof(victim));
    }
    Debug(QString() + "J'ai envoyé le PLAY_CARD_ON_PLAYER_MESSAGE joueur " + p + " carte " + c + " victime " + victim);
}

void Server::sendWinnerMessage(PlayerID player){
    Debug(QString() + "Je prépare WINNER_MESSAGE joueur " + player);
    ActionID id = WINNER_MESSAGE;
    QByteArray m = QByteArray::fromRawData(reinterpret_cast<char const *>(&id), sizeof(id));
    m.append(player);
    Debug(QString() + "J'envoie WINNER_MESSAGE joueur " + player);
    for(size_t q = 0; q < tcpSockets.size(); q++){
        tcpSockets[q]->write(m.constData(), sizeof(id)+sizeof(player));
    }
    Debug(QString() + "J'ai envoyé WINNER_MESSAGE joueur " + player);
}

void Server::sendRevealCardMessage(PlayerID to, Coords c, bool isGold){
    ActionID id = REVEAL_CARD_MESSAGE;
    QByteArray m = QByteArray::fromRawData(reinterpret_cast<char const *>(&id), sizeof(id));
    m.append(c.x).append(c.y);
    char c_gold = (isGold) ? 1 : 0;
    m.append(c_gold);
    if(to == EVERY_PLAYER){
        for(size_t q = 0; q < tcpSockets.size(); q++){
            tcpSockets[q]->write(m.constData(), sizeof(id)+sizeof(c.x)+sizeof(c.y)+sizeof(c_gold));
        }
    }
    else{
        tcpSockets[to]->write(m.constData(), sizeof(id)+sizeof(c.x)+sizeof(c.y)+sizeof(c_gold));
    }
}

void Server::sendDiscardCardMessage(PlayerID p){
    Debug(QString() + "Je prépare un DISCARD_CARD_MESSAGE joueur " + p);
    ActionID id = DISCARD_CARD_MESSAGE;
    QByteArray m = QByteArray::fromRawData(reinterpret_cast<char const *>(&id), sizeof(id));
    m.append(p);
    Debug(QString() + "J'envoie un DISCARD_CARD_MESSAGE joueur " + p);
    for(size_t q = 0; q < tcpSockets.size(); q++){
        tcpSockets[q]->write(m.constData(), sizeof(id)+sizeof(p));
    }
    Debug(QString() + "J'ai envoyé un DISCARD_CARD_MESSAGE joueur " + p);
}

void Server::sendGameMessage(RoundNumber r){
    Debug(QString() + "Je prépare un GAME_MESSAGE roundnumber " + QString::number(r));
    ActionID id = GAME_MESSAGE;
    QByteArray m = QByteArray::fromRawData(reinterpret_cast<char const *>(&id), sizeof(id));
    m.append(r);
    Debug(QString() + "J'envoie un GAME_MESSAGE roundnumber " + QString::number(r));
    for(size_t p = 0; p < tcpSockets.size(); p++) {
        tcpSockets[p]->write(m.constData(), sizeof(id)+sizeof(r));
    }
    Debug(QString() + "J'ai envoyé un GAME_MESSAGE roundnumber " + QString::number(r));
}

void Server::sendResendAllMessage(PlayerID to,
                          std::vector<PlayerInfo> const & players,
                          std::vector<PlayedCard> const & board,
                          NumCards still_in_deck,
                          std::vector<CardID> const & cards_in_hand,
                          RoleID role,
                          TurnNumber turn){
    Debug("Begin sending resendAllMessage to player " + QString::number(to));
    ActionID id = RESEND_ALL_MESSAGE;
    int size = sizeof(id);
    QByteArray m = QByteArray::fromRawData(reinterpret_cast<char const *>(&id), sizeof(id));
    std::uint8_t toId = to;
    m.append(toId);
    size+=sizeof(toId);
    std::uint8_t NumPlayers = players.size();
    size += sizeof(NumPlayers);
    m.append(NumPlayers);
    for(size_t q = 0; q < players.size(); q++){
        Debug("Setting data for player with id " + QString::number(players[q].id));
      size += sizeof(players[q].id);
      m.append(players[q].id);
      std::string pseudo = players[q].pseudo;
      std::uint8_t pseudoLength = pseudo.size();
      size += sizeof(pseudoLength);
      m.append(pseudoLength);
      const char* cpseudo = pseudo.c_str();
      for(const char* cp = cpseudo; cp - cpseudo < pseudoLength; cp++){
        size += sizeof(*cp);
        m.append(*cp);
      }
      size += sizeof(players[q].num_cards);
      m.append(players[q].num_cards);
      size += sizeof(players[q].gold);
      m.append(players[q].gold);
      std::uint8_t isBlocked = 0;
      if(players[q].isPickaxeBroken){
          isBlocked = isBlocked | PICKAXE;
      }
      if(players[q].isCarriageBroken){
          isBlocked = isBlocked | CARRIAGE;
      }
      if(players[q].isLampBroken){
          isBlocked = isBlocked | LAMP;
      }
      size += sizeof(isBlocked);
      m.append(isBlocked);

    }
    std::uint8_t NumCardsPlayed = board.size();
    size += sizeof(NumCardsPlayed);
    m.append(NumCardsPlayed);
    Debug("Number of cards played : " + QString::number(NumCardsPlayed));
    Debug("Number of card in the struct hand : "+QString::number(players[to].num_cards));
    for(size_t q = 0; q < NumCardsPlayed; q++){
        Debug("Buffer size for card on board n°" + QString::number(q));
        Debug(QString::number(size));
        if(board[q].id == 83){Debug("FOUND CARD 83 IN sendResendAllMessage, board");}
      size += sizeof(board[q].id);
      m.append(board[q].id);
      size += sizeof(board[q].pos.x);
      m.append(board[q].pos.x);
      size += sizeof(board[q].pos.y);
      m.append(board[q].pos.y);
      char c_ori = (board[q].ori) ? 1 : 0;
      size += sizeof(c_ori);
      m.append(c_ori);
    }
    size += sizeof(still_in_deck);
    m.append(still_in_deck);
    Debug("Number of cards in deck : " + QString::number(still_in_deck));
    Debug("Number of cards in hand : " + QString::number(cards_in_hand.size()));
    for(size_t q = 0; q < cards_in_hand.size(); q++){
        Debug("Card id n°" + QString::number(q) + " : " + QString::number(cards_in_hand[q]));
      size += sizeof(cards_in_hand[q]);
      m.append(cards_in_hand[q]);
    }
    size += sizeof(role);
    m.append(role);
    Debug("The role of this player is : " + QString::number(role));
    size += sizeof(turn);
    m.append(turn);
    tcpSockets[to]->write(m.constData(), size);
    Debug("Size of data sent : " + QString::number(size));
    Debug("End sending resendAllMessage to player " + QString::number(to));
}

void Server::newPlayer()
{
    Debug(QString("begin net::Server::newPlayer"));
    Debug(QString() + "Nouveau joueur qui arrive");
    QTcpSocket* tcpSocket = tcpServer->nextPendingConnection();
    tcpSockets.push_back(tcpSocket);
    buffers.emplace_back();
    PlayerID socketId = tcpSockets.size() - 1;
    signalMapper->setMapping(tcpSocket, socketId);
    QObject::connect(signalMapper, SIGNAL(mapped(int)), this, SLOT(readyRead(int)));
    QObject::connect(tcpSocket, SIGNAL(readyRead()), signalMapper, SLOT(map()));
    Debug(QString() + "Nouveau joueur ajouté avec id " + QString::number(socketId));
    Debug(QString("end net::Server::newPlayer"));
}


void Server::readyRead(int p)
{
    Debug(QString("begin net::Server::readyRead"));
    Debug(QString() + "Le joueur " + QString::number(p) + " a envoyé un message");
    QTcpSocket* tcpSocket = tcpSockets[p];
    qDebug() << "tcpSocket got";
    buffers[p].append(tcpSocket->readAll());
    Debug(QString() + "Je lis le message du joueur " + QString::number(p));
    while (tryReadBuffer(static_cast<PlayerID>(p)));
    Debug(QString() + "J'ai lu le message du joueur " + QString::number(p));
    Debug(QString("end net::Server::readyRead"));
}

bool Server::tryReadBuffer(PlayerID p)
{
    Debug(QString("begin net::Server::tryReadBuffer"));
    QByteArray & byte = buffers[p];
    int size = byte.size();
    if(size < 1) {
        Debug("Buffer size null");
      return false;
    }
    Debug("Buffer size ok");

    ActionID signal = byte[0];

    Debug("Signal type reading ok");
    Debug(QString("signal = ") + QString::number(signal));
    char len;
    std::string pseudo;
    PlayerID pOn;
    CardID c;
    Coord x, y;
    char ori;
    switch (signal) {

      case HELLO_MESSAGE:
          Debug("Message type : Hello Message");
          len = byte[1];
          if(size < 2){
            return false;
          }
          if(size < len+2){
            return false;
          }
          pseudo = "";
          Debug("Reading pseudo");
          for(int i = 0; i < len; i++){
            pseudo += byte[i+2];
          }
          Debug(QString("Pseudo read : ") + QString(pseudo.c_str()));
          byte.remove(0, len+2);
          Debug("Removed bytes");
          Debug(QString("pseudo = ") + QString::fromStdString(pseudo));
          emit newHelloMessage(p, pseudo);
          Debug("Emitted newHelloMessageSignal");
          break;

      case PLAY_CARD_ON_BOARD_MESSAGE:
        Debug("Sending play card on board");
          if (size < 6) {
            return false;
          }
          if (!byte[1]) {
            throw std::runtime_error("Ce message est un mauvais message !");
          }
          c = byte[2];
          x = byte[3];
          y = byte[4];
          ori = byte[5];
          byte.remove(0,6);
          emit newPlayCardOnBoardMessage(p, c, Coords(x,y), (ori!=0));
          break;
      case PLAY_CARD_ON_PLAYER_MESSAGE:
        Debug("Sending Play card on player");
          if (size < 4) {
            return false;
          }
          if (!byte[1]) {
            throw std::runtime_error("Ce message est un mauvais message !");
          }
          c = byte[2];
          pOn = byte[3];
          byte.remove(0,4);
          emit newPlayCardOnPlayerMessage(p, c, pOn);
          break;
       case PLEASE_RESEND_ALL_MESSAGE :
        Debug("Sending resendAllMessage");
          byte.remove(0,1);
          emit newPleaseResendAllMessage(p);
          break;
       default :
          throw std::runtime_error("Ce message n'existe pas !");
    }
    Debug(QString("end net::Server::tryReadBuffer"));
    return true;
}

}
