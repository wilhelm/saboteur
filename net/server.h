#ifndef NET_SERVER_H_INCLUDED
#define NET_SERVER_H_INCLUDED 1

#include "../net/types.h"
#include <QObject>
#include <QtNetwork/QTcpServer>
#include <QtNetwork/QTcpSocket>
#include <QSignalMapper>

namespace net {

/**
 * \brief Network server for a saboteur game
 *
 * The server can listen on any port, and will handle possibly partial delivery
 * of some messages by itself.
 */
class Server : public QObject
{
  Q_OBJECT

public:
    /**
     * \brief Constructor
     *
     *
     * \param debug for debuging
     */
    Server(bool debug = false);
    /**
     * \brief Listens on a given port
     *
     * \param port the port on which to listen
     */
    void listen(quint16 port);

    /**
     * \brief Broadcast a new game
     *
     * \param r Number of remaining rounds
     */
    void sendGameMessage(RoundNumber r);

    /**
     * \brief Broadcast a new turn
     *
     * \param t Turn number
     */
    void sendTurnMessage(TurnNumber t);

    /**
     * \brief Broadcast a card being drawn
     *
     * Sending the card information only to the right player is handled by the
     * function itself.
     *
     * \param p The player drawing a card
     * \param c The card that is being drawn (or 255 if current player is not p)
     */
    void sendDrawMessage(PlayerID p, CardID c);

    /**
     * \brief Broadcast that a player puts a card on the board
     *
     * \param p The player playing the card
     * \param c The card that is being played
     * \param coords The coordinates to which the card is played
     * \param ori The orientation of the played card
     */
    void sendPlayCardOnBoardMessage(PlayerID p, CardID c, Coords coords,
                                 Orientation ori);

    /**
     * \brief Broadcast that a player puts a card on another player
     *
     * \param p The player playing the card
     * \param c The card that is being played
     * \param victim The player to which the card is played
     */
    void sendPlayCardOnPlayerMessage(PlayerID p, CardID c, PlayerID victim);

    /**
     * \brief Broadcast that a player just discarded a card
     *
     * \param p The player who just discarded a card
     */
    void sendDiscardCardMessage(PlayerID p);

    /**
     * \brief Reveal a card to one or all player(s)
     *
     * \param to The player to whom to reveal the card (or -1 for everyone)
     * \param c The coordinates of the card to reveal
     * \param isGold True if the card revealed is gold
     */
    void sendRevealCardMessage(PlayerID to, Coords c, bool isGold);

    /**
     * \brief Refuse an action performed by player
     *
     * \param to The player of which the action has been refused
     */
    void sendRefusalMessage(PlayerID to);

    /**
     * \brief Resend the complete state of the game
     *
     * \param to Player to which to resend the game state
     * \param players Informations about the players (including the current
     *                player)
     * \param board Informations about all cards on the board
     * \param still_in_deck Number of cards still in the deck
     * \param cards_in_hand Number of cards in hand of each player
     * \param role Role of the current player
     * \param turn Current turn number
     */
    void sendResendAllMessage(PlayerID to,
                              std::vector<PlayerInfo> const & players,
                              std::vector<PlayedCard> const & board,
                              NumCards still_in_deck,
                              std::vector<CardID> const & cards_in_hand,
                              RoleID role,
                              TurnNumber turn);

    /**
     * \brief Send the game winner
     *
     * \param player The winner
     */
    void sendWinnerMessage(PlayerID player);

public slots:
    /**
     *\brief slot bind to the newConnection signal
     */
    void newPlayer();

    /**
     *\brief slot bind to the readyRead signal from the player
     *
     *\param p the ID of the player that writes a message
     */
    void readyRead(int p);


signals:
    /**
     * \brief Signal fired when a new user wants to join
     *
     * \param p The ID of the player that wants to join
     * \param pseudo The self-declared pseudonym of the player that wants to
     *               join (without any kind of duplicate checking)
     */
    void newHelloMessage(PlayerID p, std::string const & pseudo);

    /**
     * \brief Signal fired when a user wants to play a road card
     *
     * \param p The player wanting to play the card
     * \param c The card ID he wants to play
     * \param pos The position at which he wants to play the card
     * \param ori The orientation with which he wants to play the card
     */
    void newPlayCardOnBoardMessage(PlayerID p, CardID c, Coords coords,
                                   Orientation ori);

    /**
     * \brief Signal fired when a user wants to play an action card
     *
     * \param p The player wanting to play the card
     * \param c The card ID he wants to play
     * \param victim The player ID of the player onto which he wants to play the
     *               card
     */
    void newPlayCardOnPlayerMessage(PlayerID p, CardID c, PlayerID victim);

    /**
     * \brief Signal fired when a user wants to discard a card
     *
     * \param p The player wanting to discard a card
     * \param c The card ID he wants to discard
     */
    void newDiscardCardMessage(PlayerID p, CardID c);

    /**
     * \brief Signal fired when a player wants to be resent the complete state
     *        of the game
     *
     * \param p The ID of the player wanting to be resent the state
     */
    void newPleaseResendAllMessage(PlayerID p);

private:
    QTcpServer* tcpServer;
    std::vector<QTcpSocket*> tcpSockets;
    std::vector<QByteArray> buffers;
    QSignalMapper* signalMapper;

    /**
     * \brief Try to parse packet(s) from a player's incoming buffer
     *
     * \param p The player whose buffer we want to try and parse
     * \returns true iff a packet has been successfully parsed
     */
    bool tryReadBuffer(PlayerID p);
    bool debug;
    void Debug(QString message);
};

}


#endif
