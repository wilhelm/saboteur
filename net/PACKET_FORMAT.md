Everything is inside a TLS connection (via QSslSocket)

At the beginning of a round, a ResendAllMessage is sent, which describes the
initial status of the round.

Format of a packet:
 * Packet Type (8 bits)
 * Then, the packet,  depending on the packet type

HelloMessage: (client -> server only)
 * Pseudo Length (8 bits)
 * Pseudo Length * char (8 bits, ASCII)

GameMessage: (server -> client only)
 * Number of rounds (8 bits)

TurnMessage: (server -> client only)
 * Turn Number (8 bits)

DrawMessage: (server -> client only)
 * Drawing Player ID (8 bits)
 * Card ID (8 bits, 255 if the player is not the drawing player)

PlayCardOnBoardMessage:
 * Acting Player ID (8 bits, 0 when client -> server direction)
 * Card ID (8 bits)
 * Position (X, Y) (2 * 8 bits, signed)
 * Orientation (8 bits)

PlayCardOnPlayerMessage:
 * Acting Player ID (8 bits, 0 when client -> server direction)
 * Card ID (8 bits)
 * Player ID (8 bits)

DiscardCardMessage:
 * Acting Player ID (8 bits, 0 when client -> server direction)
 * Card ID (8 bits, 0 when server -> client direction)

RevealCardMessage: (server -> client only)
 * Position (X, Y) (8 bits)
 * GoldOrNot (8 bits, 0 or 1)

RefusalMessage: (server -> client only)
 * Nothing additional

PleaseResendAllMessage: (client -> server only)
 * Nothing additional

ResendAllMessage: (server -> client only, also sent at game beginning)
 * PlayerID (8 bits)
 * NumPlayers (8 bits)
 * NumPlayers times:
    * PlayerID (8 bits)
    * PlayerPseudoLength (8 bits)
    * PlayerPseudoLength * char (8 bits, ASCII)
    * NumCards (8 bits)
    * Gold (8 bits)
    * IsBlocked (8 bits, according to bitflags net::PICKAXE…)
 * NumCardsPlayed (8 bits)
 * NumCardsPlayed times:
    * Card ID (8 bits)
    * Position (X, Y) (2 * 8 bits, signed)
    * Orientation (8 bits)
 * NumCardsRemaining (8 bits)
 * For the player asking the resend all, NumCards[player id]:
    * Card ID (8 bits)
 * For the player asking, his role (8 bits)
 * Round number (8 bits)

WinnerMessage: (server -> client only)
 * PlayerID (8 bits)
