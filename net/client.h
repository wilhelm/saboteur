#ifndef NET_CLIENT_H_INCLUDED
#define NET_CLIENT_H_INCLUDED 1

#include "../net/types.h"
#include <QtNetwork/QTcpSocket>
#include <QObject>

namespace net
{

/**
 * \brief Network client for a saboteur game
 *
 * The client can connect to a server on any host/port couple.
 */
class Client : public QObject
{
    Q_OBJECT
public:
    Client(const bool isDebug = false);

    /**
     * \brief Connects the client to a server
     *
     * \param host The hostname of the server
     * \param port The port on which the server is listening
     * \param pseudo The pseudonym under which to join the game (max. 255 chars)
     */
    void connect(const QString& host, const quint16 port, const std::string& pseudo);

    /**
     * \brief Play a road card
     *
     * \param c The card ID to play (has to be in hand)
     * \param pos The position at which to play the card
     * \param ori The orientation with which to play the card
     */
    void sendPlayCardOnBoardMessage(CardID c, Coords pos, Orientation ori);

    /**
     * \brief Play an action card
     *
     * \param c The card ID to play
     * \param victim The player ID of the player onto which to play the card
     */
    void sendPlayCardOnPlayerMessage(CardID c, PlayerID victim);

    /**
     * \brief Discard a card
     *
     * \param c The card ID of the card to discard
     */
    void sendDiscardCardMessage(CardID c);

    /**
     * \brief Send a message asking to be resent all the state of the game
     */
    void sendPleaseResendAllMessage();

    bool tryReadBuffer();

public slots:
    void readyRead();

signals:
    /**
     * \brief Signal fired on a new game
     *
     * \param r Number of rounds
     */
    void newGameMessage(net::RoundNumber r);

    /**
     * \brief Signal fired on a new turn
     *
     * \param t Turn number
     */
    void newTurnMessage(net::TurnNumber t);

    /**
     * \brief Signal fired when any player draws a card
     *
     * \param p The player drawing a card
     * \param c The card that is being drawn (or 255 if current player is not p)
     */
    void newDrawMessage(net::PlayerID p, net::CardID c);

    /**
     * \brief Signal fired when any player puts a card on the board
     *
     * \param p The player playing the card
     * \param c The card that is being played
     * \param coords The coordinates to which the card is played
     * \param ori The orientation of the played card
     */
    void newPlayCardOnBoardMessage(net::PlayerID p, net::CardID c, net::Coords coords,
                                net::Orientation ori);

    /**
     * \brief Signal fired when any player puts a card on any other player
     *
     * \param p The player playing the card
     * \param c The card that is being played
     * \param victim The player to which the card is played
     */
    void newPlayCardOnPlayerMessage(net::PlayerID p, net::CardID c, net::PlayerID victim);

    /**
     * \brief Signal fired when a player just discarded a card
     *
     * \param p The player who just discarded a card
     */
    void newDiscardCardMessage(net::PlayerID p);

    /**
     * \brief Signal fired when a card is revealed to the player
     *
     * \param c The coordinates where the vision card was used
     * \param isGold True if the card on which the vision card was used is gold
     */
    void newRevealCardMessage(net::Coords c, bool isGold);

    /**
     * \brief Signal fired when an action performed has been refused by the
     *        server
     */
    void newRefusalMessage();

    /**
     * \brief Signal fired when the server resent the complete state of the game
     *
     * \param players Informations about the players (including the current
     *                player)
     * \param board Informations about all cards on the board
     * \param still_in_deck Number of cards still in the deck
     * \param cards_in_hand Number of cards in hand of each player
     * \param role Role of the current player
     * \param turn Current turn number
     */
    void newResendAllMessage(net::PlayerID id,
                             std::vector<net::PlayerInfo> const & players,
                             std::vector<net::PlayedCard> const & board,
                             net::NumCards still_in_deck,
                             std::vector<net::CardID> const & cards_in_hand,
                             net::RoleID role,
                             net::TurnNumber turn);

    /**
     * \brief Signal fired when the server advertises the game winner
     *
     * \param player The winner
     */
    void newWinnerMessage(net::PlayerID player);

private:
    QTcpSocket *socket;
    QByteArray buffer;
    bool debugMode;

    void debug(const QString& msg);
};

}

#endif
