#ifndef NET_TYPES_H_INCLUDED
#define NET_TYPES_H_INCLUDED 1

#include <cstdint>
#include <string>

namespace net
{
typedef std::uint8_t ActionID; //< An action identifier
typedef std::uint8_t CardID; //< A card identifier
typedef std::int8_t Coord; //< A one-axis coordinate \sa Coords
typedef bool Orientation; //< Whether the card should be flipped upside-down
typedef std::uint8_t PlayerID; //< A player identifier
typedef std::uint8_t RoleID; //< A role identifier
typedef std::uint8_t RoundNumber; //< The number of the current round
typedef std::uint8_t TurnNumber; //< The number of the current turn
typedef std::uint8_t NumCards; //< A number of cards
typedef std::uint8_t GoldAmount; //< An amount of gold

enum {
    HELLO_MESSAGE = 200,
    TURN_MESSAGE = 201,
    DRAW_MESSAGE = 202,
    PLAY_CARD_ON_BOARD_MESSAGE = 203,
    PLAY_CARD_ON_PLAYER_MESSAGE = 204,
    REFUSAL_MESSAGE = 205,
    PLEASE_RESEND_ALL_MESSAGE = 206,
    RESEND_ALL_MESSAGE = 207,
    DISCARD_CARD_MESSAGE = 208,
    WINNER_MESSAGE = 209,
    REVEAL_CARD_MESSAGE = 210,
    GAME_MESSAGE = 211,
};

enum {
    PICKAXE =  1 << 1,
    CARRIAGE = 1 << 2,
    LAMP =     1 << 3
};

const CardID UNKNOWN_CARD = 255;
const PlayerID EVERY_PLAYER = 255;

/// A two-coordinate point in space
struct Coords {
    Coord x, y;
    Coords(Coord x, Coord y) : x(x), y(y) { }
    bool operator<(const Coords& b) const {
        if (x<b.x) return true;
        if (x>b.x) return false;
        if (y<b.y) return true;
        return false;
    }
    bool operator == (const Coords & b) const {
        return x == b.x && y == b.y;
    }
    bool operator != (const Coords & b) const {
        return !(*this == b);
    }
};

/// All the information a user has about another user.
struct PlayerInfo {
    PlayerID id;
    std::string pseudo;
    NumCards num_cards;
    GoldAmount gold;
    bool isPickaxeBroken, isCarriageBroken, isLampBroken;
    PlayerInfo(PlayerID id, std::string pseudo, NumCards num_cards,
               GoldAmount gold, bool pickBro, bool carrBro, bool lampBro)
        : id(id), pseudo(pseudo), num_cards(num_cards), gold(gold)
        , isPickaxeBroken(pickBro), isCarriageBroken(carrBro)
        , isLampBroken(lampBro)
    { }
    bool isSomehowBlocked() {
        return isPickaxeBroken || isCarriageBroken || isLampBroken;
    }
};

/// A card that has been put somewhere on the board
struct PlayedCard {
    CardID id;
    Coords pos;
    Orientation ori;
    PlayedCard(CardID id, Coords pos, Orientation ori)
        : id(id), pos(pos), ori(ori)
    { }
};

}

#endif
