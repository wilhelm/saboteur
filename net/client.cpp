#include "../net/client.h"
namespace net {


using std::uint8_t;

Client::Client(const bool isDebug) {
    socket = new QTcpSocket(this);
    debugMode = isDebug;
}

void Client::debug(const QString& msg) {
    if (Client::debugMode)
        qDebug() << msg;
}

void Client::connect(const QString& host, const quint16 port, const std::string& pseudo){
    debug("begin net::Client::connect");
    //debug("trying to connect socket");
    QObject::connect(socket, SIGNAL(readyRead()), this, SLOT(readyRead()));
    debug("Connecting to host " + host + " with port " + QString::number(port));
    socket->connectToHost(host, port);

    if (socket->waitForConnected(3000)){
      debug("Connected!");}
    else{
      debug("Connection failed");}

    if (pseudo.length() > 255)
        throw std::runtime_error("Tu es un gros tos !");
    uint8_t len = pseudo.length();
    ActionID id = HELLO_MESSAGE;
    QByteArray ps = QByteArray::fromRawData(pseudo.c_str(),len);
    ps.prepend(len).prepend(id);
    debug("Sending HelloMessage " + QString::fromStdString(pseudo));
    socket->write(ps.constData(),sizeof(id)+sizeof(len)+pseudo.length());
    debug("I wrote the message");
    debug("end net::Client::connect");
}

void Client::sendPlayCardOnBoardMessage(CardID c, Coords pos, Orientation ori){
    debug("Sending playCardOnBoardMessage with card " + c);
    ActionID id = PLAY_CARD_ON_BOARD_MESSAGE;
    QByteArray m = QByteArray::fromRawData(reinterpret_cast<char const *>(&id),
                                           sizeof(id));
    PlayerID pid = 1;
    m.append(pid).append(c).append(pos.x).append(pos.y).append(ori);
    socket->write(m.constData(),sizeof(id)+sizeof(pid)+sizeof(c)+sizeof(pos.x)+sizeof(pos.y)+sizeof(ori));
}

void Client::sendPlayCardOnPlayerMessage(CardID c, PlayerID victim){
    debug("Sending playCardOnPlayerMessage with card " + c);
    ActionID id = PLAY_CARD_ON_PLAYER_MESSAGE;
    PlayerID pid = 1;
    QByteArray m = QByteArray::fromRawData(reinterpret_cast<char const *>(&id),
                                           sizeof(id));
    m.append(pid).append(c).append(victim);
    socket->write(m.constData(),sizeof(id)+sizeof(pid)+sizeof(c)+sizeof(victim));
}

void Client::sendDiscardCardMessage(CardID c){
    debug("Sending discardCardMessage with card " + c);
    ActionID id = DISCARD_CARD_MESSAGE;
    PlayerID pid = 1;
    QByteArray m = QByteArray::fromRawData(reinterpret_cast<char const *>(&id),
                                           sizeof(id));
    m.append(pid).append(c);
    socket->write(m.constData(),sizeof(pid)+sizeof(id)+sizeof(c));
}

void Client::sendPleaseResendAllMessage(){
    debug("Sending pleaseResendAllMessage");
    ActionID id = PLEASE_RESEND_ALL_MESSAGE;
    socket->write(reinterpret_cast<char const *>(&id), sizeof(ActionID));
}

void Client::readyRead() {
    buffer.append(socket->readAll());
    while (tryReadBuffer()) ;
}

bool Client::tryReadBuffer() {
    int size = buffer.size();
    if (size < 1)
        return false;
    ActionID signal = buffer[0];
    PlayerID id;
    int nbPlayers, k;
    NumCards player_cards = 0, still_in_deck, cards_played;
    std::vector<PlayerInfo> players;
    std::vector<PlayedCard> board;
    std::vector<CardID> cards_in_hand = *(new std::vector<CardID>());
    bool isGold;
    RoleID role;
    TurnNumber turn;
    switch (signal)
    {
    case WINNER_MESSAGE:
        if (size > 1) {
            debug(QString("Receiving winnerMessage with winner ") + buffer[1]);
            emit newWinnerMessage(buffer[1]);
            buffer.remove(0,2);
            return true;
        }
        return false;
    case DISCARD_CARD_MESSAGE:
        if (size > 1) {
            debug(QString("Receiving discardCardMessage with player ") + buffer[1]);
            emit newDiscardCardMessage(buffer[1]);
            buffer.remove(0,2);
            return true;
        }
        return false;
    case TURN_MESSAGE:
        if (size > 1) {
            debug(QString("Receiving TurnMessage with ") + buffer[1]);
            emit newTurnMessage(buffer[1]);
            buffer.remove(0,2);
            return true;
        }
        return false;
    case GAME_MESSAGE:
        if (size > 1) {
            debug(QString("Receiving GameMessage with ") + buffer[1]);
            emit newGameMessage(buffer[1]);
            buffer.remove(0,2);
            return true;
        }
        return false;
    case DRAW_MESSAGE:
        if (size > 2) {
            debug(QString("Receiving TurnMessage with player ") + buffer[1]);
            emit newDrawMessage(buffer[1],buffer[2]);
            buffer.remove(0,3);
            return true;
        }
        return false;
    case PLAY_CARD_ON_BOARD_MESSAGE:
        if (size > 5) {
            debug(QString("Receiving playCardOnBoardMessage with player ") + buffer[1]);
            emit newPlayCardOnBoardMessage(buffer[1], buffer[2], Coords(buffer[3],buffer[4]),buffer[5]);
            buffer.remove(0,6);
            return true;}
        return false;
    case PLAY_CARD_ON_PLAYER_MESSAGE:
        if (size > 3) {
            debug(QString("Receiving playCardOnPlayerMessage with player ") + buffer[1]);
            emit newPlayCardOnPlayerMessage(buffer[1], buffer[2],buffer[3]);
            buffer.remove(0,4);
            return true;}
        return false;
    case REVEAL_CARD_MESSAGE:
        if (size > 3) {
            debug(QString("Receiving revealCardMessage with player ") + buffer[1]);
            if (static_cast<char>(buffer[3]) == 0)
                isGold = false;
            else
                isGold = true;
            emit newRevealCardMessage(Coords(buffer[1], buffer[2]), isGold);
            buffer.remove(0,4);
            return true;}
        return false;
    case REFUSAL_MESSAGE:
        debug("Receiving refusalMessage");
        emit newRefusalMessage();
        buffer.remove(0,1);
        return true;
    case RESEND_ALL_MESSAGE:
        debug("Receiving resendAllMessage");
        if (size < 3)
           return false;
        id = buffer[1];
        qDebug() << "[NET] Player ID : " << QString::number(id);
        nbPlayers = buffer[2];
        qDebug() << "[NET] Nb of players : " << QString::number(nbPlayers);
        k = 3; //prochain emplacement dans le buffer que l'on veut regarder
        for (int i = 0; i < nbPlayers; i++){
            if (size < k + 2)
                    return false;
            PlayerID pid = buffer[k];
            qDebug() << "[NET] Player ID : " << QString::number(pid);
            k++;
            int pLength = buffer[k];
            k++;
            if (size < k + 3 + pLength)
                return false;
            std::string pseudo;
            for (int i = k; i < k+pLength; i++)
                pseudo.push_back(buffer[i]);
            qDebug() << "[NET] Player Pseudo : " << pseudo.c_str();
            k+=pLength;
            NumCards cards_current = buffer[k]; //nombre de cartes du joueur k
            qDebug() << "[NET] Player Cards In Hand : " << QString::number(cards_current);
            if (id == pid)
                player_cards = cards_current; //nombre de cartes du joueur actif
            k++;
            GoldAmount gold = buffer[k];
            k++;
            std::uint8_t isBlocked = buffer[k];
            k++;
            players.push_back(PlayerInfo(pid, pseudo, cards_current, gold,
                                         isBlocked&PICKAXE, isBlocked&CARRIAGE, isBlocked&LAMP));
        }
        if (size < k + 1)
            return false;
        cards_played = buffer[k];
        qDebug() << "[NET] Nb Of Cards Played : " << QString::number(cards_played);
        k++;
        for(int i = 0; i < cards_played; i++){
            if(size < k + 4)
                return false;
            qDebug() << "BUFFER k : " << QString::number(k);
            CardID id = (int)buffer[k];
            if (id==0) {
                qDebug() << "FIND ERROR : CARD ID 0 IN CARDS PLAYED";
            }
            k++;
            Coord x = buffer[k];
            k++;
            Coord y = buffer[k];
            k++;
            Orientation ori = buffer[k];
            k++;
            board.push_back(PlayedCard(id,Coords(x,y),ori));
        }
        if(size < k + 4 + player_cards)
            return false;
        still_in_deck = buffer[k];
        qDebug() << "[NET] Nb Cards In Deck : " << QString::number(still_in_deck);
        k++;
        for (int i = 0; i < player_cards; i++){
            cards_in_hand.push_back(buffer[k]);
            CardID id2 = (int)buffer[k];
            if(id2 == 0){
                qDebug() << "IND ERROR : CARD ID 0 IN HAND CARDS";
            }
            qDebug() << "[NET] Card In Hand ID : " << QString::number(buffer[k]);
            k++;
        }
        role = buffer[k];
        k++;
        turn = buffer[k];
        emit newResendAllMessage(id,players,board,still_in_deck,cards_in_hand,role,turn);
        qDebug() << "[NET] Buffer Length : " << QString::number(k);
        buffer.remove(0,k+1);
        return true;
    default:
        throw std::runtime_error("Ce message n'existe pas !");
    }
}

}
