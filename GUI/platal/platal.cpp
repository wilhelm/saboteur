#include "../GUI/platal/platal.h"
#include "../GUI/cardGUI/cardGUI.h"
#include "../saboteurGUI/saboteurGUI.h"
#include <QGridLayout>
#include <QDebug>
#include <QHeaderView>

namespace UI
{

Platal::Platal(QWidget *parent)
    : QTableWidget(parent)
{
    qDebug() << (SaboteurGUI*) parent;
    this->GUI = (SaboteurGUI*) parent;
    this->setRowCount(11);
    this->setColumnCount(15);

    this->setEditTriggers(QAbstractItemView::NoEditTriggers);

    QHeaderView *verticalHeader = this->verticalHeader();
#if QT_MAJOR_VERSION == 4
    verticalHeader->setResizeMode(QHeaderView::Fixed);
#else
    verticalHeader->setSectionResizeMode(QHeaderView::Fixed);
#endif
    verticalHeader->setDefaultSectionSize(100);
    verticalHeader->setVisible(false);

    QHeaderView *horizontalHeader = this->horizontalHeader();
#if QT_MAJOR_VERSION == 4
    horizontalHeader->setResizeMode(QHeaderView::Fixed);
#else
    horizontalHeader->setSectionResizeMode(QHeaderView::Fixed);
#endif
    horizontalHeader->setDefaultSectionSize(75);
    horizontalHeader->setVisible(false);

    this->setCellWidget(5, 3, new CardGUI(this, QString("../vrac/Cartes/Path/starting_card.jpg"), GUI, true));
    this->setCellWidget(3, 11, new CardGUI(this, GUI, true));
    this->setCellWidget(5, 11, new CardGUI(this, GUI, true));
    this->setCellWidget(7, 11, new CardGUI(this, GUI, true));

    this->connect(this, SIGNAL(cellClicked(int,int)), this, SLOT(placeCard(int,int)));
}

Platal::~Platal() { }

void Platal::updatePositionCard(int cardId, Position *p, bool orientation) {
    qDebug()<<"GUI updatePositionCard X : " << QString::number(p->first) << " Y : " << QString::number(p->second);
    QString cardPath = Client::Card::getPathToImage(cardId);
    CardGUI *card = new CardGUI(this, cardPath, GUI, cardId);
    if(orientation){
        card->rotate();
    }
    card->locked = true;
    qDebug() << "Before set widget";
    this->setCellWidget(p->first+4, p->second+4, card);
    qDebug() << "GUI End updatePositionCard";
}

void Platal::placeCard(int row, int column) {
    qDebug() << "try to place card " << ((GUI->selectedCard == 0) ? 0 : GUI->selectedCard->id);
    if(GUI->selectedCard != 0) {
        GUI->emitPlayCardSignal(GUI->selectedCard->id, GUI->selectedCard->orientation, 0, 0, new Client::Position(row-4, column-4));
        GUI->currentPlayerId = 0;
        CardGUI* item = (CardGUI*)this->cellWidget(row, column);
        if(item == 0) {
            qDebug() << "item does not exist";
        }
        if (item == 0)
        {
            this->setCellWidget(row, column, GUI->selectedCard);
            GUI->selectedCard->locked = true;
            GUI->selectedCard = 0;
            qDebug() << "card placed in (" << row << ", " << column << ")";
        }
        else {
            //what to do if cell is not empty
            qDebug() << "cell is not empty";
        }
    }
    else {
        qDebug() << "no card to drop";
    }

}
}
