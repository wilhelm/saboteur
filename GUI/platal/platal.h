#ifndef PLATAL_H
#define PLATAL_H

#include <QWidget>
#include <../GUI/cardGUI/cardGUI.h>
#include <../client/board.h>
#include <../client/pathCard.h>
#include <../client/card.h>
#include <QString>
#include <QGridLayout>
#include <QTableWidget>

namespace UI {
    class SaboteurGUI;
}

using namespace Client;

namespace UI{
/*!
 * \brief Widget containing the display of the game field.
 *
 * contains instances of Card
 * used in SaboteurGUI.displayGame()
 *
 * \see Card
 * \see SaboteurGUI
 */
class Platal : public QTableWidget
{
    Q_OBJECT
public:
    /*!
     * \brief Default constructor
     * \param parent
     */
    Platal(QWidget *parent = 0);
    ~Platal();
    UI::SaboteurGUI* GUI;
signals:
    void placeCardSignal(Position p, int cardID);
public slots:
    void updatePositionCard(int cardId, Position *p, bool orientation);
    void placeCard(int row, int column);
};
}
#endif // PLATAL_H
