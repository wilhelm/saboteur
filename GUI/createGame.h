#ifndef CREATEGAME_H
#define CREATEGAME_H

#include <QWidget>
#include <QString>
#include <QPushButton>
#include <QHBoxLayout>
#include <map>

using namespace std;

namespace UI {

class CreateGame : public QPushButton
{
    Q_OBJECT
public:
    CreateGame(QWidget *parent = 0);
    ~CreateGame();
    int nbPlayers = 1;
    int nbRounds = 1;
    bool debug;
    void Debug(QString message);

signals:
    void launchGameSignal(QString pseudo,int myId, int nbPlayers, int nbRounds,  map<int,QString> otherPlayersInfo);
    void addPlayerToGameSignal(QString pseudo);

public slots:
    void addPlayerToGame(QString pseudo);
    // Test function, remove it when project is stable
    void foo();
};
}

#endif // CREATEGAME_H
