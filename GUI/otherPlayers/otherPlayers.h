#ifndef OTHERPLAYERS_H
#define OTHERPLAYERS_H

#include <QWidget>
#include <../client/card.h>
#include <../client/playerActionCard.h>
#include "../GUI/user/userField.h"
#include <map>
#include <set>
#include <QString>

namespace UI {
    class SaboteurGUI;
}

using namespace std;
using namespace Client;

namespace UI {
/*!
 * \brief Widget containing the display of the informations on the other players.
 *
 * contains instances of UserField.
 *
 * \see UserField
 * \see SaboteurGUI
 */
class OtherPlayers : public QWidget
{
    Q_OBJECT
public:
    /*!
     * \brief Default constructor.
     * \param parent
     * \param nbPlayers
     */
    OtherPlayers(QWidget *parent = 0, int myId = 1, map<int,QString> otherPlayersInfo={{1,QString("BOT")},{2,QString("BOT")},{3,QString("BOT")}});
    ~OtherPlayers();
    map<int,UserField*> otherPlayersMap;

signals:
    void playActionPlayer(PlayerActionCard c, int numOtherPlayer);
public slots:
    void updatePlayerBlockedInfo(int id, bool* blocked); // Wagon, Pioche, blockLight
    void updatePlayerNbCards(int id, int nb);
    void isPlayerTurn(int id);
};
}
#endif // OTHERPLAYERS_H

