#include "../GUI/otherPlayers/otherPlayers.h"
#include <QVBoxLayout>
#include <QScrollArea>
#include <QString>

namespace UI {

OtherPlayers::OtherPlayers(QWidget *parent, int myId, map<int,QString> otherPlayersInfo) : QWidget(parent) {
    QVBoxLayout *layout = new QVBoxLayout();
    QScrollArea *scroll = new QScrollArea();
    QVBoxLayout *innerLayout = new QVBoxLayout();
    QFrame *inner = new QFrame(scroll);
    inner->setLayout(innerLayout);
    scroll->setWidgetResizable(true);
    scroll->setWidget(inner);
    setLayout(layout);
    for (map<int,QString>::iterator it = otherPlayersInfo.begin(); it != otherPlayersInfo.end(); ++it) {
        if (myId != it->first) {
            UserField* userField = new UserField(this, (SaboteurGUI*) parent, it->first,it->second);
            QObject::connect(userField, SIGNAL(clickedUserIcon(int,int)), parent, SLOT(clickUserTool(int,int)));
            otherPlayersMap.insert(pair<int,UserField*>(it->first, userField));
            innerLayout->addWidget(userField);
        }
    };
    layout->addWidget(scroll);

//    connect(parent, SIGNAL(otherPlayerHavingTurn(int, int)), this, SLOT(updatePlayerNbCards(int,int)));
}

OtherPlayers::~OtherPlayers() { }

void OtherPlayers::updatePlayerBlockedInfo(int id, bool* blocked) {
    otherPlayersMap[id]->updateBlockedInfo(blocked);
}

void OtherPlayers::updatePlayerNbCards(int id, int nb) {
    otherPlayersMap[id]->updateNbCards(nb);
}

void OtherPlayers::isPlayerTurn(int id) {
    for (map<int,UserField*>::iterator it = otherPlayersMap.begin(); it != otherPlayersMap.end(); ++it) {
        if (it->first == id)
            it->second->updateIcones(true);
        else
            it->second->updateIcones(false);
    }
}

}
