#include "saboteurGUI/saboteurGUI.h"
#include <QApplication>
#include <QPushButton>
#include <QDebug>

int main(int argc, char *argv[])
{
    qDebug() << "Main begin.";
    QApplication a(argc, argv);
    UI::SaboteurGUI w;
    qDebug() << "SaboteurGui show from main.";
    w.show();

    qDebug() << "Main end.";
    return a.exec();
}
