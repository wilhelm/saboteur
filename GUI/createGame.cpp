#include "../GUI/createGame.h"
#include <QDebug>
#include "../GUI/platal/platal.h"
#include "../GUI/otherPlayers/otherPlayers.h"
#include "../GUI/user/user.h"
#include "../GUI/saboteurGUI/saboteurGUI.h"

namespace UI {

CreateGame::CreateGame(QWidget *parent) : QPushButton(parent)
{
    debug = true;

    Debug("begin CreateGame CreateGame");
    qDebug() << QString::number(nbPlayers);
    setText(QString::number(nbPlayers) + " joueur à l'heure actuelle");

    QObject::connect(this, SIGNAL(launchGameSignal(QString, int, int, int, map<int,QString>)), ((SaboteurGUI*) parent), SLOT(displayGame(QString, int, int, int,  map<int,QString>)));
    QObject::connect(this, SIGNAL(addPlayerToGameSignal(QString)), this, SLOT(addPlayerToGame(QString)));
    //QObject::connect(this, SIGNAL(clicked()), this, SLOT(foo()));

    Debug("end CreateGame CreateGame");
}

CreateGame::~CreateGame() {
    Debug("begin CreateGame ~CreateGame");
    Debug("end CreateGame ~CreateGame");
}

void CreateGame::Debug(QString message){
    if(debug){
        qDebug() << message;
    }
}

void CreateGame::addPlayerToGame(QString pseudo) {
    Debug("begin CreateGame addPlayerToGame");

    this->nbPlayers++;
    setText(QString::number(this->nbPlayers) + " joueurs à l'heure actuelle");

    Debug("end CreateGame addPlayerToGame");
}

// Test function, remove it when project is stable
void CreateGame::foo() {
    Debug("begin CreateGame foo");
    emit launchGameSignal("Test",1,5,5, {{1,"Blah"},{2,"Saboteur"},{3,"Test"},{4,"BOT"}});
    Debug("end CreateGame foo");
}
}
