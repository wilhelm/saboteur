#include "../GUI/cardGUI/cardGUI.h"
#include "../saboteurGUI/saboteurGUI.h"
#include <QDebug>

namespace UI {

CardGUI::CardGUI(QWidget *parent, SaboteurGUI* GUI,int id, bool locked) : QLabel(parent)
{
    this->id = id;
    this->setScaledContents(true);
    this->locked = locked;
    this->GUI = GUI;
    QPixmap *pixmap = new QPixmap("../vrac/Cartes/Path/c1.jpg");
    img = pixmap->scaled(QSize(50, 100), Qt::KeepAspectRatio);
    setPixmap(img);
    QObject::connect(this, SIGNAL(cardDropped(CardGUI*)),GUI,SLOT(passDropCardInformation(CardGUI*)));
}

CardGUI::CardGUI(QWidget *parent, QString path, SaboteurGUI* GUI,int id, bool locked, bool pix) : QLabel(parent)
{
    qDebug() << "[GUI] Start Card GUI";
    this->id = id;
    this->setScaledContents(true);
    this->locked = locked;
    this->GUI = GUI;
    if(pix) {
        qDebug() << "[GUI] pixmap path : " << path;
        QPixmap *pixmap = new QPixmap(path);
        img = pixmap->scaled(QSize(50, 100), Qt::KeepAspectRatio);
        setPixmap(img);
    }
    QObject::connect(this, SIGNAL(cardDropped(CardGUI*)),GUI,SLOT(passDropCardInformation(CardGUI*)));
    qDebug() << "[GUI] End Card GUI";
}

CardGUI::~CardGUI() {

}

////// SLOTS
void CardGUI::mousePressEvent(QMouseEvent *event)
{
    if(event->button() == Qt::RightButton && !locked)
    {
        rotate();
    }
    else {
        if(event->button() == Qt::LeftButton && !locked && GUI->currentPlayerId == GUI->myId) {
            GUI->selectedCard = this;
            qDebug() << "card selected" << QString(this->id);
        }
    }
    event->accept();
}

void CardGUI::mouseDoubleClickEvent(QMouseEvent *event){
    if (event->button() == Qt::RightButton && !locked && (GUI->myId==GUI->currentPlayerId)){
        emit cardDropped(this);
    }
}

void CardGUI::rotate(){
    QMatrix matrix;
    matrix.rotate(180);
    img = img.transformed(matrix); //saving the changed QPixmap in a new QPixmap
    setPixmap(img); //setting changed Pixmap on the label
    orientation = !orientation;
    emit imageRotated(id);
}
}
