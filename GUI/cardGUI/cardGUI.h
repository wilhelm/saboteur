#ifndef CARDGUI_H
#define CARDGUI_H

#include <QLabel>
#include <QPushButton>
#include <QFormLayout>
#include <QMouseEvent>
#include <QString>
#include <QPixmap>

namespace UI {
    class SaboteurGUI;
}

namespace UI {
/*!
 * \brief Widget containing the display of one card.
 *
 * This widget is used in Platal
 *
 * \see Platal
 * \see SaboteurGUI
 */
class CardGUI : public QLabel
{
    Q_OBJECT
public:
    /*!
     * \brief Default constructor
     *
     * For now, creates a card with a default image
     * \param parent widget
     */
    CardGUI(QWidget *parent, UI::SaboteurGUI *GUI, int id, bool locked = false);
    CardGUI(QWidget *parent, QString path, UI::SaboteurGUI *GUI, int id,  bool locked = false, bool pix = true);
    ~CardGUI();
    bool orientation=false;
    int id;
    QPixmap img;
    void rotate();
    bool locked = true;
    UI::SaboteurGUI *GUI;

signals:
    void imageRotated(int CardID);
    void cardDropped(CardGUI *card);

public slots:
    void mousePressEvent(QMouseEvent *event);
    void mouseDoubleClickEvent(QMouseEvent *event);
};

}
#endif // CARDGUI_H
