#ifndef BEGINFORM_H
#define BEGINFORM_H

#include <QIntValidator>
#include <QLineEdit>
#include <QPushButton>
#include <QGridLayout>
#include <QFormLayout>
#include <QString>
#include <QLabel>

namespace UI {

/*!
 * \brief The main widget displayed at program initialization.
 *
 * This widget contains two forms to either create a new game or join a new one:
 * - username
 * - create game form
 *  - amount of players
 *  - amount of rounds
 *  - create game
 * - join game form
 *  - join game
 *
 * This widget is used in SaboteurGUI.init().
 * \see SaboteurGUI
 */
class BeginForm : public QGridLayout
{
    Q_OBJECT

protected:
    QLineEdit* userNameField; //!< input for the name of the user
    QLineEdit* nbRoundsField; //!< input for the amount of rounds for a new game (3-10)
    QLineEdit* nbPlayersField; //!< input for the amount of players for a new game (1-10)
    QLineEdit* playersPortNb;
    QLineEdit* hostIp; //!< input for the ip addres of the server
    QLineEdit* hostPort; //!< input for the port nb of the server
    QPushButton* createButton; //!< button to trigger the game creation
    QPushButton* joinButton; //!< button to trigger the joining of an existing game

    QLabel* problemOccurred;

public:
    /*!
     * \brief Default constructor.
     */
    BeginForm();
    /*!
     * \brief Default destructor.
     */
    ~BeginForm();

public slots:
    /*!
     * \brief Checks validity of the create form before emitting createGameSignal.
     *
     * Checks the following :
     *  - the user's name is not empty.
     *  - the amount of players is between 3 and 10 .
     *  - the amount of rounds is between 1 and 10.
     *
     * If all match, triggers the createGameSignal.
     * If not, does nothing.
     *
     * \see createGameSignal;
     * \see SaboteurGUI.createGame
     */
    void checkCreateGame();
    /*!
     * \brief Check validity of the join form before emitting joinGameSignal
     *
     * Checks if the user's name is not empty.
     * If yes, emits the joinGameSignal
     * If not, does nothing.
     *
     * \see joinGameSignal
     * \see SaboteurGUI.selectGame
     */
    void checkJoinGame();
    /*!
     * \brief Inform the user of a connection problem
     *
     * Prints the information about a problem.
     *
     * \see attemptNewConnection
     */
    void connectionErrorSlot();

signals:
    /*!
     * \brief Triggers the creation of a new game.
     * \param userName the name of the current user
     * \param nbPlayers the amount of players in the game
     * \param nbRounds the amount of rounds in the game
     * \param port the port number of the server
     */
    void createGameSignal(QString userName, int nbPlayers, int nbRounds, int port);
    /*!
     * \brief Triggers the joining of an existing game.
     * \param userName the name of the current user
     * \param ipAddress the IP address of the server
     * \param port the port number of the server
     */
    void joinGameSignal(QString userName, QString ipAddress, int port);
    /*!
     * \brief Reports if connection could not be established.
     */
    void connectionErrorSignal();

};
}
#endif // BEGINFORM_H
