#include "../GUI/beginForm/beginform.h"
#include <QDebug>

namespace UI {

BeginForm::BeginForm() : QGridLayout() {
    qDebug() << "BeginForm constructor begin.";
    //OK
    createButton = new QPushButton("Commencer la partie.");
    joinButton = new QPushButton("Participer a une partie");

    //OK
    nbPlayersField = new QLineEdit();
    nbPlayersField->setValidator(new QIntValidator(3, 10, this));

    nbRoundsField = new QLineEdit();
    nbRoundsField->setValidator(new QIntValidator(1, 10, this));

    playersPortNb = new QLineEdit("6542");
    playersPortNb->setValidator(new QIntValidator(0,66000, this));

    //OK
    userNameField = new QLineEdit();

    //---------------------
    hostIp = new QLineEdit();
    hostPort = new QLineEdit("6542");
    hostPort->setValidator(new QIntValidator(0,66000, this));

    QLabel* pseudo = new QLabel("pseudo : ");

    QLabel* creer = new QLabel("Creer une partie");
    QLabel* rejoindre = new QLabel("Rejoindre un jeu");

    QLabel* nbJoueurs = new QLabel("nb joueurs: ");
    QLabel* nbTours = new QLabel("nb tours: ");
    QLabel* myPort = new QLabel("mon port : ");
    QLabel* serverIP = new QLabel("adresse Ip du serveur: ");
    QLabel* serverPort = new QLabel("port du serveur: ");

    problemOccurred = new QLabel();

    //Arranging the disposition of elements----------------------
    //Get pseudo
    addWidget(pseudo, 0, 0);
    addWidget(userNameField, 0, 1);
    //Creer une partie
    addWidget(creer, 1,0,1,2);
    addWidget(nbJoueurs,2,0);
    addWidget(nbPlayersField,2,1);
    addWidget(nbTours,3,0);
    addWidget(nbRoundsField,3,1);
    addWidget(myPort,4,0);
    addWidget(playersPortNb,4,1);
    addWidget(createButton,5,0);
    //Rejoindre une partie
    addWidget(rejoindre,6,0,1,2);
    addWidget(serverIP,7,0);
    addWidget(hostIp, 7,1);
    addWidget(serverPort,8,0);
    addWidget(hostPort,8,1);
    addWidget(joinButton,9,0);
    addWidget(problemOccurred,10,0,1,2);
    //------------------------------------------------------------


    QObject::connect(createButton, SIGNAL(clicked()), this, SLOT(checkCreateGame()));
    QObject::connect(joinButton, SIGNAL(clicked()), this, SLOT(checkJoinGame()));
    QObject::connect(nbRoundsField, SIGNAL(returnPressed()),createButton,SIGNAL(clicked()));
    QObject::connect(playersPortNb, SIGNAL(returnPressed()),createButton,SIGNAL(clicked()));
    QObject::connect(hostIp, SIGNAL(returnPressed()),joinButton,SIGNAL(clicked()));
    QObject::connect(hostPort, SIGNAL(returnPressed()),joinButton,SIGNAL(clicked()));

    QObject::connect(this, SIGNAL(connectionErrorSignal()), this, SLOT(connectionErrorSlot()));
    QObject::connect(this, SIGNAL(connectionErrorSignal()), this, SLOT(connectionErrorSlot()));
}

void BeginForm::checkCreateGame() {
    int nbPlayers = (nbPlayersField->text()).toInt();
    int nbRounds = nbRoundsField->text().toInt();
    int portNb = playersPortNb->text().toInt();
    if(nbPlayers > 2 && nbPlayers < 11 && !userNameField->text().isEmpty() && nbRounds > 0 && nbRounds < 11) {
        //TODO : Create a wait-for-the-others window
        emit createGameSignal(userNameField->text(), nbPlayers, nbRounds, portNb);
    }
    else{
        emit connectionErrorSignal();
    }
}

void BeginForm::checkJoinGame() {
    qDebug() << "join signal begin";
    if(!userNameField->text().isEmpty() && !hostIp->text().isEmpty())
        emit joinGameSignal(userNameField->text(), hostIp->text(), hostPort->text().toInt());
    else{
        emit connectionErrorSignal();
    }
    qDebug() << "join signal end";
}

void BeginForm::connectionErrorSlot(){
    problemOccurred->setText("A connection problem occurred.\nDid you fill all fields correctly?");
}

BeginForm::~BeginForm()
{
    delete userNameField;
    delete nbRoundsField;
    delete nbPlayersField;
    delete playersPortNb;
    delete hostIp;
    delete hostPort;
    delete createButton;
    delete joinButton;
    delete problemOccurred;
}
}
