#-------------------------------------------------
#
# Project created by QtCreator 2016-10-18T13:32:14
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 5): QT += widgets
CONFIG(release, debug|release):DEFINES += QT_NO_DEBUG_OUTPUT

TARGET = saboteur_GUI
TEMPLATE = app
CONFIG += console c++11
QMAKE_CXXFLAGS += -std=c++11

DEFINES += "QT_MAJOR_VERSION=$$QT_MAJOR_VERSION"

SOURCES += main.cpp\
    saboteurGUI/saboteurGUI.cpp \
    beginForm/beginform.cpp \
    user/user.cpp \
    platal/platal.cpp\
    user/leftuserfield.cpp \
    user/iconesblocage.cpp \
    otherPlayers/otherPlayers.cpp \
    user/userField.cpp \
    cardGUI/cardGUI.cpp \
    ../client/myClient.cpp \
    ../client/board.cpp \
    ../client/card.cpp \
    ../client/pathCard.cpp \
    ../client/placeActionCard.cpp \
    ../client/playerActionCard.cpp \
    ../client/arrivalCard.cpp \
    ../client/otherplayer.cpp \
    GUI/gui.cpp \
    user/iconturn.cpp \
    createGame.cpp \
    ../net/client.cpp \
    ../server/player.cpp \
    user/blockicon.cpp

HEADERS  += saboteurGUI/saboteurGUI.h \
    beginForm/beginform.h \
    user/user.h \
    platal/platal.h \
    user/leftuserfield.h \
    user/iconesblocage.h \
    otherPlayers/otherPlayers.h \
    user/userField.h \
    cardGUI/cardGUI.h \
    ../client/myClient.h \
    ../client/board.h \
    ../client/card.h \
    ../client/pathCard.h \
    ../client/placeActionCard.h \
    ../client/playerActionCard.h \
    ../client/arrivalCard.h \
    ../client/otherplayer.h \
    GUI/gui.h \
    user/iconturn.h \
    createGame.h \
    ../net/client.h \
    ../server/player.h \
    user/blockicon.h

FORMS    +=
