#ifndef SABOTEUR_H
#define SABOTEUR_H

#include <QMainWindow>
#include <QString>
#include "../GUI/GUI/gui.h"
#include "../GUI/beginForm/beginform.h"
#include "../GUI/user/user.h"
#include "../GUI/otherPlayers/otherPlayers.h"
#include "../GUI/createGame.h"
#include "../cardGUI/cardGUI.h"
#include "../platal/platal.h"
#include <QWidget>
#include <QString>
#include <QDesktopWidget>
#include <QDebug>

namespace UI {
/*!
 * \brief Main widget controlling the game display.
 *
 * May ewist in the following configurations:
 * - Login and Create / Join game
 *  - Contains a BeginForm widget
 * - Display a game
 *  - Contains the following widgets:
 *   - Platal
 *   - OtherPlayers
 *   - User
 * - Game selection to join a game
 *
 * \see BeginForm
 * \see OtherPlayers
 * \see Platal
 * \see User
 */
class SaboteurGUI : public GUI
{
    Q_OBJECT

public:
    /*!
     * \brief Default constructor.
     */
    SaboteurGUI();
    /*!
     * \brief Default destructor.
     */
    ~SaboteurGUI();

    Platal *platal;
    QWidget *game;
    OtherPlayers *otherPlayers;
    User *user;
    BeginForm *beginForm;
    CardGUI* selectedCard = 0;
    CreateGame* createGameInstance;
    int myId;
    int currentPlayerId;

    bool debug;
    void Debug(QString message);

signals:
    /////////////////////////////////////////////////////////////////////////////
    /// Signals for createGame
    /////////////////////////////////////////////////////////////////////////////

    /*!
     * \brief addPlayerToGame
     * \param pseudo
     */
    void addPlayerToGameSignal(QString pseudo);

    /*!
     * \brief usersTurn
     * \param isItMyTurn true if it is the viewing player's turn
     */
    void usersTurn(bool isItMyTurn);

    /*!
     * \brief otherPlayerHavingTurn
     * \param playerId : identifier of the other player currently having turn
     * \param nbCards : number of cards left in the hand of the player currently having turn
     */
    void otherPlayerHavingTurn(int playerId, int nbCards);

public slots:
    /*!
     * \brief Internal to GUI : Creates a new game
     * \param username The name of the current user
     * \param nbPlayers The amount of players in the game
     * \param nbRounds The amount of rounds in the game
     * \see BeginForm.createGameSignal
     */
    void createGame(QString userName, int nbPlayers, int nbRounds, int port);
    /*!
     * \brief Internal to GUI : Opens the game selection interface
     * \param userName The name of the current user
     * \see BeginForm.joinGameSignal
     */
    void selectGame(QString userName, QString ipAddress, int port);
    /*!
     * \brief Internal to GUI : Opens the login interface
     * \see init
     */
    void goBack();
    /*!
     * \brief Internal to GUI : Creates the login interface.
     */
    void init();
    /*!
     * \brief clickUserTool - receives the signal saying which icon (representing a tool) has been clicked and to whom it belongs
     * \param userId
     * \param toolNumber - 0 for the pickaxe, 1 wagon, 2 light
     */
    void clickUserTool(int userId, int toolNumber);
    /*!
     * \brief passDropCardInformation - receives the signal saying which card the user wants to drop
     * \param card
     * \param pos
     */
    void passDropCardInformation(CardGUI* card);
    /////////////////////////////////////////////////////////////////////////////
    /// Slots inherited from GUI
    /////////////////////////////////////////////////////////////////////////////
    /*!
     * \brief Slot to recieve the id of the player which turn it is
     * \param playerId
     */
    void playerHavingTurn(int playerId, int nbCards);
    /*!
     * \brief Slot to recieve the status of one user
     * \param blocked an array containing an array of bolean for one player, describing their status
     */
    void playerBlockedStatus(bool* blocked, int playerId);
    /*!
     * \brief Slot updating one card on the board
     * \param cardId
     * \param position
     * \param orientation
     */
    void updateBoard(int cardId, Client::Position* p, bool orientation);
    /*!
     * \brief Slot to update the positions that can be reached on the board
     * \param positions
     */
    void updateReachablePositions(Client::Position *positions);
    /*!
     * \brief Slot to set the last card played in the game
     * \param position
     */
    void setLastCardPlayed(int cardId, Client::Position *position, bool orientation);
    /*!
     * \brief Slot to recieve the confirmation or refusal from client that the card played was accepted
     * \param confirmation
     */
    void confirmCardPlayed(bool confirmation);
    /*!
     * \brief Slot to set the card drawed from the user
     * \param cardId
     */
    void cardDrawn(int cardId);
    /*!
     * \brief displays the game once the round has begun
     * \param pseudo
     * \param nbPlayers
     * \param nbRounds
     * \param otherPlayersInfo
     * \param nbCardDeck
     */
    void displayGame(QString pseudo, int myId, int nbPlayers, int nbRounds, map<int,QString> otherPlayersInfo, int nbCardDeck = 100);
    /////////////////////////////////////////////////////////////////////////////
    /// Slots inherited from GUI for saboteurGUI (The welcome form)
    /////////////////////////////////////////////////////////////////////////////
    /*!
     * \brief Inform the user of a connection problem
     *
     * Prints the information about a problem.
     *
     * \see attemptNewConnection
     */
    void connectionErrorSlot();
    /////////////////////////////////////////////////////////////////////////////
    /// Slots inherited from GUI for saboteurGUI (beginform)
    /////////////////////////////////////////////////////////////////////////////
    /*!
     * \brief addPlayerToGame
     *
     * Adds a new player to the game
     *
     * \param pseudo
     */
    void addPlayerToGame(QString pseudo);
    void sendBlocageAction(int playerId, int blocage);
    void updateRole(bool isSaboteur);
    void updatePlayerNbCards(int playerId, int nbCards);

};
}
#endif // SABOTEUR_H
