#include "../GUI/saboteurGUI/saboteurGUI.h"
#include <QGraphicsView>

namespace UI {

SaboteurGUI::SaboteurGUI() {
    qDebug() << "SaboteurGui constructor begin.";
    selectedCard = 0;
    debug = true;
    init();
    qDebug() << "SaboteurGui constructor end.";
}


//TODO: use port nb information (and userName and nbPlayers and nbRounds)
void SaboteurGUI::createGame(QString userName, int nbPlayers, int nbRounds, int port) {
    Debug("begin GUI::SaboteurGUI::createGame");
    createGameInstance = new CreateGame(this);
    QObject::connect(this, SIGNAL(addPlayerToGameSignal(QString)), createGameInstance, SLOT(addPlayerToGame(QString)));
    qDebug() << createGameInstance->nbPlayers << " joueurs";
    setCentralWidget(createGameInstance);
    Debug("end GUI::SaboteurGUI::createGame");
}

void SaboteurGUI::selectGame(QString userName, QString ipAddress, int port) {
    userName = userName; ipAddress = ipAddress; port = port; // TODO: Remove this
    qDebug() << "join slot begin";
    QPushButton *game = new QPushButton("New player =)");

    setCentralWidget(game);
    qDebug() << "join slot end";
}

void SaboteurGUI::goBack() {
    Debug("begin SaboteurGUI goBack");
    init();
    Debug("end SaboteurGUI goBack");
}

void SaboteurGUI::init() {
    Debug("begin SaboteurGUI SaboteurGUI");

    QWidget *centralZone = new QWidget;

    QHBoxLayout *box = new QHBoxLayout();

    QWidget *centralBox = new QWidget;
    box->addWidget(centralBox);

    beginForm = new BeginForm;

    // ONLY FOR DEVELOPPEMENT PURPOSES
    //QObject::connect(beginForm, SIGNAL(createGameSignal(QString, int, int, int, int)), this, SLOT(createGame(QString, int, int, int, int)));
    //QObject::connect(beginForm, SIGNAL(joinGameSignal(QString, QString, int)), this, SLOT(selectGame(QString, QString, int)));

    //REAL SIGNAL CONNECTION
    QObject::connect(beginForm, SIGNAL(createGameSignal(QString, int, int, int)), this, SIGNAL(createGameSignal(QString, int, int, int)));
    QObject::connect(beginForm, SIGNAL(joinGameSignal(QString, QString, int)), this, SIGNAL(joinGameSignal(QString, QString, int)));

    centralBox->setLayout(beginForm);
    centralZone->setLayout(box);

    QSize* size = new QSize(250, 250);
    centralBox->setMaximumSize(*size);
    setCentralWidget(centralZone);

    resize(QDesktopWidget().availableGeometry().size());

    Debug("end SaboteurGUI SaboteurGUI");
}

SaboteurGUI::~SaboteurGUI()
{Debug("begin SaboteurGUI ~SaboteurGUI");
Debug("end SaboteurGUI ~SaboteurGUI");}

void SaboteurGUI::clickUserTool(int userId, int toolNumber){
    Debug("begin SaboteurGUI clickUserTool");
    userId = userId; toolNumber = toolNumber;//TODO
    Debug("end SaboteurGUI clickUserTool");
}

void SaboteurGUI::passDropCardInformation(CardGUI *card){
    Debug("begin SaboteurGUI passDropCardInformation");
    emit dumpCard(card->id);
    delete card;
    Debug("end SaboteurGUI passDropCardInformation");
}

/////////////////////////////////////////////////////////////////////////////
/// Slots inherited from GUI
/////////////////////////////////////////////////////////////////////////////

void SaboteurGUI::playerHavingTurn(int playerId, int nbCards) {
    Debug("begin SaboteurGUI playerHavingTurn");
    this->currentPlayerId = playerId;
    if(playerId != this->myId) {
        otherPlayers->isPlayerTurn(playerId);
        otherPlayers->updatePlayerNbCards(playerId,nbCards);
    }
    emit usersTurn(playerId == myId);
    Debug("end SaboteurGUI playerHavingTurn");
}

void SaboteurGUI::updatePlayerNbCards(int playerId, int nbCards) {
    Debug("begin SaboteurGUI updatePlayerNbCards");
    if(playerId != this->myId) {
        otherPlayers->updatePlayerNbCards(playerId,nbCards);
    }
    Debug("end SaboteurGUI updatePlayerNbCards");
}

void SaboteurGUI::playerBlockedStatus(bool *blocked, int playerId) {
    Debug("begin SaboteurGUI playerBlockedStatus");
    if (playerId == myId)
        user->updateBlockedInfo(blocked);
    else
        otherPlayers->updatePlayerBlockedInfo(playerId,blocked);
    Debug("end SaboteurGUI playerBlockedStatus");
}

void SaboteurGUI::updateRole(bool isSaboteur) {
    user->updateRole(isSaboteur, this);
}

void SaboteurGUI::updateBoard(int cardId, Client::Position* p,
                              bool orientation) {
    Debug("begin SaboteurGUI updateBoard");
    platal->updatePositionCard(cardId, p, orientation);
    Debug("end SaboteurGUI updateBoard");
}

void SaboteurGUI::updateReachablePositions(Client::Position *positions) {
    positions = positions; // TODO: Remove
}
void SaboteurGUI::setLastCardPlayed(int cardId, Client::Position *position, bool orientation) {
    platal->updatePositionCard(cardId, position, orientation);
}
void SaboteurGUI::confirmCardPlayed(bool confirmation) {
    confirmation = confirmation; // TODO: Remove
}
void SaboteurGUI::cardDrawn(int cardId) {
    this->user->cardsLayout->addWidget(new CardGUI(this->user, Client::Card::getPathToImage(cardId), this, cardId));
}
void SaboteurGUI::addPlayerToGame(QString pseudo){
    emit addPlayerToGameSignal(pseudo);
}

void SaboteurGUI::displayGame(QString pseudo, int myId, int nbPlayers, int nbRounds, map<int,QString> otherPlayersInfo, int nbCardDeck) {
    // TODO: Use pseudo, nbRounds
    qDebug() << "Slot displayGame";
    this->myId = myId;
    game = new QWidget(this);
    QVBoxLayout *box = new QVBoxLayout();
    QHBoxLayout *hBox = new QHBoxLayout();

    platal = new Platal(this);
    otherPlayers = new OtherPlayers(this, myId, otherPlayersInfo);
    hBox->addWidget(otherPlayers, 20);
    hBox->addWidget(platal, 80);

    user = new User(this, pseudo, nbCardDeck, myId);

    box->addLayout(hBox, 70);
    box->addWidget(user, 30);
    game->setLayout(box);

    setCentralWidget(game);
    emit gameDisplayed();
    qDebug() << "Slot displayGame end";
}
/////////////////////////////////////////////////////////////////////////////
/// Slots inherited from GUI for saboteurGUI (The welcome form)
/////////////////////////////////////////////////////////////////////////////
/*!
 * \brief Inform the user of a connection problem
 *
 * Prints the information about a problem.
 *
 * \see attemptNewConnection
 */
void SaboteurGUI::connectionErrorSlot(){
    beginForm->connectionErrorSlot();
}

void SaboteurGUI::sendBlocageAction(int playerId, int blocage) {
    qDebug() << "UserId : "<< playerId << "Blocage : " << blocage;
    if (selectedCard != 0)
        emit tryPlayCard(selectedCard->id,false,playerId,blocage);
}

void SaboteurGUI::Debug(QString message){
    if(debug){
        qDebug() << message;
    }
}

}
