#ifndef GUI_H
#define GUI_H

#include <QMainWindow>
#include "../GUI/cardGUI/cardGUI.h"
#include "../client/board.h"
#include "../server/player.h"

/*!
 * \brief This class contains the signals and slots accessible to the client
 */
class GUI : public QMainWindow
{
    Q_OBJECT
public:
    //! Default constructor
    explicit GUI(QWidget *parent = 0);
    void emitPlayCardSignal(int cardId, bool orientation, int playerId, int blockage, Client::Position* position = nullptr);

signals:
    /////////////////////////////////////////////////////////////////////////////
    /// Signals for saboteurGUI (the game)
    /////////////////////////////////////////////////////////////////////////////
    /*!
     * \brief Signal emitted when the player tries to play a card on board
     * \param cardId the id of the card to play
     * \param orientation The orientation in which the card is played (
     * \param playerID
     * \param blockage
     */
    void tryPlayCard(int cardId, bool orientation, int playerId, int blockage, Client::Position* position = nullptr);
    /*!
     * \brief Signal emitted when the player tries to drop a card
     * \param card
     * \param type
     */
    void dumpCard(int cardId);
    /*!
     * \brief Signal emitted when the displayGame operation is finished
     */
    void gameDisplayed();
    /////////////////////////////////////////////////////////////////////////////
    /// Signals for saboteurGUI (The welcome form)
    /////////////////////////////////////////////////////////////////////////////
    /*!
     * \brief Triggers the creation of a new game.
     * \param userName the name of the current user
     * \param nbPlayers the amount of players in the game
     * \param nbRounds the amount of rounds in the game
     * \param port the port number of the server
     */
    void createGameSignal(QString userName, int nbPlayers, int nbRounds, int port);
    /*!
     * \brief Triggers the joining of an existing game.
     * \param userName the name of the current user
     * \param ipAddress the IP address of the server
     * \param port the port number of the server
     */
    void joinGameSignal(QString userName, QString ipAddress, int port);

public slots:
    /////////////////////////////////////////////////////////////////////////////
    /// Slots for saboteurGUI (the game)
    /////////////////////////////////////////////////////////////////////////////
    /*!
     * \brief Slot to recieve the id of the player which turn it is
     * \param playerId
     */
    virtual void playerHavingTurn(int playerId, int nbCards) = 0;
    /*!
     * \brief Slot to recieve the status of all the users
     * \param blocked an array containing an array of bolean for each player, describing their status
     */
    virtual void playerBlockedStatus(bool* blocked, int playerId) = 0;
    /*!
     * \brief Slot updating one card on the board
     * \param cardId
     * \param position
     * \param orientation
     */
    virtual void updateBoard(int cardId, Client::Position* p, bool orientation) = 0;
    /*!
     * \brief Slot to update the positions that can be reached on the board
     * \param positions
     */
    virtual void updateReachablePositions(Client::Position *positions) = 0;
    /*!
     * \brief Slot to set the last card played in the game on the board
     * \param position
     */
    virtual void setLastCardPlayed(int cardId, Client::Position *position, bool orientation) = 0;
    /*!
     * \brief Slot to recieve the confirmation or refusal from client that the card played was accepted
     * \param confirmation
     */
    virtual void confirmCardPlayed(bool confirmation) = 0;
    /*!
     * \brief Slot to set the card drawed from the user
     * \param cardId
     */
    virtual void cardDrawn(int cardId) = 0;
    /*!
     * \brief displays the game once the round has begun
     * \param pseudo
     * \param nbPlayers
     * \param nbRounds
     * \param otherPlayersInfo is a map between playerid and playerName
     * \param nbCardDeck
     */
    virtual void displayGame(QString pseudo, int myId, int nbPlayers, int nbRounds, map<int,QString> otherPlayersInfo, int nbCardDeck = 100) = 0;
    /////////////////////////////////////////////////////////////////////////////
    /// Slots for saboteurGUI (The welcome form)
    /////////////////////////////////////////////////////////////////////////////
    /*!
     * \brief Inform the user of a connection problem
     *
     * Prints the information about a problem.
     *
     * \see attemptNewConnection
     */
    virtual void connectionErrorSlot() = 0;
    /////////////////////////////////////////////////////////////////////////////
    /// Slots for saboteurGUI (createGame)
    /////////////////////////////////////////////////////////////////////////////
    /*!
     * \brief addPlayerToGame
     * \param pseudo
     */
    virtual void addPlayerToGame(QString pseudo)=0;/*!
     * \brief Launches the create game window
     * \param username The name of the current user
     * \param nbPlayers The amount of players in the game
     * \param nbRounds The amount of rounds in the game
     * \see BeginForm.createGameSignal
     */
    virtual void createGame(QString userName, int nbPlayers, int nbRounds, int port) = 0;
    virtual void updateRole(bool isSaboteur) = 0;
    virtual void updatePlayerNbCards(int playerId, int nbCards) = 0;
};

#endif // GUI_H
