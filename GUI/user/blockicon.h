#ifndef BLOCKICON_H
#define BLOCKICON_H

#include <QLabel>
#include <QMouseEvent>

namespace UI {

class BlockIcon : public QLabel
{
    Q_OBJECT
public:
    BlockIcon(QWidget* parent, int id);
    ~BlockIcon();
    bool debug;
    void Debug(QString message);
    int id;

public slots:
    void mousePressEvent(QMouseEvent *event);

signals:
    void clickBlocageSignal(int id);
};

}
#endif // BLOCKICON_H
