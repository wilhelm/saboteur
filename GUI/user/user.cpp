#include "../GUI/user/user.h"
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QLabel>
#include <QFont>
#include <QDebug>

namespace UI {

User::User(QWidget *parent, QString pseudo, int nbCardDeck, int myId) : QWidget(parent) {

    debug = true;
    Debug("begin User User");

    QHBoxLayout *layout = new QHBoxLayout;
    setLayout(layout);
    SaboteurGUI* GUI = (SaboteurGUI*) parent;

    left = new LeftUserField(this, GUI, myId);
    QWidget *test = left;
    QObject::connect(parent, SIGNAL(usersTurn(bool)), left, SLOT(updateIcones(bool)));

    roleCard = new CardGUI(this, GUI, 0, true);

    /* central part */
    QWidget *centralWidget = new QWidget();
    QVBoxLayout *centralLayout = new QVBoxLayout();

    this->pseudo = pseudo;
    QLabel *name = new QLabel(pseudo+" : "+QString::number(nbCardDeck)+" cartes restantes.");
    name->setAlignment(Qt::AlignCenter);
    QFont f;
    f.setPointSize(45);
    f.setBold(true);
    name->setFont(f);

    QWidget *Cards = new QWidget();
    cardsLayout = new QHBoxLayout();
    //cardsLayout->addWidget(new CardGUI(this, GUI));
    //cardsLayout->addWidget(new CardGUI(this, GUI));
    //cardsLayout->addWidget(new CardGUI(this, GUI));
    //cardsLayout->addWidget(new CardGUI(this, GUI));
    //cardsLayout->addWidget(new CardGUI(this, GUI));
    Cards->setLayout(cardsLayout);
    Cards->setStyleSheet("background-color: green;");

    centralLayout->addWidget(name, 30);
    centralLayout->addWidget(Cards, 70);
    centralWidget->setLayout(centralLayout);

    layout->addWidget(test, 30);
    layout->addWidget(centralWidget, 60);
    layout->addWidget(roleCard, 10);

    Debug("end User User");
}

User::~User()
{
    Debug("begin User ~User");
    Debug("end User ~User");
}
void User::updatePlayer(int clientId){
    Debug("begin User updatePlayer");
    clientId = clientId; // TODO
    Debug("end User updatePlayer");
}
void User::updateAbilityPlayer(Client::PlayerActionCard c) {
    Debug("begin User updateAbilityPlayer");
    c = c; // TODO
    Debug("end User updateAbilityPlayer");
}

void User::updateBlockedInfo(bool* blocked) {
    Debug("begin User updateBlockedInfo");
    left->updateBlocked(blocked);
    Debug("end User updateBlockedInfo");
}

void User::Debug(QString message){
    if(debug){
        qDebug() << message;
    }
}

void User::updateRole(bool isSaboteur, QWidget *GUI) {
    if (isSaboteur) {
        QPixmap *pixmap = new QPixmap("../vrac/Cartes/Role/saboteur.jpg");
        roleCard->img = pixmap->scaled(QSize(50, 100), Qt::KeepAspectRatio);
        roleCard->setPixmap(roleCard->img);
    }
    else {
        QPixmap *pixmap = new QPixmap("../vrac/Cartes/Role/miner.jpg");
        roleCard->img = pixmap->scaled(QSize(50, 100), Qt::KeepAspectRatio);
        roleCard->setPixmap(roleCard->img);
    }
}

}
