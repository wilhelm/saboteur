#include "../GUI/user/userField.h"
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QLabel>
#include <QString>
#include <QDebug>

namespace UI
{

UserField::UserField(QWidget *parent, SaboteurGUI *s, int index, QString name)
    : QWidget(parent)
{
    debug = true;
    Debug("begin UserField UserField");

    this->name = name;
    this->id = index;
    QHBoxLayout *layout = new QHBoxLayout();
    left = new LeftUserField(this, s, id);
    QVBoxLayout *right = new QVBoxLayout();
    QLabel *nameDisplay = new QLabel(name);
    setLayout(layout);
    layout->addWidget(left,80);
    layout->addLayout(right,20);
    right->addWidget(nameDisplay,50);
    right->addWidget(nbCardsDisplay,50);
    this->setFixedHeight(150);

    QObject::connect((left->icones), SIGNAL(giveClickedIcon(int)), this, SLOT(slotBlockingIconClicked(int)));

    Debug("end UserField UserField");
}

void UserField::updateIcones(int isTurn){
    Debug("begin UserField updateIcones");
    left->updateIcones(isTurn);
    Debug("end UserField updateIcones");
}

void UserField::updateBlockedInfo(bool* blocked) {
    Debug("begin UserField updateBlockedInfo");
    left->updateBlocked(blocked);
    Debug("end UserField updateBlockedInfo");
}

void UserField::updateNbCards(int nb) {
    Debug("begin UserField updateNbCards");
    nbCardsDisplay->setNum(nb);
    Debug("end UserField updateNbCards");
}

void UserField::slotBlockingIconClicked(int i){
    Debug("begin UserField slotBlockingIconClicked");
    emit clickedUserIcon(id, i);
    Debug("end UserField slotBlockingIconClicked");
}

void UserField::Debug(QString message){
    if(debug){
        qDebug() << message;
    }
}

}
