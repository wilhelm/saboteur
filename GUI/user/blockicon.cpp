#include "blockicon.h"
#include <QDebug>

namespace UI{


BlockIcon::BlockIcon(QWidget* parent, int id): QLabel(parent){
    qDebug() << "begin BlockIcon BlockIcon";
    this->id = id;
    QObject::connect(this, SIGNAL(clickBlocageSignal(int)),parent,SLOT(clickBlocage(int)));
    qDebug() << "end BlockIcon BlockIcon";
}

BlockIcon::~BlockIcon(){
    Debug("begin BlockIcon ~BlockIcon");
    Debug("end BlockIcon ~BlockIcon");
}

void BlockIcon::Debug(QString message){
    if(debug){
        qDebug() << message;
    }
}

void BlockIcon::mousePressEvent(QMouseEvent *event)
{
    qDebug() << "begin BlockIcon mousePressEvent";
    emit clickBlocageSignal(id);
    qDebug() << "end BlockIcon mousePressEvent";

}

}

