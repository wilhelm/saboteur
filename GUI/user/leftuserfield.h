#ifndef LEFTUSERFIELD_H
#define LEFTUSERFIELD_H

#include <QObject>
#include <QWidget>
#include <QLabel>
#include "../GUI/user/iconesblocage.h"
#include "../GUI/user/iconturn.h"
#include <../client/playerActionCard.h>

namespace UI {
    class SaboteurGUI;
}

using namespace Client;

namespace UI {
/*!
 * \brief Widget containing the general status of a user.
 *
 * This includes the following:
 * - indicator to know whether it is this users turn or not
 * - blocked status indicator
 *
 * \see IconesBlocage
 * \see SaboteurGUI
 */
class LeftUserField : public QWidget
{
    Q_OBJECT
public:
    /*!
     * \brief Default constructor.
     * \param parent
     */
    LeftUserField(QWidget *parent, SaboteurGUI *s, int userId);

    //*bouton pour marquer a qui le tour*//
    IconesBlocage* icones; //!< Blocked status indicator
    IconTurn* turn;

    bool debug;
    void Debug(QString message);

signals:
    void receiveActionCard(Card c);

public slots:
    void updateBlocked(bool* blocked);
    void updateIcones(bool isTurn);

};
}
#endif // LEFTUSERFIELD_H
