#ifndef ICONESBLOCAGE_H
#define ICONESBLOCAGE_H

#include <QObject>
#include <QLabel>
#include <QHBoxLayout>
#include <../client/playerActionCard.h>
#include <../client/card.h>
#include <set>
#include <map>
#include <QMouseEvent>
#include "blockicon.h"

namespace UI {
    class SaboteurGUI;
}

using namespace std;
using namespace Client;

namespace UI {
/*!
 * \brief Widget containing the display for the blocked status of each user.
 * \see SaboteurGUI
 */
class IconesBlocage : public QLabel
{
    Q_OBJECT
public:
    /*!
     * \brief Default constructor.
     * \param parent
     */

    IconesBlocage(QWidget *parent, SaboteurGUI *s, int userId);

    ///Frame color -> defines the blocking status for each blocking type through the choice of a color (green, red)
    BlockIcon* lcPioche;
    BlockIcon* lcWagon;
    BlockIcon* lcLight;
    QHBoxLayout* layout;
    SaboteurGUI* s;
    int userId;
    bool debug;
    void Debug(QString message);

signals:
    //TODO
    void playActionPlayer(PlayerActionCard c);

    /// Tells which icon has been clicked : 0 for the Pickaxe, 1 for the Wagon, 2 for the Light
    void giveClickedIcon(int i);

public slots:
    void update(bool* blocked); // wagon/pioche/light
    void clickBlocage(int id);

};
}
#endif // ICONESBLOCAGE_H
