#ifndef USER_H
#define USER_H

#include <QWidget>
#include "../client/playerActionCard.h"
#include <QString>
#include "leftuserfield.h"
#include <QHBoxLayout>
#include "../GUI/cardGUI/cardGUI.h"

namespace UI {
    class SaboteurGUI;
}

namespace UI {

/*!
 * \brief Widget containing the display for the current user.
 *
 * This includes:
 * - a LeftUserField
 * - his hand
 * - his role : saboteur / mineur
 *
 * \see LeftUserField
 * \see SaboteurGUI
 */
class User : public QWidget
{
    Q_OBJECT

public:
    /*!
     * \brief Default constructor.
     * \param parent
     */
    User(QWidget *parent, QString pseudo, int nbCardDeck, int myId);

    /*!
     * \brief Default destructor.
     */
    ~User();

    QHBoxLayout *cardsLayout;
    QString pseudo;
    LeftUserField* left;
    CardGUI *roleCard;
    bool debug;
    void Debug(QString message);

signals:
    void playActionCard(Client::PlayerActionCard p);

public slots:
    void updatePlayer(int clientId);
    void updateAbilityPlayer(Client::PlayerActionCard c);
    void updateBlockedInfo(bool* blocked);
    void updateRole(bool isSaboteur, QWidget* GUI);
};
}
#endif // USER_H
