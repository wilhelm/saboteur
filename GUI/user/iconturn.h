#ifndef ICONTURN_H
#define ICONTURN_H

#include <QObject>
#include <QLabel>
#include <QHBoxLayout>

namespace UI{

class IconTurn : public QLabel
{
    Q_OBJECT

public:
    IconTurn(QWidget* parent);

    QLabel* iconLabel;
    QLabel* textLabel;

    QHBoxLayout* layout;

    bool debug;
    void Debug(QString message);

public slots:
    //i = 0 if someone else's turn, i = 1 if the player's trun
    void changeStatus(bool i);
};

}
#endif // ICONTURN_H
