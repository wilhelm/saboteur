#include "../GUI/user/iconturn.h"
#include <QDebug>

namespace UI{

IconTurn::IconTurn(QWidget* parent) : QLabel(parent)
{
    debug = true;
    Debug("begin IconTurn IconTurn");

    layout = new QHBoxLayout(this);

    iconLabel = new QLabel(this);
    textLabel = new QLabel(this);

    textLabel->setText("Please Wait");

    layout->addWidget(iconLabel);
    layout->addWidget(textLabel);

    iconLabel->setStyleSheet("border-image:url(../vrac/pictures/forbidden.jpg);");

    Debug("end IconTurn IconTurn");
}

void IconTurn::Debug(QString message){
    if(debug){
        qDebug() << message;
    }
}

void IconTurn::changeStatus(bool i){
    Debug("begin IconTurn changeStatus");
    if (!i)
    {
        iconLabel->setStyleSheet("border-image:url(../vrac/pictures/forbidden.jpg);");
        textLabel->setText("Please Wait");
    }
    else
    {
        iconLabel->setStyleSheet("border-image:url(../vrac/pictures/dice.jpg);");
        textLabel->setText("Your turn");
    }
    Debug("end IconTurn changeStatus");
}

}
