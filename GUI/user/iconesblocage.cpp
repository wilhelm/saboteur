#include "../GUI/user/iconesblocage.h"
#include "../GUI/saboteurGUI/saboteurGUI.h"
#include <QDebug>

namespace UI {

IconesBlocage::IconesBlocage(QWidget *parent, SaboteurGUI *s, int userId) : QLabel(parent)
{
    debug = true;
    Debug("begin IconesBlocage IconesBlocage");

    layout = new QHBoxLayout(this);
    //setStyleSheet("background-color:#202020; border-style: outset; border-width: 2px;border-color: blue;");

    //Status : frame color-------------------------------------------------------------------
    lcPioche = new BlockIcon(this,1);
    lcWagon = new BlockIcon(this,0);
    lcLight = new BlockIcon(this,2);

    layout -> addWidget(lcLight);
    layout -> addWidget(lcWagon);
    layout -> addWidget(lcPioche);

    lcPioche->setStyleSheet("border-image:url(../vrac/pictures/iconPickaxeOK.jpg);");
    lcWagon->setStyleSheet("border-image:url(../vrac/pictures/iconWagonOK.jpg);");
    lcLight->setStyleSheet("border-image:url(../vrac/pictures/iconLampOK.jpg);");
    this -> s = s;
    this -> userId = userId;

    Debug("end IconesBlocage IconesBlocage");
}

//Slots----------------------------------------------------------------------------------------

void IconesBlocage::update(bool* blocked) {
    Debug("begin IconesBlocage update");

    if (blocked[1])
        lcPioche->setStyleSheet("border-image:url(../vrac/pictures/iconPickaxeBlocked.jpg);");
    else
        lcPioche->setStyleSheet("border-image:url(../vrac/pictures/iconPickaxeOK.jpg);");
    if (blocked[0])
        lcWagon->setStyleSheet("border-image:url(../vrac/pictures/iconWagonBlocked.jpg);");
    else
        lcWagon->setStyleSheet("border-image:url(../vrac/pictures/iconWagonOK.jpg);");
    if (blocked[2])
        lcLight->setStyleSheet("border-image:url(../vrac/pictures/iconLampBlocked.jpg);");
    else
        lcLight->setStyleSheet("border-image:url(../vrac/pictures/iconLampOK.jpg);");

    Debug("end IconesBlocage update");

}

void IconesBlocage::Debug(QString message){
    if(debug){
        qDebug() << message;
    }
}

void IconesBlocage::clickBlocage(int id) {
    qDebug() << id;
    s->sendBlocageAction(userId,id);
}
}
