#ifndef USERFIELD_H
#define USERFIELD_H

#include <QObject>
#include <QWidget>
#include <QLabel>
#include <QString>
#include "../client/card.h"
#include "../client/playerActionCard.h"
#include "leftuserfield.h"

namespace UI {
    class SaboteurGUI;
}

namespace UI {
/*!
 * \brief Widget containing the information about a player different from the user.
 *
 * This includes:
 * - a LeftUserField
 * - his name
 * - his number of cards in hand
 *
 * \see LeftUserField
 * \see SaboteurGUI
 */
class UserField : public QWidget
{
    Q_OBJECT
public:
    /*!
     * \brief Default constructor.
     * \param parent
     * \param index
     */
    UserField(QWidget *parent, SaboteurGUI *s, int index=0, QString name="BOT");
    QString name;
    int id;
    LeftUserField* left;
    QLabel *nbCardsDisplay = new QLabel("0");
    bool debug;
    void Debug(QString message);

signals:
    void receiveActionCard(Client::Card c);
    ///Informs whose and which icon has been icon: card = 0 for a pickaxe, 1 wagon, 2 light
    void clickedUserIcon(int id, int icon);
public slots:
    void updateIcones(int isTurn);
    void updateBlockedInfo(bool* blocked);
    void updateNbCards(int nb);
    ///receives information about which icon has been clicked
    void slotBlockingIconClicked(int i);
};
}
#endif // USERFIELD_H
